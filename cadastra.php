<?php    
    require_once('includes/head.php');
    require_once('db/conecta.php');   
    require_once('functions/usuario-dao.php'); 
    if($_POST){
      $cadastro = cadastraUsuario($conecta, $_POST['usuario'], $_POST['nome'], $_POST['senha']);
      if($cadastro){
?>
        <script type="text/javascript">
          alert("Usuário cadastrado com sucesso, efetue login na próxima tela.");
          window.location.href = 'index.php';
          
        </script>
<?        
      }else{
?>
        <script type="text/javascript">
          alert("Ocorreu um problema, tente novamente mais tarde");
        </script>
<?php        
      }
    }

?>
    
<main class="form-signin">
  <form method="POST" action="">
    <img class="mb-4" src="assets/brand/bootstrap-logo.svg" alt="" width="72" height="57">
    <h1 class="h3 mb-3 fw-normal">Cadastro de Usuário</h1>
    <label for="inputEmail" class="visually-hidden">Usuário</label>
    <input type="text" style="margin-bottom: 16px;" id="inputEmail" name="usuario" class="form-control" placeholder="Digite o usuário" required autofocus>
    <label for="inputPassword" class="visually-hidden">Nome</label>
    <input type="text" id="inputPassword" style="margin-bottom:15px;" name="nome" class="form-control" placeholder="Digite o seu nome" required>
    <label for="inputPassword" class="visually-hidden">Senha</label>
    <input type="password" id="inputPassword" name="senha" class="form-control" placeholder="Digite a sua senha" required>    
    <div class="checkbox mb-3">
      <!-- <label>
        <input type="checkbox" value="remember-me"> Remember me
      </label> -->
    </div>
    <button class="w-100 btn btn-lg btn-success" type="submit" id="loginBtn" name="login">Cadastrar</button>
    <br>
    <a href="index.php">
    <button class="w-100 btn btn-underline btn-link" type="button" style="margin-top:22px;">Voltar</button></a>
    <p class="mt-5 mb-3 text-muted">&copy; <?=date("Y")?></p>
  </form>
</main>


    
  </body>
</html>
