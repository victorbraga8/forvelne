  <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="position-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="dashboard.php">
              <span data-feather="file-text"></span>
              Transposição Numérica
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="dashboard-b.php">
              <span data-feather="file-text"></span>
              Condição de Produtório
            </a>
          </li> 
<!--           <li class="nav-item">
            <a class="nav-link" href="dashboard-c.php">
              <span data-feather="file-text"></span>
              Distribuição Numérica
            </a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="logout.php">
              <span data-feather="file-text"></span>
              Logout
            </a>
          </li>                  
        </ul>
      </div>
    </nav>