<?php 
error_reporting(0);
ini_set('display_errors', 0 );
require_once('includes/head-dashboard.php');
require_once('functions/combi-dao-c.php');
if($_POST['dados']){  
  $dados = explode(" ", $_POST['dados']);    
}

?>
<div class="container-fluid">
  <div class="row">
<?php 
    require_once('includes/sidemenu.php');
?>
  </div>
  <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Distribuição Numérica</h1>        
    </div>
    <form method="POST">
      <div class="row">
        <div class="col-sm-3">
          <label for="elementos" style="font-size:17px;">Massa de Dados</label>
          <input type="text" name="dados" class="form-control" id="dados" required="Informe a massa de dados" value="<?=$_POST['dados']?>">
        </div>
        <div class="col-sm-1" style="max-width:77px;">          
          <button type="submit" name="gerar" id="btnGerar" class="btn btn-success" style="position: absolute;top: 16.9%;">Gerar</button>
        </div>
<?php 
        if($_POST){
?>  
        <div class="col-sm-3">
          <a href="dashboard-c.php">
            <button type="button" class="btn btn-primary"style="position: absolute;top: 16.9%;">Limpar Resultados</button>            
          </a>
        </div>
<?php         
        }
?>
      </div>
      <hr>
    </form>
<?php
  if($_POST['dados']){
?>
  <div class="table-responsive" style="overflow-x: unset!important;">     
<?php 
      echo "<h4>Massa de Dados Inserida:</h4> <b>".$_POST['dados'].'</b>';
?>
    <h2>Resultados</h2>
    <div class="row" style="max-height: 350px; overflow-x: auto; padding:15px;">              
<?php 
  for ($i=0; $i < 40; $i++) { 
    $final = distribuicaoNumerica($dados,$i);
?>
     <div class="col-sm-2">
       <ul style= "list-style-type: none; padding: 0;">
         <li>
            <?php echo $final;?>
         </li>
       </ul>
     </div> 
<?php        
  }    
?>      
    </div> 
  </div>
  <div class="table-responsive" style="overflow-x: unset!important;">     
    <h2>Repetições</h2>
    <div class="row" style="max-height: 350px; overflow-x: auto; padding:15px;">              
<?php 
  $arr = array_count_values($dados);  
  $repetido = "Item Repetido: ";
  foreach($arr as $key => $value){
    if($value > 1){
?>
    <div class="col-sm-2">
      <ul style= "list-style-type: none; padding: 0;">
        <li>
          <?php echo '<h6>Item Repetido: <b>'. $key ."</b> <br>Repetições: <b style='color:red;'>".$value."</b><h6>"?>
        </li>
      </ul>
    </div>
<?php      
       
    }
  }   
?>      
    </div>
  </div>
<?php    
  }    
?>
  </main>
</div>
<!-- <script type="text/javascript">
  condicao = document.querySelector('#condicao');
  condicao.addEventListener('change',validaInsert);
  elementos = document.querySelector('#elementos');
  elementos.addEventListener('change',validaInsert);
  function validaInsert(){                
    if(elementos.value > 5){
      alert('O valor máximo de elementos por grupo habilitado é 5.');
      elementos.value = 5          
    }
    if(condicao.value > 22){
      alert('O valor máximo para condição de somatório é 22.');
      condicao.value = 22;
    }
  }
</script> -->