<?php 
error_reporting(0);
ini_set('display_errors', 0 );
function combinacoesDe($k, $xs){
  shuffle($xs);
    if ($k === 0)
        return array(array());
    if (count($xs) === 0)
        return array();
    $x = $xs[0];
    $xs1 = array_slice($xs,1,count($xs)-1);
    $res1 = combinacoesDe($k-1,$xs1);
    for ($i = 0; $i < count($res1); $i++) {
        array_splice($res1[$i], 0, 0, $x);
    }
    $res2 = combinacoesDe($k,$xs1);
    return array_merge($res1, $res2);
}

function verificaGrupoBase($elemento){
  if($elemento == 1){
    $resultado = ['a','e','h','j','o','t','x'];
  }elseif($elemento == 2){
    $resultado = ['b','f','i','k','p','v','y'];
  }elseif($elemento == 3){
    $resultado = ['c','g','l','q','v','z'];
  }elseif($elemento == 4){
    $resultado = ['d','m','r','w'];
  }elseif($elemento == 5){
    $resultado = ['n','s'];
  }
  return $resultado;
}

 function verificaGrupo($elemento){
    if($elemento == 'a' || $elemento == 'e' || $elemento == 'h' || $elemento == 'j' || $elemento == 'o' || $elemento == 't' || $elemento == 'x'){
      return '1';
    }elseif($elemento == 'b' || $elemento == 'f' || $elemento == 'i' || $elemento == 'k' || $elemento == 'p' || $elemento == 'v' || $elemento == 'y'){
      return '2';
    }elseif($elemento == 'c' || $elemento == 'g' || $elemento == 'l' || $elemento == 'q' || $elemento == 'v' || $elemento == 'z'){
      return '3';
    }elseif($elemento == 'd' || $elemento == 'm' || $elemento == 'r' || $elemento == 'w'){
      return '4';
    }elseif($elemento == 'n' || $elemento == 's'){
      return '5';
    }     
  }
 
  function grupoBase($tamanho, $resultados){
    // shuffle($resultados); 
    if($tamanho == 1){
      return $resultados[0];
    }
    elseif($tamanho == 2){
      return $resultados[0].$resultados[1];
    }
    elseif($tamanho == 3){
      return $resultados[0].$resultados[1].$resultados[2];
    }      
    elseif($tamanho == 4){
      return $resultados[0].$resultados[1].$resultados[2].$resultados[3];
    }
    elseif($tamanho == 5){
      return $resultados[0].$resultados[1].$resultados[2].$resultados[3].$resultados[4];
    }
    // elseif($tamanho == 6){
    //   return $resultados[0].$resultados[1].$resultados[2].$resultados[3].$resultados[4].$resultados[5] ;
    // }

  }

  function montaGrupoValores($tamanho, $resultados){    
    if($tamanho == 1){
      return verificaGrupo($resultados[0]);
    }
    elseif($tamanho == 2){
      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]);
    }
    elseif($tamanho == 3){
      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]).verificaGrupo($resultados[2]);
    }      
    elseif($tamanho == 4){
      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]).verificaGrupo($resultados[2]).verificaGrupo($resultados[3]);
    }
    elseif($tamanho == 5){
      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]).verificaGrupo($resultados[2]).verificaGrupo($resultados[3]).verificaGrupo($resultados[4]);
    }
    // elseif($tamanho == 6){
    //   return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]).verificaGrupo($resultados[2]).verificaGrupo($resultados[3]).verificaGrupo($resultados[4]).verificaGrupo($resultados[5]);
    // }

  }

  function somaGrupoValores($tamanho, $resultados){    
    if($tamanho == 1){
      return verificaGrupo($resultados[0]);
    }
    elseif($tamanho == 2){
      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1]);
    }
    elseif($tamanho == 3){
      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2]);
    }      
    elseif($tamanho == 4){
      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2])+verificaGrupo($resultados[3]);
    }
    elseif($tamanho == 5){
      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2])+verificaGrupo($resultados[3])+verificaGrupo($resultados[4]);
    }
    // elseif($tamanho == 6){
    //   return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2])+verificaGrupo($resultados[3])+verificaGrupo($resultados[4])+verificaGrupo($resultados[5]);
    // }

  }

// function montagemFinal($resultados){
//   shuffle($resultados);
//   $resultado = array();

//   for ($i=0; $i < count($resultados); $i++) { 
//     // $resultado = [grupoBase(3, $resultado).' - '.montaGrupoValores(3, $resultado).' - '.somaGrupoValores(3, $resultado)]; 
//     $resultado[$i] = grupoBase(3, $resultados).' - '.montaGrupoValores(3, $resultados).' - '.somaGrupoValores(3, $resultados);
//   }    
//   return $resultado;
// }

$dezenas = ['a','e','h','j','o','t','x','b','f','i','k','p','v','y','c','g','l','q','v','z','d','m','r','w','n','s'];      
$resultados = combinacoesDe(3, $dezenas);

$resultado = grupoBase(3,$resultados);

echo "<pre>";
print_r($resultado);
echo "</pre>";
die();

// function montagemFinal($resultados){
//   $c = 0;
//   shuffle($resultados);
//   $array = array();
//   foreach ($resultados as $resultado) {
//     $array[$c][0] = grupoBase(3, $resultado);
//     $array[$c][1] = somaGrupoValores(3, $resultado);
//     $array[$c][2] = montaGrupoValores(3, $resultado);
//     $c ++;
//   }  
//   return $array;
// }

// $testes = montagemFinal($resultados);
// array_unique($testes);
// asort($testes);
// foreach ($testes as $teste) {
//   echo $teste[0].' - '.$teste[1].' - '.$teste[2].'<br>';
// }

// die();

// $dezenas = ['49','19','59','28','69','39','79','18','58','28','68','38','78','27','02','67','37','77','02','01','77','09'];
$dezenas = ['01','01','01','28','56','65','79','17','33','52','60','76','20','32','49','54','62','04','15','44','64','73','05','16','26','28','62','09','21','37','42','68','04','45','54','65','67','19','44','53','54','71','27','57','60','61','77','05','15','32','33','48','13','30','58','63','64'];


// $arr = array_count_values($dezenas);

// foreach($arr as $key => $value){
//     if($value > 1){
//         echo 'valor repetido: '. $key ." - ".$value.'<br>';
//     }
// }

// $t = array_count_values($dezenas);
// foreach ($t as $tt => $key)  {
//   if($key > 1){
//     echo $key.'<br>';
//   }
// }

// die();

// function get_duplicates( $array ) {
//     return array_unique( array_diff_assoc( $array, array_unique( $array ) ) );
// }
// $list = get_duplicates($dezenas);
 
//  foreach ($list as $lists) {   
//   echo $lists.'<br>';
//  }
// die();

// $dezenas = array_unique($dezenas);


// $b = count($verificadores);
// echo $b.'<br>';
// die();
// 39
function verificador($dezenas,$verificadorA, $verificadorB, $sentenca, $sentencaA, $sentencaB){
  if(in_array($verificadorA, $dezenas) && in_array($verificadorB, $dezenas)){
    $resultado = $sentenca;
  }elseif(!in_array($verificadorA, $dezenas) || !in_array($verificadorB, $dezenas)){
    $k1 = array_search($verificadorA, $dezenas);
    $k2 = array_search($verificadorB, $dezenas);  
    if($k1){
      $resultado = $sentencaA;
    }elseif($k2){
      $resultado = $sentencaB;
    }      
  } 
  return $resultado;
}


for ($i=0; $i <= 40; $i++) { 
  $final = distribuicaoNumerica($dezenas,$i);
  echo $final.' TESTE <br>';  
}

function distribuicaoNumerica($dezenas,$i){
$verificadores = ['09','49','19','59','29','69','39','79','18','58','28','68','38','78','27','67','37','77','01','41','02','42','03','43','04','44','05','45','10','50','11','51','12','52','13','53','14','54','20','60','21','61','22','62','23','63','30','70','31','71','32','72','08','48','07','47','17','57','06','46','16','56','26','66','36','76','15','55','25','65','35','75','24','64','34','74','33','73','49','80','40','80'];

  if($i == 0){
    $sentenca = "1 Ox1 A11v1 | 9X 49X";
    $sentencaA = "1 Ox1 A11v1 | 9X";
    $sentencaB = "1 Ox1 A11v1 | 49X";
    $t = verificador($dezenas, $verificadores[0], $verificadores[1], $sentenca, $sentencaA, $sentencaB);  
  }elseif($i == 1){
    $sentenca = "2 Ox1 A11v1 | 19X 59X";
    $sentencaA = "2 Ox1 A11v1 | 19X";
    $sentencaB = "2 Ox1 A11v1 | 59X";
    $t = verificador($dezenas, $verificadores[2], $verificadores[3], $sentenca, $sentencaA, $sentencaB);  
  }elseif($i == 2){
    $sentenca = "3 Ox1 A11v1 | 29X 69X";
    $sentencaA = "3 Ox1 A11v1 | 29X";
    $sentencaB = "3 Ox1 A11v1 | 69X";
    $t = verificador($dezenas, $verificadores[4], $verificadores[5], $sentenca, $sentencaA, $sentencaB);  
  }elseif($i == 3){
    $sentenca = "4 Ox1 A11v1 | 39X 79X";
    $sentencaA = "4 Ox1 A11v1 | 39X";
    $sentencaB = "4 Ox1 A11v1 | 79X";
    $t = verificador($dezenas, $verificadores[6], $verificadores[7], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 4){
    $sentenca = "1 OX1 A22v1 | 18X 58X";
    $sentencaA = "1 OX1 A22v1 | 18X";
    $sentencaB = "1 OX1 A22v1 | 58X";
    $t = verificador($dezenas, $verificadores[8], $verificadores[9], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 5){
    $sentenca = "2 OX1 A22v1 | 28X 68X";
    $sentencaA = "2 OX1 A22v1 | 28X";
    $sentencaB = "2 OX1 A22v1 | 68X";
    $t = verificador($dezenas, $verificadores[10], $verificadores[11], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 6){
    $sentenca = "3 OX1 A22v1 | 38X 78X";
    $sentencaA = "3 OX1 A22v1 | 38X";
    $sentencaB = "3 OX1 A22v1 | 78X";
    $t = verificador($dezenas, $verificadores[12], $verificadores[13], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 7){
    $sentenca = "1 OX1 A33v1 | 27X 67X";
    $sentencaA = "1 OX1 A33v1 | 27X";
    $sentencaB = "1 OX1 A33v1 | 67X";
    $t = verificador($dezenas, $verificadores[14], $verificadores[15], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 8){
    $sentenca = "2 OX1 A33v1 | 37X 77X";
    $sentencaA = "2 OX1 A33v1 | 37X";
    $sentencaB = "2 OX1 A33v1 | 77X";
    $t = verificador($dezenas, $verificadores[16], $verificadores[17], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 9){
    $sentenca = "1OX1 A51v1 | 1X 41X";
    $sentencaA = "1OX1 A51v1 | 1X";
    $sentencaB = "1OX1 A51v1 | 41X";
    $t = verificador($dezenas, $verificadores[18], $verificadores[19], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 10){
    $sentenca = "2OX1 A51v1 | 2X 42X";
    $sentencaA = "2OX1 A51v1 | 2X ";
    $sentencaB = "2OX1 A51v1 | 42X ";
    $t = verificador($dezenas, $verificadores[20], $verificadores[21], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 11){
    $sentenca = "3OX1 A51v1 | 3X 43X";
    $sentencaA = "3OX1 A51v1 | 3X";
    $sentencaB = "3OX1 A51v1 | 43X";
    $t = verificador($dezenas, $verificadores[22], $verificadores[23], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 12){
    $sentenca = "4OX1 A51v1 | 4X 44X";
    $sentencaA = "4OX1 A51v1 | 4X";
    $sentencaB = "4OX1 A51v1 | 44X";
    $t = verificador($dezenas, $verificadores[24], $verificadores[25], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 13){
    $sentenca = "5OX1 A51v1 | 5X 45X";
    $sentencaA = "5OX1 A51v1 | 5X";
    $sentencaB = "5OX1 A51v1 | 45X";
    $t = verificador($dezenas, $verificadores[26], $verificadores[27], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 14){
    $sentenca = "1OX1 A62v1 | 10X 50X";
    $sentencaA = "1OX1 A62v1 | 10X";
    $sentencaB = "1OX1 A62v1 | 50X";
    $t = verificador($dezenas, $verificadores[28], $verificadores[29], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 15){
    $sentenca = "2OX1 A62v1 | 11X 51X";
    $sentencaA = "2OX1 A62v1 | 11X";
    $sentencaB = "2OX1 A62v1 | 51X";
    $t = verificador($dezenas, $verificadores[30], $verificadores[31], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 16){
    $sentenca = "3OX1 A62v1 | 12X 52X";
    $sentencaA = "3OX1 A62v1 | 12X";
    $sentencaB = "3OX1 A62v1 | 52X";
    $t = verificador($dezenas, $verificadores[32], $verificadores[33], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 17){
    $sentenca = "4OX1 A62v1 | 13X 53X";
    $sentencaA = "4OX1 A62v1 | 13X";
    $sentencaB = "4OX1 A62v1 | 53X";
    $t = verificador($dezenas, $verificadores[34], $verificadores[35], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 18){
    $sentenca = "5OX1 A62v1 | 14X 54X";
    $sentencaA = "5OX1 A62v1 | 14X";
    $sentencaB = "5OX1 A62v1 | 54X";    
    $t = verificador($dezenas, $verificadores[36], $verificadores[37], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 19){
    $sentenca = "1OX1 A73v1 | 20X 60X";
    $sentencaA = "1OX1 A73v1 | 20X";
    $sentencaB = "1OX1 A73v1 | 60X";    
    $t = verificador($dezenas, $verificadores[38], $verificadores[39], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 20){
    $sentenca = "2OX1 A73v1 | 21X 61X";
    $sentencaA = "2OX1 A73v1 | 21X";
    $sentencaB = "2OX1 A73v1 | 61X";    
    $t = verificador($dezenas, $verificadores[40], $verificadores[41], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 21){
    $sentenca = "3OX1 A73v1 | 22X 62X";
    $sentencaA = "3OX1 A73v1 | 22X";
    $sentencaB = "3OX1 A73v1 | 62X";    
    $t = verificador($dezenas, $verificadores[42], $verificadores[43], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 22){
    $sentenca = "4OX1 A73v1 | 23X 63X";
    $sentencaA = "4OX1 A73v1 | 23X";
    $sentencaB = "4OX1 A73v1 | 63X";    
    $t = verificador($dezenas, $verificadores[44], $verificadores[45], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 23){
    $sentenca = "1OX1 A84v1 | 30X 70X";
    $sentencaA = "1OX1 A84v1 | 30X";
    $sentencaB = "1OX1 A84v1 | 70X";    
    $t = verificador($dezenas, $verificadores[46], $verificadores[47], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 24){
    $sentenca = "2OX1 A84v1 | 31X 71X";
    $sentencaA = "2OX1 A84v1 | 31X";
    $sentencaB = "2OX1 A84v1 | 71X";    
    $t = verificador($dezenas, $verificadores[48], $verificadores[49], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 25){
    $sentenca = "3OX1 A84v1 | 32X 72X";
    $sentencaA = "3OX1 A84v1 | 32X";
    $sentencaB = "3OX1 A84v1 | 72X";    
    $t = verificador($dezenas, $verificadores[50], $verificadores[51], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 26){
    $sentenca = "A21v1 | 8X 48X";
    $sentencaA = "A21v1 | 8X";
    $sentencaB = "A21v1 | 48X";    
    $t = verificador($dezenas, $verificadores[52], $verificadores[53], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 27){
    $sentenca = "A31v1 | 7X 47X";
    $sentencaA = "A31v1 | 7X";
    $sentencaB = "A31v1 | 47X";    
    $t = verificador($dezenas, $verificadores[54], $verificadores[55], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 28){
    $sentenca = "A32v1 | 17X 57X";
    $sentencaA = "A32v1 | 17X";
    $sentencaB = "A32v1 | 57X";    
    $t = verificador($dezenas, $verificadores[56], $verificadores[57], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 29){
    $sentenca = "A41v1 | 6X 46X";
    $sentencaA = "A41v1 | 6X";
    $sentencaB = "A41v1 | 46X";    
    $t = verificador($dezenas, $verificadores[58], $verificadores[59], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 30){
    $sentenca = "A42v1 | 16X 56X";
    $sentencaA = "A42v1 | 16X";
    $sentencaB = "A42v1 | 56X";    
    $t = verificador($dezenas, $verificadores[60], $verificadores[61], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 31){
    $sentenca = "A43v1 | 26X 66X";
    $sentencaA = "A43v1 | 26X";
    $sentencaB = "A43v1 | 66X";    
    $t = verificador($dezenas, $verificadores[62], $verificadores[63], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 32){
    $sentenca = "A44v1 | 36X 76X";
    $sentencaA = "A44v1 | 36X";
    $sentencaB = "A44v1 | 76X";    
    $t = verificador($dezenas, $verificadores[64], $verificadores[65], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 33){
    $sentenca = "A52v1 | 15X 55X";
    $sentencaA = "A52v1 | 15X";
    $sentencaB = "A52v1 | 55X";    
    $t = verificador($dezenas, $verificadores[66], $verificadores[67], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 34){
    $sentenca = "A53v1 | 25X 65X";
    $sentencaA = "A53v1 | 25X";
    $sentencaB = "A53v1 | 65X";    
    $t = verificador($dezenas, $verificadores[68], $verificadores[69], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 35){
    $sentenca = "A54v1 | 35X 75X";
    $sentencaA = "A54v1 | 35X";
    $sentencaB = "A54v1 | 75X";    
    $t = verificador($dezenas, $verificadores[70], $verificadores[71], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 36){
    $sentenca = "A63v1 | 24X 64X";
    $sentencaA = "A63v1 | 24X";
    $sentencaB = "A63v1 | 64X";    
    $t = verificador($dezenas, $verificadores[72], $verificadores[73], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 37){
    $sentenca = "A63v1 | 34X 74X";
    $sentencaA = "A63v1 | 34X";
    $sentencaB = "A63v1 | 74X";    
    $t = verificador($dezenas, $verificadores[74], $verificadores[75], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 38){
    $sentenca = "A74v1 | 33X 73X";
    $sentencaA = "A74v1 | 33X";
    $sentencaB = "A74v1 | 73X";    
    $t = verificador($dezenas, $verificadores[76], $verificadores[77], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 39){
    $sentenca = "A91v1 | 49X 80X";
    $sentencaA = "A91v1 | 49X";
    $sentencaB = "A91v1 | 80X";    
    $t = verificador($dezenas, $verificadores[78], $verificadores[79], $sentenca, $sentencaA, $sentencaB);  
  }
  elseif($i == 40){
    $sentenca = "A95v1 | 40X 80X";
    $sentencaA = "A95v1 | 40X";
    $sentencaB = "A95v1 | 80X";    
    $t = verificador($dezenas, $verificadores[80], $verificadores[81], $sentenca, $sentencaA, $sentencaB);  
  }
  if($t){
    $resultado = $t;    
  }else{
    $resultado = "Não Encontrado";
  }
  return $resultado;  
}






// function verificacaoGrupo($grupo, $grupo2){   
//   if($grupo.$grupo2 == "4909" || $grupo.$grupo2 == "0949"){
//     return "1 Ox1 A11v1 | 09X 49X"." - ".$grupo.$grupo2;
//   // }elseif($grupo.$grupo2 == "4909" || $grupo.$grupo2 == "0949" && $grupo == "49" || $grupo2 == "49"){
//   //   return "1 Ox1 A11v1 | 49X"." - ".$grupo.$grupo2;
//   // }elseif($grupo.$grupo2 == "4909" || $grupo.$grupo2 == "0949"){
//   //   return "1 Ox1 A11v1 | 09X 49X"." - ".$grupo.$grupo2;
//   }
//   if($grupo == '09' || $grupo2 == '09' )
//   // if($grupo == "09"){
//   //   if($grupo.$grupo2 == "0949" || $grupo.$grupo2 == "4909"){      
//   //     $resultado = "1 Ox1 A11v1 | 9X 49x ";      
//   //     return $resultado." Parametro: ".$grupo.$grupo2;
//   //   }
//   //   $resultado = "1 Ox1 A11v1 | 9X ";  
//   //   return $resultado." Parametro: ".$grupo.$grupo2;
//   // }
//   // elseif($grupo == "49" && $grupo.$grupo2 == "0949" || $grupo.$grupo2 == "4909"){    
//   //     $resultado = "1 Ox1 A11v1 | 9X 49x ";     
//   //     return $resultado." Parametro: ".$grupo.$grupo2;
//   //   }elseif($grupo == "49"){}
//   //   $resultado = "1 Ox1 A11v1 | 49X ";      
//   //   return $resultado." Parametro: ".$grupo.$grupo2;
//   // }  
//   else{   
//     return $grupo.$grupo2;
//   }
// }


// $resultadoBase = combinacoesDe(2,array_unique($dezenas));

// foreach ($resultadoBase as $resultado) {
//   // echo $resultado[0].$resultado[1].'<br>';
//   echo verificacaoGrupo($resultado[0], $resultado[1]).'<br>';
//   // $base = verificacaoGrupo(grupoBase(2,$resultado));   
//   // if($base){
//   //   echo $base.'<br>';
//   // }
// }

// // echo "<pre>";
// print_r($resultadoBase);
// echo "</pre>";


// print_r($list);
  // print_r($repeticoes);
  // var_dump($repeticoes);
  // foreach ($repeticoes as $repeticao) {
  //   if($repeticao > 1){
  //     $key = array_search($repeticao, $dezenas);
  //     // echo verificacaoGrupo($key).'<br>';
  //     echo $key.'<br>';
  //   }
  // }
  
// FINAL DO TRATAMENTO DE REPETICAO



 ?>