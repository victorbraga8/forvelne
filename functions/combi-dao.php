<?php 

function combinacoesDe($k, $xs){
  shuffle($xs);
    if ($k === 0)

        return array(array());

    if (count($xs) === 0)

        return array();

    $x = $xs[0];

    $xs1 = array_slice($xs,1,count($xs)-1);

    $res1 = combinacoesDe($k-1,$xs1);

    for ($i = 0; $i < count($res1); $i++) {

        array_splice($res1[$i], 0, 0, $x);

    }

    $res2 = combinacoesDe($k,$xs1);

    return array_merge($res1, $res2);

}


function verificaGrupoBase($elemento){
  if($elemento == 1){
    $resultado = ['a','e','h','j','o','t','x'];
  }elseif($elemento == 2){
    $resultado = ['b','f','i','k','p','v','y'];
  }elseif($elemento == 3){
    $resultado = ['c','g','l','q','v','z'];
  }elseif($elemento == 4){
    $resultado = ['d','m','r','w'];
  }elseif($elemento == 5){
    $resultado = ['n','s'];
  }
  shuffle($resultado);
  return $resultado;
}


 function verificaGrupo($elemento){

    if($elemento == 'a' || $elemento == 'e' || $elemento == 'h' || $elemento == 'j' || $elemento == 'o' || $elemento == 't' || $elemento == 'x'){

      return '1';

    }elseif($elemento == 'b' || $elemento == 'f' || $elemento == 'i' || $elemento == 'k' || $elemento == 'p' || $elemento == 'v' || $elemento == 'y'){

      return '2';

    }elseif($elemento == 'c' || $elemento == 'g' || $elemento == 'l' || $elemento == 'q' || $elemento == 'v' || $elemento == 'z'){

      return '3';

    }elseif($elemento == 'd' || $elemento == 'm' || $elemento == 'r' || $elemento == 'w'){

      return '4';

    }elseif($elemento == 'n' || $elemento == 's'){

      return '5';

    }     

  }

 

  function grupoBase($tamanho, $resultados){

    // shuffle($resultados); 

    if($tamanho == 1){

      return $resultados[0];

    }

    elseif($tamanho == 2){

      return $resultados[0].$resultados[1];

    }

    elseif($tamanho == 3){

      return $resultados[0].$resultados[1].$resultados[2];

    }      

    elseif($tamanho == 4){

      return $resultados[0].$resultados[1].$resultados[2].$resultados[3];

    }

    elseif($tamanho == 5){

      return $resultados[0].$resultados[1].$resultados[2].$resultados[3].$resultados[4];

    }

    elseif($tamanho == 6){

      return $resultados[0].$resultados[1].$resultados[2].$resultados[3].$resultados[4].$resultados[5] ;

    }



  }



  function montaGrupoValores($tamanho, $resultados){    

    if($tamanho == 1){

      return verificaGrupo($resultados[0]);

    }

    elseif($tamanho == 2){

      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]);

    }

    elseif($tamanho == 3){

      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]).verificaGrupo($resultados[2]);

    }      

    elseif($tamanho == 4){

      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]).verificaGrupo($resultados[2]).verificaGrupo($resultados[3]);

    }

    elseif($tamanho == 5){

      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]).verificaGrupo($resultados[2]).verificaGrupo($resultados[3]).verificaGrupo($resultados[4]);

    }

    elseif($tamanho == 6){

      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]).verificaGrupo($resultados[2]).verificaGrupo($resultados[3]).verificaGrupo($resultados[4]).verificaGrupo($resultados[5]);

    }



  }



  function somaGrupoValores($tamanho, $resultados){    

    if($tamanho == 1){

      return verificaGrupo($resultados[0]);

    }

    elseif($tamanho == 2){

      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1]);

    }

    elseif($tamanho == 3){

      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2]);

    }      

    elseif($tamanho == 4){

      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2])+verificaGrupo($resultados[3]);

    }

    elseif($tamanho == 5){

      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2])+verificaGrupo($resultados[3])+verificaGrupo($resultados[4]);

    }

    elseif($tamanho == 6){

      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2])+verificaGrupo($resultados[3])+verificaGrupo($resultados[4])+verificaGrupo($resultados[5]);

    }
  }

function montagemFinal($resultados, $elementos){
  $c = 0;  
  $array = array();
  foreach ($resultados as $resultado) {
    $array[$c][0] = somaGrupoValores($elementos, $resultado);    
    $array[$c][1] = montaGrupoValores($elementos, $resultado);    
    $array[$c][2] = grupoBase($elementos, $resultado);
    $c ++;
  }  
  return $array;
}

function grupoBaseReverso($tamanho, $resultados){
    // shuffle($resultados); 
    if($tamanho == 1){
      return $resultados[0];
    }
    elseif($tamanho == 2){
      return $resultados[1].$resultados[0];
    }
    elseif($tamanho == 3){
      return $resultados[2].$resultados[1].$resultados[0];
    }      
    elseif($tamanho == 4){
      return $resultados[3].$resultados[2].$resultados[1].$resultados[0];
    }
    elseif($tamanho == 5){
      return $resultados[4].$resultados[3].$resultados[2].$resultados[1].$resultados[0];
    }
  } 

function montaBase($elementos,$resultados){
  $arr = array();
  $i = 0;
  $y = 0;
  foreach ($resultados as $rs1) {   
    $arr['X'.$i] = grupoBase($elementos,$rs1);
    $arr['Y'.$y] = grupoBaseReverso($elementos,$rs1);
    $i++;
    $y++;     
  }  
  return $arr;  
}


 ?>