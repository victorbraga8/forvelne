<?php 

function combinacoesDe($k, $xs){
  shuffle($xs);
    if ($k === 0)

        return array(array());

    if (count($xs) === 0)

        return array();

    $x = $xs[0];

    $xs1 = array_slice($xs,1,count($xs)-1);

    $res1 = combinacoesDe($k-1,$xs1);

    for ($i = 0; $i < count($res1); $i++) {

        array_splice($res1[$i], 0, 0, $x);

    }

    $res2 = combinacoesDe($k,$xs1);

    return array_merge($res1, $res2);

}


function verificaGrupoBase($elemento){
  if($elemento == 1){
    $resultado = ['a','b','d','h','k','m','n'];
  }elseif($elemento == 2){
    $resultado = ['c','e','i','l'];
  }elseif($elemento == 3){
    $resultado = ['f','j'];
  }elseif($elemento == 4){
    $resultado = ['g'];
  }
  shuffle($resultado);
  return $resultado;
}


 function verificaGrupo($elemento){
    if($elemento == 'a' || $elemento == 'b' || $elemento == 'd' || $elemento == 'h' || $elemento == 'k' || $elemento == 'm' || $elemento == 'n'){
      return '1';
    }elseif($elemento == 'c' || $elemento == 'e' || $elemento == 'i' || $elemento == 'l'){
      return '2';
    }elseif($elemento == 'f' || $elemento == 'j'){
      return '3';
    }elseif($elemento == 'g'){
      return '4';
    }
  }

 

  function grupoBase($tamanho, $resultados){
    if($tamanho == 1){
      return $resultados[0];
    }
    elseif($tamanho == 2){
      return $resultados[0].$resultados[1];
    }
    elseif($tamanho == 3){
      return $resultados[0].$resultados[1].$resultados[2];
    }      
    elseif($tamanho == 4){
      return $resultados[0].$resultados[1].$resultados[2].$resultados[3];
    }
    elseif($tamanho == 5){
      return $resultados[0].$resultados[1].$resultados[2].$resultados[3].$resultados[4];
    } 
  }



  function montaGrupoValores($tamanho, $resultados){    

    if($tamanho == 1){

      return verificaGrupo($resultados[0]);

    }

    elseif($tamanho == 2){

      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]);

    }

    elseif($tamanho == 3){

      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]).verificaGrupo($resultados[2]);

    }      

    elseif($tamanho == 4){

      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]).verificaGrupo($resultados[2]).verificaGrupo($resultados[3]);

    }

    elseif($tamanho == 5){

      return verificaGrupo($resultados[0]).verificaGrupo($resultados[1]).verificaGrupo($resultados[2]).verificaGrupo($resultados[3]).verificaGrupo($resultados[4]);

    }
  }



  function somaGrupoValores($tamanho, $resultados){    

    if($tamanho == 1){

      return verificaGrupo($resultados[0]);

    }

    elseif($tamanho == 2){

      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1]);

    }

    elseif($tamanho == 3){

      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2]);

    }      

    elseif($tamanho == 4){

      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2])+verificaGrupo($resultados[3]);

    }

    elseif($tamanho == 5){

      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2])+verificaGrupo($resultados[3])+verificaGrupo($resultados[4]);

    }

    elseif($tamanho == 6){

      return verificaGrupo($resultados[0])+verificaGrupo($resultados[1])+verificaGrupo($resultados[2])+verificaGrupo($resultados[3])+verificaGrupo($resultados[4])+verificaGrupo($resultados[5]);

    }
  }

function montagemFinal($resultados, $elementos){
  $c = 0;  
  $array = array();
  foreach ($resultados as $resultado) {
    $array[$c][0] = somaGrupoValores($elementos, $resultado);    
    $array[$c][1] = montaGrupoValores($elementos, $resultado);    
    $array[$c][2] = grupoBase($elementos, $resultado);
    $c ++;
  }  
  return $array;
}

function grupoBaseReverso($tamanho, $resultados){
    // shuffle($resultados); 
    if($tamanho == 1){
      return $resultados[0];
    }
    elseif($tamanho == 2){
      return $resultados[1].$resultados[0];
    }
    elseif($tamanho == 3){
      return $resultados[2].$resultados[1].$resultados[0];
    }      
    elseif($tamanho == 4){
      return $resultados[3].$resultados[2].$resultados[1].$resultados[0];
    }
    elseif($tamanho == 5){
      return $resultados[4].$resultados[3].$resultados[2].$resultados[1].$resultados[0];
    }
  } 

function montaBase($elementos,$resultados){
  $arr = array();
  $i = 0;
  $y = 0;
  foreach ($resultados as $rs1) {   
    $arr['X'.$i] = grupoBase($elementos,$rs1);
    $arr['Y'.$y] = grupoBaseReverso($elementos,$rs1);
    $i++;
    $y++;     
  }  
  return $arr;  
}


 ?>