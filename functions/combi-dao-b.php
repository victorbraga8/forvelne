<?php 
function verificador($dezenas,$verificadorA, $verificadorB, $sentenca){
  if(in_array($verificadorA, $dezenas) && in_array($verificadorB, $dezenas)){
    $resultado = "<b>".$sentenca."</b>";
  }elseif(!in_array($verificadorA, $dezenas) || !in_array($verificadorB, $dezenas)){
    $k1 = array_search($verificadorA, $dezenas);
    $k2 = array_search($verificadorB, $dezenas);  
    if($k1){
      $resultado = "<b>".$sentenca."</b>";
    }elseif($k2){
      $resultado = "<b>".$sentenca."</b>";
    }      
  } 
  return $resultado;
}

function distribuicaoNumerica2($dezenas,$i){
array_unique($dezenas); 
// $verificadores = ['09','49','19','59','29','69','39','79','18','58','28','68','38','78','27','67','37','77','01','41','02','42','03','43','04','44','05','45','10','50','11','51','12','52','13','53','14','54','20','60','21','61','22','62','23','63','30','70','31','71','32','72'];

// Dados do segundo subgrupo abaixo
$verificadores = ['08','48','07','47','17','57','06','46','16','56','26','66','36','76','15','55','25','65','35','75','24','64','34','74','33','73','49','80','40','80'];

  if($i == 0){
    $sentenca = "A21V1 | 8X 48X";    
    $t = verificador($dezenas, $verificadores[0], $verificadores[1], $sentenca);  
  }elseif($i == 1){
    $sentenca = "A31V1 | 7X 47X";    
    $t = verificador($dezenas, $verificadores[2], $verificadores[3], $sentenca);  
  }elseif($i == 2){
    $sentenca = "A32V1 | 17X 57X";    
    $t = verificador($dezenas, $verificadores[4], $verificadores[5], $sentenca);  
  }elseif($i == 3){
    $sentenca = "A41V1 | 6X 46X";    
    $t = verificador($dezenas, $verificadores[6], $verificadores[7], $sentenca);  
  }
  elseif($i == 4){
    $sentenca = "A42V1 | 16X 56X";    
    $t = verificador($dezenas, $verificadores[8], $verificadores[9], $sentenca);  
  }
  elseif($i == 5){
    $sentenca = "A43V1 | 26X 66X";    
    $t = verificador($dezenas, $verificadores[10], $verificadores[11], $sentenca);  
  }
  elseif($i == 6){
    $sentenca = "A44V1 | 36X 76X";    
    $t = verificador($dezenas, $verificadores[12], $verificadores[13], $sentenca);  
  }
  elseif($i == 7){
    $sentenca = "A52V1 | 15X 55X";    
    $t = verificador($dezenas, $verificadores[14], $verificadores[15], $sentenca);  
  }
  elseif($i == 8){
    $sentenca = "A53V1 | 25X 65X";    
    $t = verificador($dezenas, $verificadores[16], $verificadores[17], $sentenca);  
  }
  elseif($i == 9){
    $sentenca = "A54V1 | 35X 75X";    
    $t = verificador($dezenas, $verificadores[18], $verificadores[19], $sentenca);  
  }
  elseif($i == 10){
    $sentenca = "A63V1 | 24X 64X";    
    $t = verificador($dezenas, $verificadores[20], $verificadores[21], $sentenca);  
  }
  elseif($i == 11){
    $sentenca = "A63V1 | 34X 74X";    
    $t = verificador($dezenas, $verificadores[22], $verificadores[23], $sentenca);  
  }
  elseif($i == 12){
    $sentenca = "A74V1 | 22X 73X";    
    $t = verificador($dezenas, $verificadores[24], $verificadores[25], $sentenca);  
  }
  elseif($i == 13){
    $sentenca = "A91V1 | 49X 80X";    
    $t = verificador($dezenas, $verificadores[26], $verificadores[27], $sentenca);  
  }
  elseif($i == 14){
    $sentenca = "A95V1 | 40X 80X";    
    $t = verificador($dezenas, $verificadores[28], $verificadores[29], $sentenca);  
  }  
  if($t){
    $resultado = $t;    
  }else{
    $resultado = "<u>Não Encontrado</u>";
  }
  return $resultado;  
}

function duplicados( $array ) {
$arr = array_count_values($array);
  foreach($arr as $key => $value){
      if($value > 1){
          $resultado = 'valor repetido: '. $key ." - ".$value.'<br>';
      }
  }
  return $resultado;
    // return array_unique( array_diff_assoc( $array, array_unique( $array ) ) );
}



 ?>