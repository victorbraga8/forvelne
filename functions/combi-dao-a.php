<?php 
require_once('functions/Combinations.php');
function montaGrupo($dezenas){

  $gpA = array();
  $gpB = array();

  $variaveisA = array();
  $variaveisB = array();

  $arrOxA = array();
  $arrOxB = array();

  // Separando quem é do Grupo A e do B
  foreach($dezenas as $dezena){
    if(strpos($dezena, "A") !== false ){
        $valor = explode(":", $dezena);                
        array_push($gpA, $valor[1]);
      }
      if(strpos($dezena, "B") !== false ){
        $valor = explode(":", $dezena);
        array_push($gpB, $valor[1]);        
      }    
  }

  if($gpA){
    $prodA = array();
    $variavelGpA = array();
    
    for($i=0; $i<count($gpA); $i++){    
      $final = distribuicaoNumerica($gpA[$i]);
        if($final){
          $valorVariavelA = explode("-", $final);
          array_push($prodA, $valorVariavelA[0]);
          array_push($variavelGpA, $valorVariavelA[0]." ".$valorVariavelA[1]);          
          $oxA = explode(" ", $valorVariavelA[1]);
          array_push($variaveisA, $oxA[1]);
          array_push($arrOxA, $valorVariavelA[0]." ".$oxA[0]);
        }  
    }

    foreach ($variavelGpA as $variaveis) {    
      $variavelA .= '('.$variaveis.') ';
    }

    $variavelA = substr($variavelA,0,-1);
    $prodA = array_product($prodA);
    $blocoA = "<p><span><i class='fas fa-check-circle' style='color:green;'></i> A</span> - ".$variavelA." = ".$prodA."</p>";
  }
  else{
    $blocoA = "<p'><span><i class='fas fa-times-circle' style='color:red;'></i> A</span> - <span style='font-weight:bold'>0</span></p>" ;
  }

  if($gpB){
    $prodB = array();
    $variavelGpB = array();

    for($i=0; $i<count($gpB); $i++){    
      $final = distribuicaoNumerica2($gpB[$i]);
        if($final){
          $valorVariavelB = explode("-", $final);
          array_push($prodB, $valorVariavelB[0]);
          array_push($variavelGpB, $valorVariavelB[0]." ".$valorVariavelB[1]);          
          $oxB = explode(" ", $valorVariavelB[1]);          
          array_push($variaveisB, $oxB[0]);
          array_push($arrOxB, $valorVariavelB[0]);
      }  
    }  

    foreach ($variavelGpB as $variaveis) {    
      $variavelB .= '('.$variaveis.') ';
    } 

    $variavelB = substr($variavelB,0,-1);
    $prodB = array_product($prodB);
    $blocoB = "<p><span><i class='fas fa-check-circle' style='color:green;'></i> B</span> - ".$variavelB." = ".$prodB."</p>";
  }else{
    $blocoB = "<p><span><i class='fas fa-times-circle' style='color:red;'></i> B</span> - <span style='font-weight:bold'>0</span></p>" ;
  }

  $arrTemp = [];  
  array_push($arrTemp, $blocoA);
  array_push($arrTemp, $variavelGpA);
  array_push($arrTemp, $blocoB);
  array_push($arrTemp, $variavelGpB);
  array_push($arrTemp, $variaveisA);
  array_push($arrTemp, $variaveisB);
  array_push($arrTemp, $arrOxA);
  array_push($arrTemp, $arrOxB);
  return $arrTemp;    
}

function distribuicaoNumerica2($dezenas){
  $verificador = ['08','4B8r3B4p7yhRXuBWLqsQ546WR43cqQwrbXMDFnBi6vSJBeif8tPW85a7r7DM961Jvk4hdryZoByEp8GC8HzsqJpRN4FxGM924','64','34','74','33','73','49','80','40','80'];

  if(in_array($dezenas, $verificador)){
    if($dezenas == "08" || $dezenas == "8"){
      $resultado = "1<span style='opacity:2;'>-</span>A21v1";
    }
    if($dezenas == "48"){
      $resultado = "1<span style='opacity:2;'>-</span>A21v1";
    }
    if($dezenas == "07" || $dezenas == "7"){
      $resultado = "1<span style='opacity:2;'>-</span>A31v1";
    }
    if($dezenas == "47"){
      $resultado = "1<span style='opacity:2;'>-</span>A31v1";
    }
    if($dezenas == "17"){
      $resultado = "2<span style='opacity:2;'>-</span>A32v1";
    }
    if($dezenas == "57"){
      $resultado = "2<span style='opacity:2;'>-</span>A32v1";
    }
    if($dezenas == "06" || $dezenas == "6"){
      $resultado = "1<span style='opacity:2;'>-</span>A41v1";
    }
    if($dezenas == "46"){
      $resultado = "1<span style='opacity:2;'>-</span>A41v1";
    }
    if($dezenas == "16"){
      $resultado = "2<span style='opacity:2;'>-</span>A42v1";
    }
    if($dezenas == "56"){
      $resultado = "2<span style='opacity:2;'>-</span>A42v1";
    }
    if($dezenas == "26"){
      $resultado = "3<span style='opacity:2;'>-</span>A43v1";
    }
    if($dezenas == "66"){
      $resultado = "3<span style='opacity:2;'>-</span>A43v1";
    }
    if($dezenas == "36"){
      $resultado = "4<span style='opacity:2;'>-</span>A44v1";
    }
    if($dezenas == "76"){
      $resultado = "4<span style='opacity:2;'>-</span>A44v1";
    }    
    if($dezenas == "15"){
      $resultado = "1<span style='opacity:2;'>-</span>A52v1";
    }
    if($dezenas == "55"){
      $resultado = "1<span style='opacity:2;'>-</span>A52v1";
    }
    if($dezenas == "25"){
      $resultado = "2<span style='opacity:2;'>-</span>A53v1";
    }
    if($dezenas == "65"){
      $resultado = "2<span style='opacity:2;'>-</span>A53v1";
    }
    if($dezenas == "35"){
      $resultado = "3<span style='opacity:2;'>-</span>A54v1";
    }
    if($dezenas == "75"){
      $resultado = "3<span style='opacity:2;'>-</span>A54v1";
    }
    if($dezenas == "24"){
      $resultado = "1<span style='opacity:2;'>-</span>A63v1";
    }
    if($dezenas == "64"){
      $resultado = "1<span style='opacity:2;'>-</span>A63v1";
    }    
    if($dezenas == "34"){
      $resultado = "1<span style='opacity:2;'>-</span>A63v1";
    }
    if($dezenas == "74"){
      $resultado = "1<span style='opacity:2;'>-</span>A63v1";
    }
    if($dezenas == "33"){
      $resultado = "1<span style='opacity:2;'>-</span>A74v1";
    }
    if($dezenas == "73"){
      $resultado = "1<span style='opacity:2;'>-</span>A74v1";
    }
    if($dezenas == "40"){
      $resultado = "1<span style='opacity:2;'>-</span>A95v1";
    }
    if($dezenas == "80"){
      $resultado = "1<span style='opacity:2;'>-</span>A95v1";
    }
  }
  return $resultado;
}

function distribuicaoNumerica($dezenas){
  $verificador = ['09','4B8r3B4p7yhRXuBWLqsQ546WR43cqQwrbXMDFnBi6vSJBeif8tPW85a7r7DM961Jvk4hdryZoByEp8GC8HzsqJpRN4FxGM902','4B8r3B4p7yhRXuBWLqsQ546WR43cqQwrbXMDFnBi6vSJBeif8tPW85a7r7DM961Jvk4hdryZoByEp8GC8HzsqJpRN4FxGM921','61','22','62','23','63','30','70','31','71','32','72'];

  if(in_array($dezenas, $verificador)){
    if($dezenas == "09" || $dezenas == "9"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "49"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "19"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "59"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "29"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "69"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "39"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "79"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "18"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A22v1";
    }
    if($dezenas == "58"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A22v1";
    }    
    if($dezenas == "28"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A22v1";
    }
    if($dezenas == "68"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A22v1";
    }    
    if($dezenas == "38"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A22v1";
    }
    if($dezenas == "78"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A22v1";
    }    
    if($dezenas == "27"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A33v1";
    }
    if($dezenas == "67"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A33v1";
    }    
    if($dezenas == "37"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A33v1";
    }
    if($dezenas == "77"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A33v1";
    }    
    if($dezenas == "1" || $dezenas == "01" ){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "41"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A51v1";
    }    
    if($dezenas == "2" || $dezenas == "02" ){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "42"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A51v1";
    }    
    if($dezenas == "3" || $dezenas == "03" ){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "43"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "4" || $dezenas == "04" ){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "44"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A51v1";
    }        
    if($dezenas == "5" || $dezenas == "05" ){
      $resultado = "5<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "45"){
      $resultado = "5<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "10"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "50"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A62v1";
    }        
    if($dezenas == "11"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "51"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A62v1";
    }        
    if($dezenas == "12"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "52"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A62v1";
    }        
    if($dezenas == "13"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "53"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A62v1";
    }        
    if($dezenas == "14"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "54"){
      $resultado = "5<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "20"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A73v1";
    }
    if($dezenas == "60"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A73v1";
    }             
    if($dezenas == "21"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A73v1";
    }
    if($dezenas == "61"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A73v1";
    }             
    if($dezenas == "22"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A73v1";
    }
    if($dezenas == "62"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A73v1";
    }
    if($dezenas == "23"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A73v1";
    }
    if($dezenas == "63"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A73v1";
    }               
    if($dezenas == "30"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A84v1";
    }
    if($dezenas == "70"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A84v1";
    }                        
    if($dezenas == "31"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A84v1";
    }
    if($dezenas == "71"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A84v1";
    }                
    if($dezenas == "32"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A84v1";
    }
    if($dezenas == "72"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A84v1";
    }                                
  }
  return $resultado;
}

function verificaGrupo($dezenas){
  $arr = array();

  $verificadorA = ['09','4B8r3B4p7yhRXuBWLqsQ546WR43cqQwrbXMDFnBi6vSJBeif8tPW85a7r7DM961Jvk4hdryZoByEp8GC8HzsqJpRN4FxGM902','4B8r3B4p7yhRXuBWLqsQ546WR43cqQwrbXMDFnBi6vSJBeif8tPW85a7r7DM961Jvk4hdryZoByEp8GC8HzsqJpRN4FxGM921','61','22','62','23','63','30','70','31','71','32','72'];

  $verificadorB = ['08','4B8r3B4p7yhRXuBWLqsQ546WR43cqQwrbXMDFnBi6vSJBeif8tPW85a7r7DM961Jvk4hdryZoByEp8GC8HzsqJpRN4FxGM924','64','34','74','33','73','80','40','80'];

  foreach ($dezenas as $valor){
    $gpA = in_array($valor, $verificadorA);
    $gpB = in_array($valor, $verificadorB);

    // Questionar sobre os valores quando tem correspondência nos dois subgrupos, como é o caso do 49.
    // Questionar também sobre quando existe repetição de valor no subgrupo, como é o caso do 80 no subgrupo 2

    if($gpA){
      $valor = "GrupoA:".$valor;      
       array_push($arr, $valor);
    }
    if($gpB){
      $valor = "GrupoB:".$valor;      
       array_push($arr, $valor);
    }
  }

  return $arr;
}

function primeFactors($n)
{
    $numeros = "";
    while($n % 2 == 0)
    {
        $n = $n / 2;
        $numeros .= "2,";
    }

    for($i = 3; $i <= sqrt($n); $i = $i + 2){
        while ($n % $i == 0){
            $numeros .= $i.",";
            $n = $n / $i;
        }
    }
 
    if ($n > 2){
        $numeros .= $n.",";
    }

    return mb_substr($numeros, 0, -1);
}

function montaQuintetoBase($grupoBase){
    $grupoQtd = count($grupoBase);
    $diferenca = 5 - $grupoQtd;

    if($diferenca){
      for($i=0; $i<$diferenca; $i++){
          array_push($grupoBase, 1);
      }
    }
    arsort($grupoBase);
    return $grupoBase;
}

function selecionaGrupo($controle){

    $grupo1 = ['1-A11v1', '1-A22v1', '1-A33v1', '1-A51v1', '1-A62v1', '1-A73v1', '1-A84v1'];
    $grupo2 = ['2-A11v1', '2-A22v1', '2-A33v1', '2-A51v1', '2-A62v1', '2-A73v1', '2-A84v1'];
    $grupo3 = ['3-A11v1', '3-A22v1', '3-A51v1', '3-A62v1', '3-A73v1', '3-A84v1'];
    $grupo5 = ['5-A51v1', '5-A62v1'];

    if($controle == 1){
      $grupoBase = $grupo1;
    }elseif($controle == 2){
      $grupoBase = $grupo2;
    }elseif($controle == 3){
      $grupoBase = $grupo3;
    }elseif($controle == 5){
      $grupoBase = $grupo5;
    }

    return $grupoBase;
}

function selecionaGrupo2($controle){

    $grupo1 = ['1-A21V1', '1-A31V1', '1-A41V1', '1-A52V1', '1-A63V1', '1-A95V1'];
    $grupo2 = ['2-A32V1', '2-A42V1', '2-A53V1', '2-A64V1'];
    $grupo3 = ['3-A43V1', '3-A54V1'];
    $grupo4 = ['4-A44V1'];

    if($controle == 1){
      $grupoBase = $grupo1;
    }elseif($controle == 2){
      $grupoBase = $grupo2;
    }elseif($controle == 3){
      $grupoBase = $grupo3;
    }elseif($controle == 4){
      $grupoBase = $grupo4;
    }

    return $grupoBase;
}

function verificaTamanho($grupoBase){
    $max = intval(max($grupoBase));
    if($max > 5) {
      return true;
    }else{
      return false;
    }
}

function verificaProdutorio($grupoBase){
    $grupoBase = explode(",", $grupoBase);
    $verificaTamanho = verificaTamanho($grupoBase);

    if(!$verificaTamanho){
      if(count($grupoBase) < 5){
        $resultado = montaQuintetoBase($grupoBase);
      }
      elseif(count($grupoBase) > 5){
         $resultado = montaQuintetoBase($grupoBase);
      }
      else{
        $resultado = $grupoBase;
      }
    }
  
    return $resultado; 
}

function escreveFatoracao($fatoracao){
  $i = 1;
  foreach ($fatoracao as $key => $value){
    if($i !== count($fatoracao)){
      $resultado .= $value.'-';
    }else{
      $resultado .= $value;
    }
    $i++;
  }
  return $resultado;
}

function escreveRepeticoes($repeticoes){
  $i = 1;
  foreach ($repeticoes as $key => $value){
    if($i !== count($repeticoes)){
      $resultado .= "Grupo: ".$key." = ".$value.'/ ';
    }else{
      $resultado .= "Grupo: ".$key." = ".$value;
    }
    $i++;
  }
  return $resultado;
}

function gerenciaRepeticoes($repeticoes, $tipoGrupo){

  $grupo1 = array();
  $grupo2 = array();
  $grupo3 = array();
  $grupo4 = array();
  $grupo5 = array();

  foreach($repeticoes as $chave => $valor){
      if($tipoGrupo){
        $Combinations = new Combinations(selecionaGrupo2($chave));
      }else{
        $Combinations = new Combinations(selecionaGrupo($chave));
      } 
      $permutations = $Combinations->getCombinations($valor, false);
      $controleTotal = count($permutations);
        
      if($controleTotal){
          $tamanhoArray = count($permutations[0])-1;
      }

      for($i=$controleTotal; $i >= 0; $i--){
          for($j=0; $j <= $tamanhoArray; $j++){
            if($chave == 1){
              array_push($grupo1, $permutations[$i][$j]);
            }elseif($chave == 2){
              array_push($grupo2, $permutations[$i][$j]);
            }elseif($chave == 3){
               array_push($grupo3, $permutations[$i][$j]);
            }elseif($chave == 4){
               array_push($grupo4, $permutations[$i][$j]);
            }
            elseif($chave == 5){
               array_push($grupo5, $permutations[$i][$j]);
            }   
            $resultado .= $permutations[$i][$j]." - ";
            if($j == $tamanhoArray){
              $resultado = "";
            }
          }
      }

      if($chave == 1){
        $grupo1 = array_chunk($grupo1, $valor);
      }elseif($chave == 2){
        $grupo2 = array_chunk($grupo2, $valor);
      }elseif($chave == 3){
        $grupo3 = array_chunk($grupo3, $valor);
      }elseif($chave == 4){
        $grupo3 = array_chunk($grupo4, $valor);
      }elseif($chave == 5){
        $grupo5 = array_chunk($grupo5, $valor);
      }
  }
  $arr = array_merge_recursive($grupo1, $grupo2, $grupo3, $grupo4, $grupo5);
  return $arr;
}

  function montaQuintetoFinal($arr, $grupos){

    $qtdGrupos = count($grupos);
    $tamanhoGrupos = array_sum($grupos);

    $Combinations = new Combinations($arr);
    $permutations = $Combinations->getCombinations($qtdGrupos, false);
    $permKey = $permutations;

    $arrT = array();

    for($i=0; $i < count($permKey); $i++){ 
      for($j=0; $j < count($permKey[$i]); $j++){ 
        array_push($arrT, count($permKey[$i][$j]));
      }
      $resultado = array_sum($arrT);
      $arrT = array();
      if($tamanhoGrupos <= 5){
        if($resultado > 5){
          unset($permutations[$i]);
        }
      }

    }

    $arrS = array();
    foreach ($permutations as $keys => $value){
      foreach ($value as $key => $values){
        foreach ($values as $key => $variaveis){
          $variavel = explode('-',$variaveis);
          array_push($arrS, $variavel[0]);
        }
      }
    }

    return $permutations;
  }

  function escreveQuintetoFinal($arr, $condicao, $grupos){
    $tamanhoGrupos = array_sum($grupos);


    $arrS = array();
    $arrResult = array();
    foreach ($arr as $key => $arrs){
      foreach ($arrs as $key => $variavel){
        array_push($arrResult, str_replace("-", "", $variavel)." - ");
        $variavel = explode('-',$variavel);
        array_push($arrS, $variavel[0]);    
      }
    }
    if(array_product($arrS) == intval($condicao)){
      $resultado = $arrResult; 
      if(intval($tamanhoGrupos) > 5){
        unset($resultado[0]);
      }
      return $resultado;
    }else{
      $arrS = array();
    }

  }

  function escreveResultado($grupos){
    foreach($grupos as $key => $variavel){
      $resultado .= str_replace("-", "", $variavel)." - ";
    }
    $resultado = substr($resultado,0,-2);
    return $resultado;
  }

function combinacoesDe($k, $xs){
    shuffle($xs);
    if ($k === 0)
        return array(array());
    if (count($xs) === 0)
        return array();
    $x = $xs[0];
    $xs1 = array_slice($xs,1,count($xs)-1);
    $res1 = combinacoesDe($k-1,$xs1);
    for ($i = 0; $i < count($res1); $i++) {
        array_splice($res1[$i], 0, 0, $x);
    }
    $res2 = combinacoesDe($k,$xs1);
    // sort($res1);
    // sort($res2);
    return array_merge($res1, $res2);
}

function tn2AOx($tn2AOX){
      
      $verificadores = ["1 OX1,1 OX1","1 OX1,2 OX1","2 OX1,1 OX1","1 OX1,3 OX1","3 OX1,1 OX1","1 OX1,4 OX1","4 OX1,1 OX1","1 OX1,5 OX1","5 OX1,1 OX1","2 OX1,2 OX1","2 OX1,3 OX1","3 OX1,2 OX1","2 OX1,4 OX1","4 OX1,2 OX1","2 OX1,5 OX1","5 OX1,2 OX1","3 OX1,3 OX1","3 OX1,4 OX1","4 OX1,3 OX1","3 OX1,5 OX1","5 OX1,3 OX1","4 OX1,4 OX1","4 OX1,5 OX1","5 OX1,4 OX1","5 OX1,5 OX1"];

      $valoresTn = ["1"=>"1 OX1,1 OX1","2"=>"1 OX1,2 OX1","2AN"=>"2 OX1,1 OX1","3"=>"1 OX1,3 OX1","3AN"=>"3 OX1,1 OX1","4"=>"1 OX1,4 OX1","4AN"=>"4 OX1,1 OX1","5"=>"1 OX1,5 OX1","5AN"=>"5 OX1,1 OX1","6"=>"2 OX1,2 OX1","7"=>"2 OX1,3 OX1","7AN"=>"3 OX1,2 OX1","8"=>"2 OX1,4 OX1","8AN"=>"4 OX1,2 OX1","9"=>"2 OX1,5 OX1","9AN"=>"5 OX1,2 OX1","10"=>"3 OX1,3 OX1","11"=>"3 OX1,4 OX1","11AN"=>"4 OX1,3 OX1","12"=>"3 OX1,5 OX1","12AN"=>"5 OX1,3 OX1","13"=>"4 OX1,4 OX1","14"=>"4 OX1,5 OX1","14AN"=>"5 OX1,4 OX1","15"=>"5 OX1,5 OX1"]; 

      $valorFinal = ["1"=>"1 OX1,1 OX1","2"=>"1 OX1,2 OX1","2AN"=>"2 OX1,1 OX1","3"=>"1 OX1,3 OX1","3AN"=>"3 OX1,1 OX1","4"=>"1 OX1,4 OX1","4AN"=>"4 OX1,1 OX1","5"=>"1 OX1,5 OX1","5AN"=>"5 OX1,1 OX1","6"=>"2 OX1,2 OX1","7"=>"2 OX1,3 OX1","7AN"=>"3 OX1,2 OX1","8"=>"2 OX1,4 OX1","8AN"=>"4 OX1,2 OX1","9"=>"2 OX1,5 OX1","9AN"=>"5 OX1,2 OX1","10"=>"3 OX1,3 OX1","11"=>"3 OX1,4 OX1","11AN"=>"4 OX1,3 OX1","12"=>"3 OX1,5 OX1","12AN"=>"5 OX1,3 OX1","13"=>"4 OX1,4 OX1","14"=>"4 OX1,5 OX1","14AN"=>"5 OX1,4 OX1","15"=>"5 OX1,5 OX1"]; 

      $tnGerado = [];
      $arrTemp = [];

    foreach ($tn2AOX as $key => $value){             
        $chave = strip_tags(strtoupper(trim($value[0]).",".trim($value[1])));                    
        if(in_array($chave, $verificadores)){          
          $chaveTn = array_search($chave, $verificadores);          
          $tnEncontrado = $verificadores[$chaveTn];          
          $valorTn = array_search($tnEncontrado, $valoresTn);          
          $tnFinal = $valorFinal[$valorTn].'<br>';                    
          $tnGerado[strip_tags(trim($tnFinal))] = $valorTn." - ".$tnFinal;                  
      }
    }   
      return $tnGerado;      
  }


function tn2B($tn2){

  $verificadores = [
      "A21V1,A31V1","A31V1,A21V1",
      "A21V1,A41V1","A41V1,A21V1",
      "A21V1,A52V1","A52V1,A21V1",
      "A21V1,A63V1","A63V1,A21V1",
      "A21V1,A74V1","A74V1,A21V1",
      "A21V1,A95V1","A95V1,A21V1",
      "A31V1,A41V1","A41V1,A31V1",
      "A31V1,A52V1","A52V1,A31V1",
      "A31V1,A63V1","A63V1,A31V1",
      "A31V1,A74V1","A74V1,A31V1",
      "A31V1,A95V1","A95V1,A31V1",
      "A41V1,A52V1","A52V1,A41V1",
      "A41V1,A63V1","A63V1,A41V1",
      "A41V1,A74V1","A74V1,A41V1",
      "A41V1,A95V1","A95V1,A41V1",
      "A52V1,A63V1","A63V1,A52V1",
      "A52V1,A74V1","A74V1,A52V1",
      "A52V1,A95V1","A95V1,A52V1",
      "A63V1,A74V1","A74V1,A63V1",
      "A63V1,A95V1","A95V1,A63V1",
      "A74V1,A95V1","A95V1,A74V1",
      "A41V1,A41V1","A31V1,A32V1",
      "A32V1,A31V1"
      ];

      $valoresTn = [
        "1"=>"A21V1,A31V1","1AN"=>"A31V1,A21V1",
        "2"=>"A21V1,A41V1","2AN"=>"A41V1,A21V1",
        "3"=>"A21V1,A52V1","3AN"=>"A52V1,A21V1",
        "4"=>"A21V1,A63V1","4AN"=>"A63V1,A21V1",
        "5"=>"A21V1,A74V1","5AN"=>"A74V1,A21V1",
        "6"=>"A21V1,A95V1","6AN"=>"A95V1,A21V1",
        "7"=>"A31V1,A32V1", "7AN"=>"A32V1,A31V1",
        "8"=>"A31V1,A41V1","8AN"=>"A41V1,A31V1",
        "9"=>"A31V1,A52V1","9AN"=>"A52V1,A31V1",
        "10"=>"A31V1,A63V1","10AN"=>"A63V1,A31V1",
        "11"=>"A31V1,A74V1","11AN"=>"A74V1,A31V1",
        "12"=>"A31V1,A95V1","12AN"=>"A95V1,A31V1",
        "13"=>"A41V1,A41V1",
        "14"=>"A41V1,A52V1","14AN"=>"A52V1,A41V1",
        "15"=>"A41V1,A63V1","15AN"=>"A63V1,A41V1",
        "16"=>"A41V1,A74V1","16AN"=>"A74V1,A41V1",
        "17"=>"A41V1,A95V1","17AN"=>"A95V1,A41V1",
        "18"=>"A52V1,A52V1",
        "19"=>"A52V1,A63V1","19AN"=>"A63V1,A52V1",
        "20"=>"A52V1,A74V1","20AN"=>"A74V1,A52V1",
        "21"=>"A52V1,A95V1","21AN"=>"A95V1,A52V1",
        "22"=>"A63V1,A63V1",
        "23"=>"A63V1,A74V1","23AN"=>"A74V1,A63V1",
        "24"=>"A63V1,A95V1","24AN"=>"A95V1,A63V1",
        "25"=>"A74V1,A95V1","25AN"=>"A95V1,A74V1",
        ]; 

      $valorFinal = [
        "1"=>"21V1,31V1","1AN"=>"31V1,21V1",
        "2"=>"21V1,41V1","2AN"=>"41V1,21V1",
        "3"=>"21V1,52V1","3AN"=>"52V1,21V1",
        "4"=>" 21V1,63V1","4AN"=>"63V1,21V1",
        "5"=>"21V1,74V1","5AN"=>"74V1,21V1",
        "6"=>"21V1,95V1","6AN"=>"95V1,21V1",
        "7"=>"31V1,32V1","7AN"=>"32V1,31V1",
        "8"=>"31V1,41V1","8AN"=>"41V1,31V1",
        "9"=>"31V1,52V1","9AN"=>"52V1,31V1",
        "10"=>"31V1,63V1","10AN"=>"63V1,31V1",
        "11"=>"31V1,74V1","11AN"=>"74V1,31V1",
        "12"=>"31V1,95V1","12AN"=>"95V1,31V1",
        "13"=>"41V1,41V1",
        "14"=>"41V1,52V1","14AN"=>"52V1,41V1",
        "15"=>"41V1,63V1","15AN"=>"63V1,41V1",
        "16"=>"41V1,74V1","16AN"=>"74V1,41V1",
        "17"=>"41V1,95V1","17AN"=>"95V1,41V1",
        "18"=>"52V1,52V1",
        "19"=>"52V1,63V1","19AN"=>"63V1,52V1",
        "20"=>"52V1,74V1","20AN"=>"74V1,52V1",
        "21"=>"52V1,95V1","21AN"=>"95V1,52V1",
        "22"=>"63V1,63V1",
        "23"=>"63V1,74V1","23AN"=>"74V1,63V1",
        "24"=>"63V1,95V1","24AN"=>"95V1,63V1",
        "25"=>"74V1,95V1","25AN"=>"95V1,74V1",
        ];

      $tnGerado = [];
      $arrTemp = [];

      foreach ($tn2 as $key => $value){                
        // if(mb_strpos($value[0], '32') !== false){
        //   $value[0] = 'A31V1';
        // }

        if(mb_strpos($value[0], '42') !== false){
          $value[0] = 'A41V1';
        }
        if(mb_strpos($value[0], '43') !== false){
          $value[0] = 'A41V1';
        }
        if(mb_strpos($value[0], '44') !== false){
          $value[0] = 'A41V1';
        }
        if(mb_strpos($value[0], '53') !== false){
          $value[0] = 'A52V1';
        }
        if(mb_strpos($value[0], '54') !== false){
          $value[0] = 'A52V1';
        }
        if(mb_strpos($value[0], '64') !== false){
          $value[0] = 'A63V1';
        }  

           
        // if(mb_strpos($value[1], '32') !== false){
        //   $value[1] = 'A31V1';
        // }
        if(mb_strpos($value[1], '42') !== false){
          $value[1] = 'A41V1';
        }
        if(mb_strpos($value[1], '43') !== false){
          $value[1] = 'A41V1';
        }
        if(mb_strpos($value[1], '44') !== false){
          $value[1] = 'A41V1';
        } 
        if(mb_strpos($value[1], '53') !== false){
          $value[1] = 'A52V1';
        }
        if(mb_strpos($value[1], '54') !== false){
          $value[1] = 'A52V1';
        }

        if(mb_strpos($value[1], '64') !== false){
          $value[1] = 'A63V1';
        }                            

        $chave = strtoupper($value[0].",".$value[1]);  
        // echo $chave.'<br>';
        if(in_array(strip_tags(trim($chave)), $verificadores)){
          $chaveTn = array_search(strip_tags(trim($chave)), $verificadores);          
          $tnEncontrado = $verificadores[$chaveTn];              
          $valorTn = array_search($tnEncontrado, $valoresTn);          
          $tnFinal = $valorFinal[$valorTn].'<br>';                    
          $tnGerado[strip_tags(trim($tnFinal))] = $valorTn." - ".$tnFinal;          
        }
      }

      return $tnGerado;      
  }

  function tn2A($tn2){
      
      $verificadores = ["A11V1,A11V1","A11V1,A22V1","A22V1,A11V1","A11V1,A33V1","A33V1,A11V1","A11V1,A51V1","A51V1,A11V1","A11V1,A62V1","A62V1,A11V1","A11V1,A73V1","A73V1,A11V1","A11V1,A84V1","A84V1,A11V1","A22V1,A22V1","A22V1,A22V1","A22V1,A33V1","A33V1,A22V1","A22V1,A51V1","A51V1,A22V1","A22V1,A62V1","A62V1,A22V1","A22V1,A73V1","A73V1,A22V1","A22V1,A84V1","A84V1,A22V1","A33V1,A33V1","A33V1,A33V1","A33V1,A51V1","A51V1,A33V1","A33V1,A62V1","A62V1,A33V1","A33V1,A73V1","A73V1,A33V1", "A33V1,A84V1","A84V1,A33V1","A51V1,A51V1","A51V1,A51V1","A51V1,A62V1","A62V1,A51V1","A51V1,A73V1","A73V1,A51V1","A51V1,A84V1","A84V1,A51V1","A62V1,A62V1", "A62V1,A62V1","A62V1,A73V1","A73V1,A62V1","A62V1,A84V1","A84V1,A62V1","A73V1,A73V1","A73V1,A73V1","A73V1,A84V1","A84V1,A73V1","A84V1,A84V1","A84V1,A84V1"];

      $valoresTn = ["1"=>"A11V1,A11V1","1AN"=>"A11V1,A11V1","2"=>"A11V1,A22V1","2AN"=>"A22V1,A11V1","3"=>"A11V1,A33V1","3AN"=>"A33V1,A11V1","4"=>"A11V1,A51V1","4AN"=>"A51V1,A11V1","5"=>"A11V1,A62V1","5AN"=>"A62V1,A11V1","6"=>"A11V1,A73V1","6AN"=>"A73V1,A11V1","7"=>"A11V1,A84V1","7AN"=>"A84V1,A11V1","8"=>"A22V1,A22V1","8AN"=>"A22V1,A22V1","9"=>"A22V1,A33V1","9AN"=>"A33V1,A22V1","10"=>"A22V1,A51V1","10AN"=>"A51V1,A22V1","11"=>"A22V1,A62V1","11AN"=>"A62V1,A22V1","12"=>"A22V1,A73V1","12AN"=>"A73V1,A22V1","13"=>"A22V1,A84V1","13AN"=>"A84V1,A22V1","14"=>"A33V1,A33V1","14AN"=>"A33V1,A33V1","15"=>"A33V1,A51V1","15AN"=>"A51V1,A33V1","16"=>"A33V1,A62V1","16AN"=>"A62V1,A33V1","17"=>"A33V1,A73V1","17AN"=>"A73V1,A33V1","18"=>"A33V1,A84V1","18AN"=>"A84V1,A33V1","19"=>"A51V1,A51V1","19AN"=>"A51V1,A51V1","20"=>"A51V1,A62V1","20AN"=>"A62V1,A51V1","21"=>"A51V1,A73V1","21AN"=>"A73V1,A51V1","22"=>"A51V1,A84V1","22AN"=>"A84V1,A51V1","23"=>"A62V1,A62V1","24"=>"A62V1,A73V1","24AN"=>"A73V1,A62V1","25"=>"A62V1,A84V1","25AN"=>"A84V1,A62V1","26"=>"A73V1,A73V1","26AN"=>"A73V1,A73V1","27"=>"A73V1,A84V1","27AN"=>"A84V1,A73V1","28"=>"A84V1,A84V1","28AN"=>"A84V1,A84V1"]; 

        $valorFinal = ["1"=>"A11V1,A11V1","1AN"=>"A11V1,A11V1","2"=>"A11V1,A22V1","2AN"=>"A22V1,A11V1","3"=>"A11V1,A33V1","3AN"=>"A33V1,A11V1","4"=>"A11V1,A51V1","4AN"=>"A51V1,A11V1","5"=>"A11V1,A62V1","5AN"=>"A11V1,A62V1","6"=>"A11V1,A73V1","6AN"=>"A73V1,A11V1","7"=>"A11V1,A84V1","7AN"=>"A84V1,A11V1","8"=>"A22V1,A22V1","8AN"=>"A22V1,A22V1","9"=>"A22V1,A33V1","9AN"=>"A33V1,A22V1","10"=>"A22V1,A51V1","10AN"=>"A51V1,A22V1","11"=>"A22V1,A62V1","11AN"=>"A62V1,A22V1","12"=>"A22V1,A73V1","12AN"=>"A73V1,A22V1","13"=>"A22V1,A84V1","13AN"=>"A84V1,A22V1","14"=>"A33V1,A33V1","14AN"=>"A33V1,A33V1","15"=>"A33V1,A51V1","15AN"=>"A51V1,A33V1","16"=>"A33V1,A62V1","16AN"=>"A62V1,A33V1","17"=>"A33V1,A73V1","17AN"=>"A73V1,A33V1","18"=>"A33V1,A84V1","18AN"=>"A84V1,A33V1","19"=>"A51V1,A51V1","19AN"=>"A51V1,A51V1","20"=>"A51V1,A62V1","20AN"=>"A62V1,A51V1","21"=>"A51V1,A73V1","21AN"=>"A73V1,A51V1","22"=>"A51V1,A84V1","22AN"=>"A84V1,A51V1","23"=>"A62V1,A62V1","24"=>"A62V1,A73V1","24AN"=>"A73V1,A62V1","25"=>"A62V1,A84V1","25AN"=>"A84V1,A62V1","26"=>"A73V1,A73V1","26AN"=>"A73V1,A73V1","27"=>"A73V1,A84V1","27AN"=>"A84V1,A73V1","28"=>"A84V1,A84V1","28AN"=>"A84V1,A84V1"]; 

      $tnGerado = [];
      $arrTemp = [];

      foreach ($tn2 as $key => $value){        
        $chave = strtoupper($value[0].",".$value[1]);             
        if(in_array($chave, $verificadores)){
          $chaveTn = array_search($chave, $verificadores);          
          $tnEncontrado = $verificadores[$chaveTn];          
          $valorTn = array_search($tnEncontrado, $valoresTn);          
          $tnFinal = $valorFinal[$valorTn].'<br>';                    
          $tnGerado[strip_tags(trim($tnFinal))] = $valorTn." - ".$tnFinal;          
        }
      }

      // echo "<pre>";
      // print_r($tnGerado);
      // echo "</pre>";

      return $tnGerado;      
  }

  function tn2AChaves($tn2){
      
      $verificadores = ["A11V1,A11V1","A11V1,A22V1","A22V1,A11V1","A11V1,A33V1","A33V1,A11V1",
      "A11V1,A51V1","A51V1,A11V1","A11V1,A62V1","A62V1,A11V1","A11V1,A73V1","A73V1,A11V1","A11V1,A84V1","A84V1,A11V1","A22V1,A22V1","A22V1,A22V1","A22V1,A33V1","A33V1,A22V1","A22V1,A51V1","A51V1,A22V1","A22V1,A62V1","A62V1,A22V1","A22V1,A73V1","A73V1,A22V1","A22V1,A84V1","A84V1,A22V1","A33V1,A33V1","A33V1,A33V1","A33V1,A51V1","A51V1,A33V1","A33V1,A62V1","A62V1,A33V1","A33V1,A73V1","A73V1,A33V1", "A33V1,A84V1","A84V1,A33V1","A51V1,A51V1","A51V1,A51V1","A51V1,A62V1","A62V1,A51V1","A51V1,A73V1","A73V1,A51V1","A51V1,A84V1","A84V1,A51V1","A62V1,A62V1", "A62V1,A62V1","A62V1,A73V1","A73V1,A62V1","A62V1,A84V1","A84V1,A62V1","A73V1,A73V1","A73V1,A73V1","A73V1,A84V1","A84V1,A73V1","A84V1,A84V1","A84V1,A84V1"];

      $valoresTn = ["1"=>"A11V1,A11V1","1AN"=>"A11V1,A11V1","2"=>"A11V1,A22V1","2AN"=>"A22V1,A11V1","3"=>"A11V1,A33V1","3AN"=>"A33V1,A11V1","4"=>"A11V1,A51V1","4AN"=>"A51V1,A11V1","5"=>"A11V1,A62V1","5AN"=>"A62V1,A11V1","6"=>"A11V1,A73V1","6AN"=>"A73V1,A11V1","7"=>"A11V1,A84V1","7AN"=>"A84V1,A11V1","8"=>"A22V1,A22V1","8AN"=>"A22V1,A22V1","9"=>"A22V1,A33V1","9AN"=>"A33V1,A22V1","10"=>"A22V1,A51V1","10AN"=>"A51V1,A22V1","11"=>"A22V1,A62V1","11AN"=>"A62V1,A22V1","12"=>"A22V1,A73V1","12AN"=>"A73V1,A22V1","13"=>"A22V1,A84V1","13AN"=>"A84V1,A22V1","14"=>"A33V1,A33V1","14AN"=>"A33V1,A33V1","15"=>"A33V1,A51V1","15AN"=>"A51V1,A33V1","16"=>"A33V1,A62V1","16AN"=>"A62V1,A33V1","17"=>"A33V1,A73V1","17AN"=>"A73V1,A33V1","18"=>"A33V1,A84V1","18AN"=>"A84V1,A33V1","19"=>"A51V1,A51V1","19AN"=>"A51V1,A51V1","20"=>"A51V1,A62V1","20AN"=>"A62V1,A51V1","21"=>"A51V1,A73V1","21AN"=>"A73V1,A51V1","22"=>"A51V1,A84V1","22AN"=>"A84V1,A51V1","23"=>"A62V1,A62V1","24"=>"A62V1,A73V1","24AN"=>"A73V1,A62V1","25"=>"A62V1,A84V1","25AN"=>"A84V1,A62V1","26"=>"A73V1,A73V1","26AN"=>"A73V1,A73V1","27"=>"A73V1,A84V1","27AN"=>"A84V1,A73V1","28"=>"A84V1,A84V1","28AN"=>"A84V1,A84V1"]; 

      $valorFinal = ["1"=>"A11V1,A11V1","1AN"=>"A11V1,A11V1","2"=>"A11V1,A22V1","2AN"=>"A22V1,A11V1","3"=>"A11V1,A33V1","3AN"=>"A33V1,A11V1","4"=>"A11V1,A51V1","4AN"=>"A51V1,A11V1","5"=>"A11V1,A62V1","5AN"=>"A11V1,A62V1","6"=>"A11V1,A73V1","6AN"=>"A73V1,A11V1","7"=>"A11V1,A84V1","7AN"=>"A84V1,A11V1","8"=>"A22V1,A22V1","8AN"=>"A22V1,A22V1","9"=>"A22V1,A33V1","9AN"=>"A33V1,A22V1","10"=>"A22V1,A51V1","10AN"=>"A51V1,A22V1","11"=>"A22V1,A62V1","11AN"=>"A62V1,A22V1","12"=>"A22V1,A73V1","12AN"=>"A73V1,A22V1","13"=>"A22V1,A84V1","13AN"=>"A84V1,A22V1","14"=>"A33V1,A33V1","14AN"=>"A33V1,A33V1","15"=>"A33V1,A51V1","15AN"=>"A51V1,A33V1","16"=>"A33V1,A62V1","16AN"=>"A62V1,A33V1","17"=>"A33V1,A73V1","17AN"=>"A73V1,A33V1","18"=>"A33V1,A84V1","18AN"=>"A84V1,A33V1","19"=>"A51V1,A51V1","19AN"=>"A51V1,A51V1","20"=>"A51V1,A62V1","20AN"=>"A62V1,A51V1","21"=>"A51V1,A73V1","21AN"=>"A73V1,A51V1","22"=>"A51V1,A84V1","22AN"=>"A84V1,A51V1","23"=>"A62V1,A62V1","24"=>"A62V1,A73V1","24AN"=>"A73V1,A62V1","25"=>"A62V1,A84V1","25AN"=>"A84V1,A62V1","26"=>"A73V1,A73V1","26AN"=>"A73V1,A73V1","27"=>"A73V1,A84V1","27AN"=>"A84V1,A73V1","28"=>"A84V1,A84V1","28AN"=>"A84V1,A84V1"]; 


      $tnGerado = [];
      $arrTemp = [];
      
      foreach ($tn2 as $key => $value){        
        $chave = strtoupper($value[0].",".$value[1]);             
        if(in_array($chave, $verificadores)){
          $chaveTn = array_search($chave, $verificadores);          
          $tnEncontrado = $verificadores[$chaveTn];          
          $valorTn = array_search($tnEncontrado, $valoresTn);          
          $tnFinal = $valorFinal[$valorTn].'<br>';                    
          $tnGerado[$tnFinal] = $tnFinal;          
        }
      }

      return $tnGerado;      
  }

  // Inicio Área Sub A
  function montaChave($controle){
    $chaveMontada = [];

    // echo "<pre>";
    // print_r($controle);
    // echo "</pre>";
    
    // Inicio etapa 19,15 - 15,11
    if(in_array("19", $controle) && in_array("15", $controle) && in_array("11", $controle)){
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn19-Hn15] = 1A12v1 = 1A(1,2)V1");
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn15,Hn11] = 2A22v1 = 2A(2,2)V1");
    }
    if(in_array("19", $controle) && in_array("15", $controle) && !in_array("11", $controle)){
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn19-Hn15] = 1A12v1 = 1A(1,2)V1");      
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn15] = 2A22v1 = 2A(2,0)V1");      
    }
    if(!in_array("19", $controle) && in_array("15", $controle) && in_array("11", $controle)){
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn15,Hn11] = 2A22v1 = 2A(2,2)V1");      
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn15] = 1A12v1 = 1A(0,2)V1");      
    }    
    if(!in_array("19", $controle) && in_array("15", $controle) && !in_array("11", $controle)){
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn15] = 1A12v1 = 1A(0,2)V1");      
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn15] = 2A22v1 = 2A(2,0)V1");      
    }
    if(in_array("19", $controle) && !in_array("15", $controle) && !in_array("11", $controle)){
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn19] = 1A12v1 = 1A(1,0)V1");      
    }   
    if(!in_array("19", $controle) && !in_array("15", $controle) && in_array("11", $controle)){
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn11] = 2A22v1 = 2A(0,2)V1");      
    }
    if(in_array("19", $controle) && !in_array("15", $controle) && in_array("11", $controle)){
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn19] = 1A12v1 = 1A(1,0)V1");      
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn11] = 2A22v1 = 2A(0,2)V1");      
    }

    // Inicio etapa 28,24 - 24,20
    if(in_array("28", $controle) && in_array("24", $controle) && in_array("20", $controle)){
      array_push($chaveMontada, strip_tags("(Hn10-Hn6) = [Hn28-Hn24] = 1A23v1 = 1A(2,2)V1"));      
      array_push($chaveMontada, strip_tags("(Hn6-Hn2) =  [Hn24,Hn20] = 1A33v1 = 1A(3,3)V1"));
    }    
    if(in_array("28", $controle) && in_array("24", $controle) && !in_array("20", $controle)){
      array_push($chaveMontada, strip_tags("(Hn10-Hn6) = [Hn28-Hn24] = 1A23v1 = 1A(2,2)V1"));      
      array_push($chaveMontada, strip_tags("(Hn6-Hn2) =  [Hn24] = 1A33v1 = 1A(3,0)V1"));
    }    
    if(!in_array("28", $controle) && in_array("24", $controle) && in_array("20", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn2) =  [Hn24,Hn20] = 1A33v1 = 1A(3,3)V1"));
      array_push($chaveMontada, strip_tags("(Hn10-Hn6) = [Hn24] = 1A23v1 = 1A(0,3)V1"));      
    }    
    if(!in_array("28", $controle) && in_array("24", $controle) && !in_array("20", $controle)){
      array_push($chaveMontada, strip_tags("(Hn10-Hn6) = [Hn24] = 1A23v1 = 1A(0,3)V1"));      
      array_push($chaveMontada, strip_tags("(Hn6-Hn2) =  [Hn24] = 1A33v1 = 1A(3,0)V1"));
    }        
    if(in_array("28", $controle) && !in_array("24", $controle) && !in_array("20", $controle)){
      array_push($chaveMontada, strip_tags("(Hn10) = [Hn28] = 1A23v1 = 1A(2,0)V1"));            
    }        
    if(!in_array("28", $controle) && !in_array("24", $controle) && in_array("20", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn2) =  [Hn20] = 1A33v1 = 1A(0,3)V1"));
    }        
    if(in_array("28", $controle) && !in_array("24", $controle) && in_array("20", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn2) =  [Hn20] = 1A33v1 = 1A(0,3)V1"));
      array_push($chaveMontada, strip_tags("(Hn10) = [Hn28] = 1A23v1 = 1A(2,0)V1"));            
    }   

    // Inicio etapa 9,5 - 5,1
    if(in_array("9", $controle) && in_array("5", $controle) && in_array("1", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn9-Hn5] = 5A11v1 = 5A(1,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn5,Hn1] = 1A11v1 = 1A(1,1)V1"));
    }
    if(in_array("9", $controle) && in_array("5", $controle) && !in_array("1", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn9-Hn5] = 5A11v1 = 5A(1,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn5] = 1A11v1 = 1A(1,0)V1"));
    }
    if(!in_array("9", $controle) && in_array("5", $controle) && in_array("1", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn5] = 5A11v1 = 5A(0,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn5,Hn1] = 1A11v1 = 1A(1,1)V1"));
    }
    if(!in_array("9", $controle) && in_array("5", $controle) && !in_array("1", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn5] = 5A11v1 = 5A(0,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn5] = 1A11v1 = 1A(1,0)V1"));
    }
    if(in_array("9", $controle) && !in_array("5", $controle) && !in_array("1", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn9] = 5A11v1 = 5A(1,0)V1"));      
    }
    if(!in_array("9", $controle) && !in_array("5", $controle) && in_array("1", $controle)){
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn1] = 1A11v1 = 1A(0,1)V1"));
    }
    if(in_array("9", $controle) && !in_array("5", $controle) && in_array("1", $controle)){
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn1] = 1A11v1 = 1A(0,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn9] = 5A11v1 = 5A(1,0)V1"));      
    }

    // Inicio etapa 18,14 - 14,10
    if(in_array("18", $controle) && in_array("14", $controle) && in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn18-Hn14] = 5A22V1 = 5A(2,2)V1"));      
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn14,Hn10] = 1A22V1 = 1A(2,2)V1"));     
    }
    if(in_array("18", $controle) && in_array("14", $controle) && !in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn18-Hn14] = 5A22V1 = 5A(2,2)V1"));      
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn14] = 1A22V1 = 1A(2,0)V1"));     
    }
    if(!in_array("18", $controle) && in_array("14", $controle) && in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn14] = 5A22V1 = 5A(0,2)V1"));      
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn14,Hn10] = 1A22V1 = 1A(2,2)V1"));
    }
    if(!in_array("18", $controle) && in_array("14", $controle) && !in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn14] = 5A22V1 = 5A(0,2)V1"));      
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn14] = 1A22V1 = 1A(2,0)V1"));
    }
    if(in_array("18", $controle) && !in_array("14", $controle) && !in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn18] = 5A22V1 = 5A(2,0)V1"));            
    }        
    if(!in_array("18", $controle) && !in_array("14", $controle) && in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn10] = 1A22V1 = 1A(0,2)V1"));
    }        
    if(in_array("18", $controle) && !in_array("14", $controle) && in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn5-Hn1) = [Hn10] = 1A22V1 = 1A(0,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn18] = 5A22V1 = 5A(2,0)V1"));            
    }        

    // Inicio etapa 27,23
    if(in_array("27", $controle) && in_array("23", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn27,Hn23] = 4A33v1 = 4A(3,3)V1"));
    }
    if(!in_array("27", $controle) && in_array("23", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn23] = 4A33v1 = 4A(0,3)V1"));
    }
    if(in_array("27", $controle) && !in_array("23", $controle)){      
      array_push($chaveMontada, strip_tags("(Hn9-Hn5) = [Hn27] = 4A33v1 = 4A(3,0)V1"));
    }

    // Inicio etapa 8,4
    if(in_array("8", $controle) && in_array("4", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn4) = [Hn8,Hn4] = 4A11v1 = 4A(1,1)V1"));      
    }
    if(in_array("8", $controle) && !in_array("4", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn4) = [Hn8] = 4A11v1 = 4A(1,0)V1"));      
    }
    if(!in_array("8", $controle) && in_array("4", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn4) = [Hn4] = 4A11v1 = 4A(0,1)V1"));      
    }

    // Inicio etapa 17,13
    if(in_array("17", $controle) && in_array("13", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn4) = [Hn17,Hn13] = 4A22v1 = 4A(2,2)V1"));      
    }
    if(!in_array("17", $controle) && in_array("13", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn4) = [Hn13] = 4A22v1 = 4A(0,2)V1"));      
    }
    if(in_array("17", $controle) && !in_array("13", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn4) = [Hn17] = 4A22v1 = 4A(2,0)V1"));      
    }    

    // Inicio etapa 26,22
    if(in_array("26", $controle) && in_array("22", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn4) = [Hn26,Hn22] = 3A33v1 = 4A(3,3)V1"));      
    }
    if(in_array("26", $controle) && !in_array("22", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn4) = [Hn26] = 3A33v1 = 4A(3,0)V1"));      
    }
    if(!in_array("26", $controle) && in_array("22", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn4) = [Hn22] = 3A33v1 = 3A(0,3)V1"));      
    }

    // Inicio etapa 7,3
    if(in_array("7", $controle) && in_array("3", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn3) = [Hn7,Hn3] = 3A11V1 = 3A(1,1)V1"));      
    }
    if(in_array("7", $controle) && !in_array("3", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn3) = [Hn7] = 3A11V1 = 3A(1,0)V1"));      
    }
    if(!in_array("7", $controle) && in_array("3", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn3) = [Hn3] = 3A11V1 = 3A(0,1)V1"));      
    }

    // Inicio etapa 6,2
    if(in_array("6", $controle) && in_array("2", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn2) = [Hn6,Hn2] = 2A11v1 = 2A(1,1)V1"));      
    }
    if(in_array("6", $controle) && !in_array("2", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn2) = [Hn6] = 2A11v1 = 2A(1,0)V1"));      
    }
    if(!in_array("6", $controle) && in_array("2", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn2) = [Hn2] = 2A11v1 = 2A(0,1)V1"));      
    }


    return $chaveMontada;
  }
  function tnRefiltragem($tn){
    $controle = [];                        
    $chaves = explode("-", strip_tags(trim($tn)));

    foreach ($chaves as $chave) {
      array_push($controle, strip_tags(trim($chave)));
    }

    $chaveMontada = montaChave($controle);

    return $chaveMontada;
  }
  function montaChaveOxA($controle){
    
      $oxMontado = [];
      if(in_array("9", $controle) && in_array("6", $controle) && in_array("3", $controle)){
        array_push($oxMontado, "(Oxn9-Oxn6) = [Oxn9-Oxn6] = 6Ox A11V1");   
        array_push($oxMontado, "(Oxn6-Oxn3) = [Oxn6-Oxn3] = 3Ox A11V1");                    
      }    
      if(in_array("9", $controle) && in_array("6", $controle) && !in_array("3", $controle)){
        array_push($oxMontado, "(Oxn9-Oxn6) = [Oxn9-Oxn6] = 6Ox A11V1");   
        array_push($oxMontado, "(Oxn6-Oxn3) = [Oxn6] = 3Ox A11V1");                    
      }    
      if(!in_array("9", $controle) && in_array("6", $controle) && in_array("3", $controle)){
        array_push($oxMontado, "(Oxn9-Oxn6) = [Oxn6] = 6Ox A11V1");   
        array_push($oxMontado, "(Oxn6-Oxn3) = [Oxn6-Oxn3] = 3Ox A11V1");                    
      }
      if(!in_array("9", $controle) && in_array("6", $controle) && !in_array("3", $controle)){
        array_push($oxMontado, "(Oxn9-Oxn6) = [Oxn6] = 6Ox A11V1");   
        array_push($oxMontado, "(Oxn6-Oxn3) = [Oxn6] = 3Ox A11V1");                    
      }
      if(in_array("9", $controle) && !in_array("6", $controle) && !in_array("3", $controle)){
        array_push($oxMontado, "(Oxn9-Oxn6) = [Oxn9] = 6Ox A11V1");           
      }    
      if(!in_array("9", $controle) && !in_array("6", $controle) && in_array("3", $controle)){        
        array_push($oxMontado, "(Oxn6-Oxn3) = [Oxn3] = 3Ox A11V1");                    
      }    
      if(in_array("9", $controle) && !in_array("6", $controle) && in_array("3", $controle)){
        array_push($oxMontado, "(Oxn9-Oxn6) = [Oxn9] = 6Ox A11V1");   
        array_push($oxMontado, "(Oxn6-Oxn3) = [Oxn3] = 3Ox A11V1");                    
      }    

      if(in_array("8", $controle) && in_array("5", $controle)){
        array_push($oxMontado, "(Oxn8-Oxn5) = [Oxn8-Oxn5] = 5Ox A11V1");        
      }        
      if(in_array("8", $controle) && !in_array("5", $controle)){
        array_push($oxMontado, "(Oxn8-Oxn5) = [Oxn8] = 5Ox A11V1");        
      }        
      if(!in_array("8", $controle) && in_array("5", $controle)){
        array_push($oxMontado, "(Oxn8-Oxn5) = [Oxn5] = 5Ox A11V1");        
      }

      if(in_array("7", $controle) && in_array("4", $controle) && in_array("1", $controle)){
        array_push($oxMontado, "(Oxn7-Oxn4) = [Oxn7-Oxn4] = 4Ox A11V1"); 
        array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn4-Oxn1] = 1Ox A11V1");               
      }
      if(in_array("7", $controle) && in_array("4", $controle) && !in_array("1", $controle)){
        array_push($oxMontado, "(Oxn7-Oxn4) = [Oxn7-Oxn4] = 4Ox A11V1"); 
        array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn4] = 1Ox A11V1");               
      }                  
      if(!in_array("7", $controle) && in_array("4", $controle) && in_array("1", $controle)){
        array_push($oxMontado, "(Oxn7-Oxn4) = [Oxn4] = 4Ox A11V1"); 
        array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn4-Oxn1] = 1Ox A11V1");               
      }
      if(in_array("7", $controle) && !in_array("4", $controle) && in_array("1", $controle)){
        array_push($oxMontado, "(Oxn7-Oxn4) = [Oxn7] = 4Ox A11V1"); 
        array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn1] = 1Ox A11V1");               
      }
      if(!in_array("7", $controle) && in_array("4", $controle) && !in_array("1", $controle)){
        array_push($oxMontado, "(Oxn7-Oxn4) = [Oxn4] = 4Ox A11V1"); 
        array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn4] = 1Ox A11V1");               
      }      
      if(in_array("7", $controle) && !in_array("4", $controle) && !in_array("1", $controle)){
        array_push($oxMontado, "(Oxn7-Oxn4) = [Oxn7] = 4Ox A11V1");         
      }      
      if(!in_array("7", $controle) && !in_array("4", $controle) && in_array("1", $controle)){
        array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn1] = 1Ox A11V1");               
      }      

      if(in_array("15", $controle) && in_array("12", $controle)){
        array_push($oxMontado, "(Onx6,Oxn3) = [Oxn15-Oxn12] = 3Ox A22V1");        
      }                           
      if(in_array("15", $controle) && !in_array("12", $controle)){
        array_push($oxMontado, "(Onx6,Oxn3) = [Oxn15] = 3Ox A22V1");        
      }                           
      if(!in_array("15", $controle) && in_array("12", $controle)){
        array_push($oxMontado, "(Onx6,Oxn3) = [Oxn12] = 3Ox A22V1");        
      }

      if(in_array("5", $controle) && in_array("2", $controle)){
        array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn5-Oxn2] = 2Ox A11V1");        
      }                                  
      if(in_array("5", $controle) && !in_array("2", $controle)){
        array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn5] = 2Ox A11V1");        
      }                                  
      if(!in_array("5", $controle) && in_array("2", $controle)){
        array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn2] = 2Ox A11V1");        
      }

      if(in_array("14", $controle) && in_array("11", $controle)){
        array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn14-Oxn11] = 2Ox A22V1");        
      }                                          
      if(in_array("14", $controle) && !in_array("11", $controle)){
        array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn14] = 2Ox A22V1");        
      }                                          
      if(!in_array("14", $controle) && in_array("11", $controle)){
        array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn11] = 2Ox A22V1");        
      }

      if(in_array("13", $controle) && in_array("10", $controle)){
        array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn13-Oxn10] = 1Ox A22V1");       
      }                                                 
      if(in_array("13", $controle) && !in_array("10", $controle)){
        array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn13] = 1Ox A22V1");       
      }                                                 
      if(!in_array("13", $controle) && in_array("10", $controle)){
        array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn10] = 1Ox A22V1");       
      }                                                 

      return $oxMontado;
  }
  function tnRefiltragemOxA($tnOx){
    $controleOx = [];                        
    
    $chavesOx = explode("-", strip_tags(trim($tnOx)));        

    foreach ($chavesOx as $chaveOx) {
      array_push($controleOx, strip_tags(trim($chaveOx)));
    }

    $oxMontado = montaChaveOxA($controleOx);
  
    return $oxMontado;
  }

  // Inicio Área Sub B
  function montaChaveB($controle){
    
    $chaveMontada = [];
      
    if(in_array("25", $controle) && in_array("22", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn25-Hn22] = 3A33v1 = 3A(3,3)V1"));
    }
    if(in_array("25", $controle) && !in_array("22", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn25] = 3A33v1 = 3A(3,0)V1"));
    }
    if(!in_array("25", $controle) && in_array("22", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn22] = 3A33v1 = 3A(0,3)V1"));
    }

    if(in_array("24", $controle) && in_array("21", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn24-Hn21] = 2A33v1 = 2A(3,3)V1"));
    }    
    if(in_array("24", $controle) && !in_array("21", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn24] = 2A33v1 = 2A(3,0)V1"));
    }      
    if(!in_array("24", $controle) && in_array("21", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn21] = 2A33v1 = 2A(0,3)V1"));
    }

    if(in_array("23", $controle) && in_array("20", $controle)){
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn23-Hn20] = 1A33v1 = 1A(3,3)V1"));
    }           
    if(in_array("23", $controle) && !in_array("20", $controle)){
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn23] = 1A33v1 = 1A(3,0)V1"));
    }           
    if(!in_array("23", $controle) && in_array("20", $controle)){
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn20] = 1A33v1 = 1A(0,3)V1"));
    } 

    // Inicio duplas 5,2 - 8,5
    if(in_array("8", $controle) && in_array("5", $controle) && in_array("2", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn8-Hn5] = 5A11v1 = 5A(1,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn5-Hn2] = 2A11v1 = 2A(1,1)V1"));
    }
    if(in_array("8", $controle) && in_array("5", $controle) && !in_array("2", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn8-Hn5] = 5A11v1 = 5A(1,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn5] = 2A11v1 = 2A(1,0)V1"));
    }
    if(!in_array("8", $controle) && in_array("5", $controle) && in_array("2", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn5] = 5A11v1 = 5A(0,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn5-Hn2] = 2A11v1 = 2A(1,1)V1"));
    }        
    if(!in_array("8", $controle) && in_array("5", $controle) && !in_array("2", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn5] = 5A11v1 = 5A(0,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn5] = 2A11v1 = 2A(1,0)V1"));
    }    
    if(in_array("8", $controle) && !in_array("5", $controle) && !in_array("2", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn8] = 5A11v1 = 5A(1,0)V1"));      
    }    
    if(!in_array("8", $controle) && !in_array("5", $controle) && in_array("2", $controle)){      
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn2] = 2A11v1 = 2A(0,1)V1"));
    }
    if(in_array("8", $controle) && !in_array("5", $controle) && in_array("2", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn8] = 5A11v1 = 5A(1,0)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn2] = 2A11v1 = 2A(0,1)V1"));
    }    

    // Inicio duplas 16,13 - 13,10
    if(in_array("16", $controle) && in_array("13", $controle) && in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn16-Hn13] = 4A22v1 = 4A(2,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn13-Hn10] = 1A22v1 = 1A(2,2)V1"));
    }    
    if(in_array("16", $controle) && in_array("13", $controle) && !in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn16-Hn13] = 4A22v1 = 4A(2,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn13] = 1A22v1 = 1A(2,0)V1"));
    }     
    if(!in_array("16", $controle) && in_array("13", $controle) && in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn13] = 4A22v1 = 4A(0,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn13-Hn10] = 1A22v1 = 1A(2,2)V1"));
    }     
    if(!in_array("16", $controle) && in_array("13", $controle) && !in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn13] = 4A22v1 = 4A(0,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn13] = 1A22v1 = 1A(2,0)V1"));
    }     
    if(!in_array("16", $controle) && !in_array("13", $controle) && in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn10] = 1A22v1 = 1A(0,2)V1"));
    }     
    if(in_array("16", $controle) && !in_array("13", $controle) && !in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn16] = 4A22v1 = 4A(2,0)V1"));
    }
    if(in_array("16", $controle) && !in_array("13", $controle) && in_array("10", $controle)){
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn16] = 4A22v1 = 4A(2,0)V1"));
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn10] = 1A22v1 = 1A(0,2)V1"));
    }

     // Inicio duplas 18,15 - 15,12
    if(in_array("18", $controle) && in_array("15", $controle) && in_array("12", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn18-Hn15] = 6A22v1 = 6A(2,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn15-Hn12] = 3A22v1 = 3A(2,2)V1"));
    }                 
    if(in_array("18", $controle) && in_array("15", $controle) && !in_array("12", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn18-Hn15] = 6A22v1 = 6A(2,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn15] = 3A22v1 = 3A(2,0)V1"));
    }     
    if(!in_array("18", $controle) && in_array("15", $controle) && in_array("12", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn15] = 6A22v1 = 6A(0,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn15-Hn12] = 3A22v1 = 3A(2,2)V1"));
    }     
    if(!in_array("18", $controle) && in_array("15", $controle) && !in_array("12", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn15] = 6A22v1 = 6A(0,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn15] = 3A22v1 = 3A(2,0)V1"));
    }     
    if(in_array("18", $controle) && in_array("15", $controle) && !in_array("12", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn18] = 6A22v1 = 6A(2,0)V1"));
    }     
    if(!in_array("18", $controle) && in_array("15", $controle) && in_array("12", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn12] = 3A22v1 = 3A(0,2)V1"));
    }     
    if(in_array("18", $controle) && !in_array("15", $controle) && in_array("12", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn12] = 3A22v1 = 3A(0,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn18] = 6A22v1 = 6A(2,0)V1"));
    }

    // Inicio duplas 17,14 - 14,11
    if(in_array("17", $controle) && in_array("14", $controle) && in_array("11", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn17-Hn14] = 5A22v1 = 5A(2,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn14-Hn11] = 2A22v1 = 2A(2,2)V1"));
    }                
    if(in_array("17", $controle) && in_array("14", $controle) && !in_array("11", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn17-Hn14] = 5A22v1 = 5A(2,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn14] = 2A22v1 = 2A(2,0 )V1"));
    }                
    if(!in_array("17", $controle) && in_array("14", $controle) && in_array("11", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn14] = 5A22v1 = 5A(0,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn14-Hn11] = 2A22v1 = 2A(2,2)V1"));
    }                    
    if(!in_array("17", $controle) && in_array("14", $controle) && !in_array("11", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn14] = 5A22v1 = 5A(0,2)V1"));
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn14] = 2A22v1 = 2A(2,0 )V1"));
    }                
    if(in_array("17", $controle) && !in_array("14", $controle) && !in_array("11", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn17] = 5A22v1 = 5A(2,0)V1"));      
    }                
    if(!in_array("17", $controle) && !in_array("14", $controle) && in_array("11", $controle)){      
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn11] = 2A22v1 = 2A(0,2)V1"));
    }                    
    if(in_array("17", $controle) && !in_array("14", $controle) && in_array("11", $controle)){
      array_push($chaveMontada, strip_tags("(Hn8-Hn5) = [Hn17] = 5A22v1 = 5A(2,0)V1"));      
      array_push($chaveMontada, strip_tags("(Hn5-Hn2) = [Hn11] = 2A22v1 = 2A(0,2)V1"));
    }                

    // Inicio duplas 6,3 - 9,6
    if(in_array("3", $controle) && in_array("6", $controle) && in_array("9", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn6-Hn3] = 3A11v1 = 3A(1,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn9-Hn6] = 6A11v1 = 6A(1,1)V1"));
    } 
    if(in_array("3", $controle) && in_array("6", $controle) && !in_array("9", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn6-Hn3] = 3A11v1 = 3A(1,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn6] = 6A11v1 = 6A(0,1)V1"));
    } 
    if(!in_array("3", $controle) && in_array("6", $controle) && in_array("9", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn6] = 3A11v1 = 3A(1,0)V1"));
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn9-Hn6] = 6A11v1 = 6A(1,1)V1"));
    } 
    if(!in_array("3", $controle) && in_array("6", $controle) && !in_array("9", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn6] = 3A11v1 = 3A(1,0)V1"));
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn6] = 6A11v1 = 6A(0,1)V1"));
    } 
    if(in_array("3", $controle) && !in_array("6", $controle) && !in_array("9", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn3] = 3A11v1 = 3A(0,1)V1"));
    }     
    if(!in_array("3", $controle) && !in_array("6", $controle) && in_array("9", $controle)){
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn9] = 6A11v1 = 6A(1,0)V1"));
    }     
    if(in_array("3", $controle) && !in_array("6", $controle) && in_array("9", $controle)){
      array_push($chaveMontada, strip_tags("(Hn6-Hn3) = [Hn3] = 3A11v1 = 3A(0,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn9-Hn6) = [Hn9] = 6A11v1 = 6A(1,0)V1"));
    }

    // Inicio duplas 4,1 - 7,4 - 19,7
    if(in_array("1", $controle) && in_array("4", $controle) && in_array("7", $controle)){
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn4-Hn1] = 1A11v1 = 1A(1,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn7-Hn4] = 4A11v1 = 4A(1,1)V1")); 
      array_push($chaveMontada, strip_tags("(Hn10-Hn7) = [Hn7] = 7A11v1 = 1A(0,1)V1"));     
    } 
    if(in_array("1", $controle) && in_array("4", $controle) && !in_array("7", $controle)){
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn4-Hn1] = 1A11v1 = 1A(1,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn4] = 4A11v1 = 4A(0,1)V1"));      
    } 
    if(!in_array("1", $controle) && in_array("4", $controle) && in_array("7", $controle)){
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn4] = 1A11v1 = 1A(1,0)V1"));
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn7-Hn4] = 4A11v1 = 4A(1,1)V1")); 
      array_push($chaveMontada, strip_tags("(Hn10-Hn7) = [Hn7] = 7A11v1 = 1A(0,1)V1"));     
    } 
    if(!in_array("1", $controle) && in_array("4", $controle) && !in_array("7", $controle)){
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn4] = 1A11v1 = 1A(1,0)V1"));
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn4] = 4A11v1 = 4A(0,1)V1"));       
    }     
    if(!in_array("1", $controle) && !in_array("4", $controle) && in_array("7", $controle) && !in_array("19", $controle)){      
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn7] = 4A11v1 = 4A(1,0)V1")); 
      array_push($chaveMontada, strip_tags("(Hn10-Hn7) = [Hn7] = 7A11v1 = 1A(0,1)V1"));     
    } 
    if(in_array("1", $controle) && !in_array("4", $controle) && !in_array("7", $controle)){
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn1] = 1A11v1 = 1A(0,1)V1"));      
    } 
    if(in_array("1", $controle) && !in_array("4", $controle) && in_array("7", $controle)){
      array_push($chaveMontada, strip_tags("(Hn4-Hn1) = [Hn1] = 1A11v1 = 1A(0,1)V1"));
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn7] = 4A11v1 = 4A(1,0)V1")); 
      array_push($chaveMontada, strip_tags("(Hn10-Hn7) = [Hn7] = 7A11v1 = 1A(0,1)V1"));     
    }

    // Inicio duplas 19,7 - 7,4
    if(in_array("19", $controle) && in_array("7", $controle)){
      array_push($chaveMontada, strip_tags("(Hn10-Hn7) = [Hn19-Hn7] = 7A11v1 = 1A(1,1)V1"));  
      array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn7] = 4A11v1 = 4A(1,0)V1")); 
    }
    if(in_array("19", $controle) && !in_array("7", $controle)){
      array_push($chaveMontada, strip_tags("(Hn10-Hn7) = [Hn19] = 7A11v1 = 1A(1,0)V1"));  
    }
    // if(in_array("7", $controle) && !in_array("10", $controle) && !in_array("4", $controle)){
    //   array_push($chaveMontada, strip_tags("(Hn10-Hn7) = [Hn7] = 7A11v1 = 1A(0,1)V1"));  
    //   array_push($chaveMontada, strip_tags("(Hn7-Hn4) = [Hn7] = 4A11v1 = 4A(1,0)V1")); 
    // }

    return $chaveMontada;          
  }
  
  function tnRefiltragemB($tn){
    $controle = [];

    $chaves = explode("-", strip_tags(trim($tn)));

    foreach ($chaves as $chave) {
      array_push($controle, strip_tags(trim($chave)));
    }
    
    $chaveMontada = montaChaveB($controle);
    
    return $chaveMontada;
  }

  function tn2BOx($tn2BOx){ 

    $oxBase = [];
    $oxBMontado = []; 

    foreach ($tn2BOx as $key => $value){
      array_push($oxBase, strip_tags(trim($value[0].','.$value[1])));      
    }

    // echo "<pre>";   
    // print_r($oxBase);
    // echo "</pre>"; 

    if(in_array("1,1", $oxBase)){
      array_push($oxBMontado, strip_tags(trim("1")));
    }    
    if(in_array("1,2", $oxBase) || in_array("2,1", $oxBase)){
      array_push($oxBMontado, strip_tags(trim("2")));
    }    
    if(in_array("1,3", $oxBase) || in_array("3,1", $oxBase)){
      array_push($oxBMontado, strip_tags(trim("3")));
    }    
    if(in_array("1,4", $oxBase) || in_array("4,1", $oxBase)){
      array_push($oxBMontado, strip_tags(trim("4")));
    }    
    if(in_array("2,2", $oxBase)){
      array_push($oxBMontado, strip_tags(trim("5")));
    }
    if(in_array("2,3", $oxBase) || in_array("3,2", $oxBase)){
      array_push($oxBMontado, strip_tags(trim("6")));
    }    
    if(in_array("2,4", $oxBase) || in_array("4,2", $oxBase)){
      array_push($oxBMontado, strip_tags(trim("7")));
    }    
    if(in_array("3,3", $oxBase)){
      array_push($oxBMontado, strip_tags(trim("8")));
    }
    if(in_array("3,4", $oxBase) || in_array("4,3", $oxBase)){
      array_push($oxBMontado, strip_tags(trim("9")));
    }
    if(in_array("4,4", $oxBase)){
      array_push($oxBMontado, strip_tags(trim("10")));
    }    

    // Área do Monta Chave
    $oxBCompleto = montaChaveOxB($oxBMontado);
    
    return $oxBCompleto;

  }

  function montaChaveOxB($controle){ 
    // echo "<pre>";
    // print_r($controle);
    // echo "</pre>";

    // A função está errada, eu preciso verificar se existe a dupla toda, ou uma parte ou outra conforme exemplo abaixo.

    //  if(in_array("9", $controle) && in_array("4", $controle)){
    $chaveBMontada = [];      
    if(in_array("10", $controle) && in_array("5", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn10-Oxn5) = [Oxn10-Oxn5] = 5Ox A11V1")));
    }
    if(in_array("10", $controle) && !in_array("5", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn10-Oxn5) = [Oxn10] = 5Ox A11V1")));
    }
    if(!in_array("10", $controle) && in_array("5", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn10-Oxn5) = [Oxn5] = 5Ox A11V1")));
    }

    if(in_array("9", $controle) && in_array("4", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn9-Oxn4) = [Oxn9-Oxn4] = 4Ox A11V1")));
    }
    if(in_array("9", $controle) && !in_array("4", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn9-Oxn4) = [Oxn9] = 4Ox A11V1")));
    }
    if(!in_array("9", $controle) && in_array("4", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn9-Oxn4) = [Oxn4] = 4Ox A11V1")));
    }

    if(in_array("8", $controle) && in_array("3", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn8-Oxn3) = [Oxn8-Oxn3] = 3Ox A11V1")));
    }
    if(in_array("8", $controle) && !in_array("3", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn8-Oxn3) = [Oxn8] = 3Ox A11V1")));
    }
    if(!in_array("8", $controle) && in_array("3", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn8-Oxn3) = [Oxn3] = 3Ox A11V1")));
    }

    if(in_array("7", $controle) && in_array("2", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn7-Oxn2) = [Oxn7-Oxn2] = 2Ox 11V1")));
    }
    if(in_array("7", $controle) && !in_array("2", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn7-Oxn2) = [Oxn7] = 2Ox 11V1")));
    }
    if(!in_array("7", $controle) && in_array("2", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn7-Oxn2) = [Oxn2] = 2Ox 11V1")));
    }

    if(in_array("6", $controle) && in_array("1", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn6-Oxn1) = [Oxn6-Oxn1] = 1Ox 11V1")));
    }
    if(in_array("6", $controle) && !in_array("1", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn6-Oxn1) = [Oxn6] = 1Ox 11V1")));
    }
    if(!in_array("6", $controle) && in_array("1", $controle)){
      array_push($chaveBMontada, strip_tags(trim("(Oxn6-Oxn1) = [Oxn1] = 1Ox 11V1")));
    }    

    return $chaveBMontada;
  }

 ?>