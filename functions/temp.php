<?php 
require_once('functions/Combinations.php');
function montaGrupo($dezenas){

  $gpA = array();
  $gpB = array();

  $variaveisA = array();
  $variaveisB = array();

  $arrOxA = array();
  $arrOxB = array();

  // Separando quem é do Grupo A e do B
  foreach($dezenas as $dezena){
    if(strpos($dezena, "A") !== false ){
        $valor = explode(":", $dezena);                
        array_push($gpA, $valor[1]);
      }
      if(strpos($dezena, "B") !== false ){
        $valor = explode(":", $dezena);
        array_push($gpB, $valor[1]);        
      }    
  }

  if($gpA){
    $prodA = array();
    $variavelGpA = array();
    
    for($i=0; $i<count($gpA); $i++){    
      $final = distribuicaoNumerica($gpA[$i]);
        if($final){
          $valorVariavelA = explode("-", $final);
          array_push($prodA, $valorVariavelA[0]);
          array_push($variavelGpA, $valorVariavelA[0]." ".$valorVariavelA[1]);          
          $oxA = explode(" ", $valorVariavelA[1]);
          array_push($variaveisA, $oxA[1]);
          array_push($arrOxA, $valorVariavelA[0]." ".$oxA[0]);
        }  
    }

    foreach ($variavelGpA as $variaveis) {    
      $variavelA .= '('.$variaveis.') ';
    }

    $variavelA = substr($variavelA,0,-1);
    $prodA = array_product($prodA);
    $blocoA = "<p class='produtorio'>A - ".$variavelA." = ".$prodA."</p>";
  }
  else{
    $blocoA = "<p class='produtorio'>A - <span style='color:red; font-weight:bold'>0</span></p>" ;
  }

  if($gpB){
    $prodB = array();
    $variavelGpB = array();

    for($i=0; $i<count($gpB); $i++){    
      $final = distribuicaoNumerica2($gpB[$i]);
        if($final){
          $valorVariavelB = explode("-", $final);
          array_push($prodB, $valorVariavelB[0]);
          array_push($variavelGpB, $valorVariavelB[0]." ".$valorVariavelB[1]);          
          $oxB = explode(" ", $valorVariavelB[1]);
          array_push($variaveisB, $oxB[1]);
          array_push($arrOxB, $valorVariavelB[0]." ".$oxB[0]);
      }  
    }  

    foreach ($variavelGpB as $variaveis) {    
      $variavelB .= '('.$variaveis.') ';
    } 

    $variavelB = substr($variavelB,0,-1);
    $prodB = array_product($prodB);
    $blocoB = "<p class='produtorio'>B - ".$variavelB." = ".$prodB."</p>";
  }else{
    $blocoB = "<p class='produtorio'>B - <span style='color:red; font-weight:bold'>0</span></p>" ;
  }

  $arrTemp = [];  
  array_push($arrTemp, $blocoA);
  array_push($arrTemp, $variavelGpA);
  array_push($arrTemp, $blocoB);
  array_push($arrTemp, $variavelGpB);
  array_push($arrTemp, $variaveisA);
  array_push($arrTemp, $variaveisB);
  array_push($arrTemp, $arrOxA);
  array_push($arrTemp, $arrOxB);
  return $arrTemp;    
}

function distribuicaoNumerica2($dezenas){
  $verificador = ['08','48','07','47','17','57','06','46','16','56','26','66','36','76','15','55','25','65','35','75','24','64','34','74','33','73','49','80','40','80'];

  if(in_array($dezenas, $verificador)){
    if($dezenas == "08" || $dezenas == "8"){
      $resultado = "1<span style='opacity:2;'>-</span>A21v1";
    }
    if($dezenas == "48"){
      $resultado = "1<span style='opacity:2;'>-</span>A21v1";
    }
    if($dezenas == "07" || $dezenas == "7"){
      $resultado = "1<span style='opacity:2;'>-</span>A31v1";
    }
    if($dezenas == "47"){
      $resultado = "1<span style='opacity:2;'>-</span>A31v1";
    }
    if($dezenas == "17"){
      $resultado = "2<span style='opacity:2;'>-</span>A32v1";
    }
    if($dezenas == "57"){
      $resultado = "2<span style='opacity:2;'>-</span>A32v1";
    }
    if($dezenas == "06" || $dezenas == "6"){
      $resultado = "1<span style='opacity:2;'>-</span>A41v1";
    }
    if($dezenas == "46"){
      $resultado = "1<span style='opacity:2;'>-</span>A41v1";
    }
    if($dezenas == "16"){
      $resultado = "2<span style='opacity:2;'>-</span>A42v1";
    }
    if($dezenas == "56"){
      $resultado = "2<span style='opacity:2;'>-</span>A42v1";
    }
    if($dezenas == "26"){
      $resultado = "3<span style='opacity:2;'>-</span>A43v1";
    }
    if($dezenas == "66"){
      $resultado = "3<span style='opacity:2;'>-</span>A43v1";
    }
    if($dezenas == "36"){
      $resultado = "4<span style='opacity:2;'>-</span>A44v1";
    }
    if($dezenas == "76"){
      $resultado = "2<span style='opacity:2;'>-</span>A44v1";
    }    
    if($dezenas == "15"){
      $resultado = "1<span style='opacity:2;'>-</span>A52v1";
    }
    if($dezenas == "55"){
      $resultado = "1<span style='opacity:2;'>-</span>A52v1";
    }
    if($dezenas == "25"){
      $resultado = "2<span style='opacity:2;'>-</span>A53v1";
    }
    if($dezenas == "65"){
      $resultado = "2<span style='opacity:2;'>-</span>A53v1";
    }
    if($dezenas == "35"){
      $resultado = "3<span style='opacity:2;'>-</span>A54v1";
    }
    if($dezenas == "75"){
      $resultado = "3<span style='opacity:2;'>-</span>A54v1";
    }
    if($dezenas == "24"){
      $resultado = "1<span style='opacity:2;'>-</span>A63v1";
    }
    if($dezenas == "64"){
      $resultado = "1<span style='opacity:2;'>-</span>A63v1";
    }    
    if($dezenas == "34"){
      $resultado = "1<span style='opacity:2;'>-</span>A63v1";
    }
    if($dezenas == "74"){
      $resultado = "1<span style='opacity:2;'>-</span>A63v1";
    }
    if($dezenas == "33"){
      $resultado = "1<span style='opacity:2;'>-</span>A74v1";
    }
    if($dezenas == "73"){
      $resultado = "1<span style='opacity:2;'>-</span>A74v1";
    }
    if($dezenas == "40"){
      $resultado = "1<span style='opacity:2;'>-</span>A95v1";
    }
    if($dezenas == "80"){
      $resultado = "1<span style='opacity:2;'>-</span>A95v1";
    }
  }
  return $resultado;
}

function distribuicaoNumerica($dezenas){
  $verificador = ['09','49','19','59','29','69','39','79','18','58','28','68','38','78','27','67','37','77','01','41','02','42','03','43','04','44','05','45','10','50','11','51','12','52','13','53','14','54','20','60','21','61','22','62','23','63','30','70','31','71','32','72'];

  if(in_array($dezenas, $verificador)){
    if($dezenas == "09" || $dezenas == "9"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "49"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "19"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "59"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "29"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "69"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "39"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "79"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A11v1";
    }
    if($dezenas == "18"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A22v1";
    }
    if($dezenas == "58"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A22v1";
    }    
    if($dezenas == "28"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A22v1";
    }
    if($dezenas == "68"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A22v1";
    }    
    if($dezenas == "38"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A22v1";
    }
    if($dezenas == "78"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A22v1";
    }    
    if($dezenas == "27"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A33v1";
    }
    if($dezenas == "67"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A33v1";
    }    
    if($dezenas == "37"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A33v1";
    }
    if($dezenas == "77"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A33v1";
    }    
    if($dezenas == "1" || $dezenas == "01" ){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "41"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A51v1";
    }    
    if($dezenas == "2" || $dezenas == "02" ){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "42"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A51v1";
    }    
    if($dezenas == "3" || $dezenas == "03" ){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "43"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "4" || $dezenas == "04" ){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "44"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A51v1";
    }        
    if($dezenas == "5" || $dezenas == "05" ){
      $resultado = "5<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "45"){
      $resultado = "5<span style='opacity:2;'>-</span>Ox1 A51v1";
    }
    if($dezenas == "10"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "50"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A62v1";
    }        
    if($dezenas == "11"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "51"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A62v1";
    }        
    if($dezenas == "12"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "52"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A62v1";
    }        
    if($dezenas == "13"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "53"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A62v1";
    }        
    if($dezenas == "14"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "54"){
      $resultado = "5<span style='opacity:2;'>-</span>Ox1 A62v1";
    }
    if($dezenas == "20"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A73v1";
    }
    if($dezenas == "60"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A73v1";
    }             
    if($dezenas == "21"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A73v1";
    }
    if($dezenas == "61"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A73v1";
    }             
    if($dezenas == "22"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A73v1";
    }
    if($dezenas == "62"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A73v1";
    }
    if($dezenas == "23"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A73v1";
    }
    if($dezenas == "63"){
      $resultado = "4<span style='opacity:2;'>-</span>Ox1 A73v1";
    }               
    if($dezenas == "30"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A84v1";
    }
    if($dezenas == "70"){
      $resultado = "1<span style='opacity:2;'>-</span>Ox1 A84v1";
    }                        
    if($dezenas == "31"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A84v1";
    }
    if($dezenas == "71"){
      $resultado = "2<span style='opacity:2;'>-</span>Ox1 A84v1";
    }                
    if($dezenas == "32"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A84v1";
    }
    if($dezenas == "72"){
      $resultado = "3<span style='opacity:2;'>-</span>Ox1 A84v1";
    }                                
  }
  return $resultado;
}

function verificaGrupo($dezenas){
  $arr = array();

  $verificadorA = ['09','49','19','59','29','69','39','79','18','58','28','68','38','78','27','67','37','77','01','41','02','42','03','43','04','44','05','45','10','50','11','51','12','52','13','53','14','54','20','60','21','61','22','62','23','63','30','70','31','71','32','72'];

  $verificadorB = ['08','48','07','47','17','57','06','46','16','56','26','66','36','76','15','55','25','65','35','75','24','64','34','74','33','73','80','40','80'];

  foreach ($dezenas as $valor){
    $gpA = in_array($valor, $verificadorA);
    $gpB = in_array($valor, $verificadorB);

    // Questionar sobre os valores quando tem correspondência nos dois subgrupos, como é o caso do 49.
    // Questionar também sobre quando existe repetição de valor no subgrupo, como é o caso do 80 no subgrupo 2

    if($gpA){
      $valor = "GrupoA:".$valor;      
       array_push($arr, $valor);
    }
    if($gpB){
      $valor = "GrupoB:".$valor;      
       array_push($arr, $valor);
    }
  }

  return $arr;
}

function primeFactors($n)
{
    $numeros = "";
    while($n % 2 == 0)
    {
        $n = $n / 2;
        $numeros .= "2,";
    }

    for($i = 3; $i <= sqrt($n); $i = $i + 2){
        while ($n % $i == 0){
            $numeros .= $i.",";
            $n = $n / $i;
        }
    }
 
    if ($n > 2){
        $numeros .= $n.",";
    }

    return mb_substr($numeros, 0, -1);
}

function montaQuintetoBase($grupoBase){
    $grupoQtd = count($grupoBase);
    $diferenca = 5 - $grupoQtd;

    if($diferenca){
      for($i=0; $i<$diferenca; $i++){
          array_push($grupoBase, 1);
      }
    }
    arsort($grupoBase);
    return $grupoBase;
}

function selecionaGrupo($controle){

    $grupo1 = ['1-A11v1', '1-A22v1', '1-A33v1', '1-A51v1', '1-A62v1', '1-A73v1', '1-A84v1'];
    $grupo2 = ['2-A11v1', '2-A22v1', '2-A33v1', '2-A51v1', '2-A62v1', '2-A73v1', '2-A84v1'];
    $grupo3 = ['3-A11v1', '3-A22v1', '3-A51v1', '3-A62v1', '3-A73v1', '3-A84v1'];
    $grupo5 = ['5-A51v1', '5-A62v1'];

    if($controle == 1){
      $grupoBase = $grupo1;
    }elseif($controle == 2){
      $grupoBase = $grupo2;
    }elseif($controle == 3){
      $grupoBase = $grupo3;
    }elseif($controle == 5){
      $grupoBase = $grupo5;
    }

    return $grupoBase;
}

function selecionaGrupo2($controle){

    $grupo1 = ['1-A21V1', '1-A31V1', '1-A41V1', '1-A52V1', '1-A63V1', '1-A95V1'];
    $grupo2 = ['2-A32V1', '2-A42V1', '2-A53V1', '2-A64V1'];
    $grupo3 = ['3-A43V1', '3-A54V1'];
    $grupo4 = ['4-A44V1'];

    if($controle == 1){
      $grupoBase = $grupo1;
    }elseif($controle == 2){
      $grupoBase = $grupo2;
    }elseif($controle == 3){
      $grupoBase = $grupo3;
    }elseif($controle == 4){
      $grupoBase = $grupo4;
    }

    return $grupoBase;
}

function verificaTamanho($grupoBase){
    $max = intval(max($grupoBase));
    if($max > 5) {
      return true;
    }else{
      return false;
    }
}

function verificaProdutorio($grupoBase){
    $grupoBase = explode(",", $grupoBase);
    $verificaTamanho = verificaTamanho($grupoBase);

    if(!$verificaTamanho){
      if(count($grupoBase) < 5){
        $resultado = montaQuintetoBase($grupoBase);
      }
      elseif(count($grupoBase) > 5){
         $resultado = montaQuintetoBase($grupoBase);
      }
      else{
        $resultado = $grupoBase;
      }
    }
  
    return $resultado; 
}

function escreveFatoracao($fatoracao){
  $i = 1;
  foreach ($fatoracao as $key => $value){
    if($i !== count($fatoracao)){
      $resultado .= $value.'-';
    }else{
      $resultado .= $value;
    }
    $i++;
  }
  return $resultado;
}

function escreveRepeticoes($repeticoes){
  $i = 1;
  foreach ($repeticoes as $key => $value){
    if($i !== count($repeticoes)){
      $resultado .= "Grupo: ".$key." = ".$value.'/ ';
    }else{
      $resultado .= "Grupo: ".$key." = ".$value;
    }
    $i++;
  }
  return $resultado;
}

function gerenciaRepeticoes($repeticoes, $tipoGrupo){

  $grupo1 = array();
  $grupo2 = array();
  $grupo3 = array();
  $grupo4 = array();
  $grupo5 = array();

  foreach($repeticoes as $chave => $valor){
      if($tipoGrupo){
        $Combinations = new Combinations(selecionaGrupo2($chave));
      }else{
        $Combinations = new Combinations(selecionaGrupo($chave));
      } 
      $permutations = $Combinations->getCombinations($valor, false);
      $controleTotal = count($permutations);
        
      if($controleTotal){
          $tamanhoArray = count($permutations[0])-1;
      }

      for($i=$controleTotal; $i >= 0; $i--){
          for($j=0; $j <= $tamanhoArray; $j++){
            if($chave == 1){
              array_push($grupo1, $permutations[$i][$j]);
            }elseif($chave == 2){
              array_push($grupo2, $permutations[$i][$j]);
            }elseif($chave == 3){
               array_push($grupo3, $permutations[$i][$j]);
            }elseif($chave == 4){
               array_push($grupo4, $permutations[$i][$j]);
            }
            elseif($chave == 5){
               array_push($grupo5, $permutations[$i][$j]);
            }   
            $resultado .= $permutations[$i][$j]." - ";
            if($j == $tamanhoArray){
              $resultado = "";
            }
          }
      }

      if($chave == 1){
        $grupo1 = array_chunk($grupo1, $valor);
      }elseif($chave == 2){
        $grupo2 = array_chunk($grupo2, $valor);
      }elseif($chave == 3){
        $grupo3 = array_chunk($grupo3, $valor);
      }elseif($chave == 4){
        $grupo3 = array_chunk($grupo4, $valor);
      }elseif($chave == 5){
        $grupo5 = array_chunk($grupo5, $valor);
      }
  }
  $arr = array_merge_recursive($grupo1, $grupo2, $grupo3, $grupo4, $grupo5);
  return $arr;
}

  function montaQuintetoFinal($arr, $grupos){

    $qtdGrupos = count($grupos);
    $tamanhoGrupos = array_sum($grupos);

    $Combinations = new Combinations($arr);
    $permutations = $Combinations->getCombinations($qtdGrupos, false);
    $permKey = $permutations;

    $arrT = array();

    for($i=0; $i < count($permKey); $i++){ 
      for($j=0; $j < count($permKey[$i]); $j++){ 
        array_push($arrT, count($permKey[$i][$j]));
      }
      $resultado = array_sum($arrT);
      $arrT = array();
      if($tamanhoGrupos <= 5){
        if($resultado > 5){
          unset($permutations[$i]);
        }
      }

    }

    $arrS = array();
    foreach ($permutations as $keys => $value){
      foreach ($value as $key => $values){
        foreach ($values as $key => $variaveis){
          $variavel = explode('-',$variaveis);
          array_push($arrS, $variavel[0]);
        }
      }
    }

    return $permutations;
  }

  function escreveQuintetoFinal($arr, $condicao, $grupos){
    $tamanhoGrupos = array_sum($grupos);


    $arrS = array();
    $arrResult = array();
    foreach ($arr as $key => $arrs){
      foreach ($arrs as $key => $variavel){
        array_push($arrResult, str_replace("-", "", $variavel)." - ");
        $variavel = explode('-',$variavel);
        array_push($arrS, $variavel[0]);    
      }
    }
    if(array_product($arrS) == intval($condicao)){
      $resultado = $arrResult; 
      if(intval($tamanhoGrupos) > 5){
        unset($resultado[0]);
      }
      return $resultado;
    }else{
      $arrS = array();
    }

  }

  function escreveResultado($grupos){
    foreach($grupos as $key => $variavel){
      $resultado .= str_replace("-", "", $variavel)." - ";
    }
    $resultado = substr($resultado,0,-2);
    return $resultado;
  }

function combinacoesDe($k, $xs){
    shuffle($xs);
    if ($k === 0)
        return array(array());
    if (count($xs) === 0)
        return array();
    $x = $xs[0];
    $xs1 = array_slice($xs,1,count($xs)-1);
    $res1 = combinacoesDe($k-1,$xs1);
    for ($i = 0; $i < count($res1); $i++) {
        array_splice($res1[$i], 0, 0, $x);
    }
    $res2 = combinacoesDe($k,$xs1);
    // sort($res1);
    // sort($res2);
    return array_merge($res1, $res2);
}

function tn2AOx($tn2AOX){
      
      $verificadores = ["1 OX1,1 OX1","1 OX1,2 OX1","2 OX1,1 OX1","1 OX1,3 OX1","3 OX1,1 OX1","1 OX1,4 OX1","4 OX1,1 OX1","1 OX1,5 OX1","5 OX1,1 OX1","2 OX1,2 OX1","2 OX1,3 OX1","3 OX1,2 OX1","2 OX1,4 OX1","4 OX1,2 OX1","2 OX1,5 OX1","5 OX1,2 OX1","3 OX1,3 OX1","3 OX1,4 OX1","4 OX1,3 OX1","3 OX1,5 OX1","5 OX1,3 OX1","4 OX1,4 OX1","4 OX1,5 OX1","5 OX1,4 OX1","5 OX1,5 OX1"];

      $valoresTn = ["1"=>"1 OX1,1 OX1","2"=>"1 OX1,2 OX1","2AN"=>"2 OX1,1 OX1","3"=>"1 OX1,3 OX1","3AN"=>"3 OX1,1 OX1","4"=>"1 OX1,4 OX1","4AN"=>"4 OX1,1 OX1","5"=>"1 OX1,5 OX1","5AN"=>"5 OX1,1 OX1","6"=>"2 OX1,2 OX1","7"=>"2 OX1,3 OX1","7AN"=>"3 OX1,2 OX1","8"=>"2 OX1,4 OX1","8AN"=>"4 OX1,2 OX1","9"=>"2 OX1,5 OX1","9AN"=>"5 OX1,2 OX1","10"=>"3 OX1,3 OX1","11"=>"3 OX1,4 OX1","11AN"=>"4 OX1,3 OX1","12"=>"3 OX1,5 OX1","12AN"=>"5 OX1,3 OX1","13"=>"4 OX1,4 OX1","14"=>"4 OX1,5 OX1","14AN"=>"5 OX1,4 OX1","15"=>"5 OX1,5 OX1"]; 

      $valorFinal = ["1"=>"1 OX1,1 OX1","2"=>"1 OX1,2 OX1","2AN"=>"2 OX1,1 OX1","3"=>"1 OX1,3 OX1","3AN"=>"3 OX1,1 OX1","4"=>"1 OX1,4 OX1","4AN"=>"4 OX1,1 OX1","5"=>"1 OX1,5 OX1","5AN"=>"5 OX1,1 OX1","6"=>"2 OX1,2 OX1","7"=>"2 OX1,3 OX1","7AN"=>"3 OX1,2 OX1","8"=>"2 OX1,4 OX1","8AN"=>"4 OX1,2 OX1","9"=>"2 OX1,5 OX1","9AN"=>"5 OX1,2 OX1","10"=>"3 OX1,3 OX1","11"=>"3 OX1,4 OX1","11AN"=>"4 OX1,3 OX1","12"=>"3 OX1,5 OX1","12AN"=>"5 OX1,3 OX1","13"=>"4 OX1,4 OX1","14"=>"4 OX1,5 OX1","14AN"=>"5 OX1,4 OX1","15"=>"5 OX1,5 OX1"]; 

      $tnGerado = [];
      $arrTemp = [];

      foreach ($tn2AOX as $key => $value){             
        $chave = strip_tags(strtoupper(trim($value[0]).",".trim($value[1])));                    
        if(in_array($chave, $verificadores)){          
          $chaveTn = array_search($chave, $verificadores);          
          $tnEncontrado = $verificadores[$chaveTn];          
          $valorTn = array_search($tnEncontrado, $valoresTn);          
          $tnFinal = $valorFinal[$valorTn].'<br>';                    
          $tnGerado[strip_tags(trim($tnFinal))] = $valorTn." - ".$tnFinal;                  
      }
    }   
      return $tnGerado;      
  }

  function tn2A($tn2){
      
      $verificadores = ["A11V1,A11V1","A11V1,A22V1","A22V1,A11V1","A11V1,A33V1","A33V1,A11V1",
"A11V1,A51V1","A51V1,A11V1","A11V1,A62V1","A62V1,A11V1","A11V1,A73V1","A73V1,A11V1","A11V1,A84V1","A84V1,A11V1","A22V1,A22V1","A22V1,A22V1","A22V1,A33V1","A33V1,A22V1","A22V1,A51V1","A51V1,A22V1","A22V1,A62V1","A62V1,A22V1","A22V1,A73V1","A73V1,A22V1","A22V1,A84V1","A84V1,A22V1","A33V1,A33V1","A33V1,A33V1","A33V1,A51V1","A51V1,A33V1","A33V1,A62V1","A62V1,A33V1","A33V1,A73V1","A73V1,A33V1", "A33V1,A84V1","A84V1,A33V1","A51V1,A51V1","A51V1,A51V1","A51V1,A62V1","A62V1,A51V1","A51V1,A73V1","A73V1,A51V1","A51V1,A84V1","A84V1,A51V1","A62V1,A62V1", "A62V1,A62V1","A62V1,A73V1","A62V1,A73V1","A62V1,A84V1","A84V1,A62V1","A73V1,A73V1","A73V1,A73V1","A73V1,A84V1","A84V1,A73V1","A84V1,A84V1","A84V1,A84V1"];

      $valoresTn = ["1"=>"A11V1,A11V1","1AN"=>"A11V1,A11V1","2"=>"A11V1,A22V1","2AN"=>"A22V1,A11V1","3"=>"A11V1,A33V1","3AN"=>"A33V1,A11V1","4"=>"A11V1,A51V1","4AN"=>"A51V1,A11V1","5"=>"A11V1,A62V1","5AN"=>"A62V1,A11V1","6"=>"A11V1,A73V1","6AN"=>"A73V1,A11V1","7"=>"A11V1,A84V1","7AN"=>"A84V1,A11V1","8"=>"A22V1,A22V1","8AN"=>"A22V1,A22V1","9"=>"A22V1,A33V1","9AN"=>"A33V1,A22V1","10"=>"A22V1,A51V1","10AN"=>"A51V1,A22V1","11"=>"A22V1,A62V1","11AN"=>"A62V1,A22V1","12"=>"A22V1,A73V1","12AN"=>"A73V1,A22V1","13"=>"A22V1,A84V1","13AN"=>"A84V1,A22V1","14"=>"A33V1,A33V1","14AN"=>"A33V1,A33V1","15"=>"A33V1,A51V1","15AN"=>"A51V1,A33V1","16"=>"A33V1,A62V1","16AN"=>"A62V1,A33V1","17"=>"A33V1,A73V1","17AN"=>"A73V1,A33V1","18"=>"A33V1,A84V1","18AN"=>"A84V1,A33V1","19"=>"A51V1,A51V1","19AN"=>"A51V1,A51V1","20"=>"A51V1,A62V1","20AN"=>"A62V1,A51V1","21"=>"A51V1,A73V1","21AN"=>"A73V1,A51V1","22"=>"A51V1,A84V1","22AN"=>"A84V1,A51V1","23"=>"A62V1,A62V1","24"=>"A62V1,A73V1","24AN"=>"A62V1,A73V1","25"=>"A62V1,A84V1","25AN"=>"A84V1,A62V1","26"=>"A73V1,A73V1","26AN"=>"A73V1,A73V1","27"=>"A73V1,A84V1","27AN"=>"A84V1,A73V1","28"=>"A84V1,A84V1","28AN"=>"A84V1,A84V1"]; 

      $valorFinal = ["1"=>"A11V1,A11V1","1AN"=>"A11V1,A11V1","2"=>"A11V1,A22V1","2AN"=>"A22V1,A11V1","3"=>"A11V1,A33V1","3AN"=>"A33V1,A11V1","4"=>"A11V1,A51V1","4AN"=>"A51V1,A11V1","5"=>"A11V1,A62V1","5AN"=>"A11V1,A62V1","6"=>"A11V1,A73V1","6AN"=>"A73V1,A11V1","7"=>"A11V1,A84V1","7AN"=>"A84V1,A11V1","8"=>"A22V1,A22V1","8AN"=>"A22V1,A22V1","9"=>"A22V1,A33V1","9AN"=>"A33V1,A22V1","10"=>"A22V1,A51V1","10AN"=>"A51V1,A22V1","11"=>"A22V1,A62V1","11AN"=>"A62V1,A22V1","12"=>"A22V1,A73V1","12AN"=>"A73V1,A22V1","13"=>"A22V1,A84V1","13AN"=>"A84V1,A22V1","14"=>"A33V1,A33V1","14AN"=>"A33V1,A33V1","15"=>"A33V1,A51V1","15AN"=>"A51V1,A33V1","16"=>"A33V1,A62V1","16AN"=>"A62V1,A33V1","17"=>"A33V1,A73V1","17AN"=>"A73V1,A33V1","18"=>"A33V1,A84V1","18AN"=>"A84V1,A33V1","19"=>"A51V1,A51V1","19AN"=>"A51V1,A51V1","20"=>"A51V1,A62V1","20AN"=>"A62V1,A51V1","21"=>"A51V1,A73V1","21AN"=>"A73V1,A51V1","22"=>"A51V1,A84V1","22AN"=>"A84V1,A51V1","23"=>"A62V1,A62V1","24"=>"A62V1,A73V1","24AN"=>"A62V1,A73V1","25"=>"A62V1,A84V1","25AN"=>"A84V1,A62V1","26"=>"A73V1,A73V1","26AN"=>"A73V1,A73V1","27"=>"A73V1,A84V1","27AN"=>"A84V1,A73V1","28"=>"A84V1,A84V1","28AN"=>"A84V1,A84V1"]; 

      $tnGerado = [];
      $arrTemp = [];

      foreach ($tn2 as $key => $value){        
        $chave = strtoupper($value[0].",".$value[1]);             
        if(in_array($chave, $verificadores)){
          $chaveTn = array_search($chave, $verificadores);          
          $tnEncontrado = $verificadores[$chaveTn];          
          $valorTn = array_search($tnEncontrado, $valoresTn);          
          $tnFinal = $valorFinal[$valorTn].'<br>';                    
          $tnGerado[strip_tags(trim($tnFinal))] = $valorTn." - ".$tnFinal;          
        }
      }

      return $tnGerado;      
  }

    function tn2AChaves($tn2){
      
      $verificadores = ["A11V1,A11V1","A11V1,A22V1","A22V1,A11V1","A11V1,A33V1","A33V1,A11V1",
"A11V1,A51V1","A51V1,A11V1","A11V1,A62V1","A62V1,A11V1","A11V1,A73V1","A73V1,A11V1","A11V1,A84V1","A84V1,A11V1","A22V1,A22V1","A22V1,A22V1","A22V1,A33V1","A33V1,A22V1","A22V1,A51V1","A51V1,A22V1","A22V1,A62V1","A62V1,A22V1","A22V1,A73V1","A73V1,A22V1","A22V1,A84V1","A84V1,A22V1","A33V1,A33V1","A33V1,A33V1","A33V1,A51V1","A51V1,A33V1","A33V1,A62V1","A62V1,A33V1","A33V1,A73V1","A73V1,A33V1", "A33V1,A84V1","A84V1,A33V1","A51V1,A51V1","A51V1,A51V1","A51V1,A62V1","A62V1,A51V1","A51V1,A73V1","A73V1,A51V1","A51V1,A84V1","A84V1,A51V1","A62V1,A62V1", "A62V1,A62V1","A62V1,A73V1","A62V1,A73V1","A62V1,A84V1","A84V1,A62V1","A73V1,A73V1","A73V1,A73V1","A73V1,A84V1","A84V1,A73V1","A84V1,A84V1","A84V1,A84V1"];

      $valoresTn = ["1"=>"A11V1,A11V1","1AN"=>"A11V1,A11V1","2"=>"A11V1,A22V1","2AN"=>"A22V1,A11V1","3"=>"A11V1,A33V1","3AN"=>"A33V1,A11V1","4"=>"A11V1,A51V1","4AN"=>"A51V1,A11V1","5"=>"A11V1,A62V1","5AN"=>"A62V1,A11V1","6"=>"A11V1,A73V1","6AN"=>"A73V1,A11V1","7"=>"A11V1,A84V1","7AN"=>"A84V1,A11V1","8"=>"A22V1,A22V1","8AN"=>"A22V1,A22V1","9"=>"A22V1,A33V1","9AN"=>"A33V1,A22V1","10"=>"A22V1,A51V1","10AN"=>"A51V1,A22V1","11"=>"A22V1,A62V1","11AN"=>"A62V1,A22V1","12"=>"A22V1,A73V1","12AN"=>"A73V1,A22V1","13"=>"A22V1,A84V1","13AN"=>"A84V1,A22V1","14"=>"A33V1,A33V1","14AN"=>"A33V1,A33V1","15"=>"A33V1,A51V1","15AN"=>"A51V1,A33V1","16"=>"A33V1,A62V1","16AN"=>"A62V1,A33V1","17"=>"A33V1,A73V1","17AN"=>"A73V1,A33V1","18"=>"A33V1,A84V1","18AN"=>"A84V1,A33V1","19"=>"A51V1,A51V1","19AN"=>"A51V1,A51V1","20"=>"A51V1,A62V1","20AN"=>"A62V1,A51V1","21"=>"A51V1,A73V1","21AN"=>"A73V1,A51V1","22"=>"A51V1,A84V1","22AN"=>"A84V1,A51V1","23"=>"A62V1,A62V1","24"=>"A62V1,A73V1","24AN"=>"A62V1,A73V1","25"=>"A62V1,A84V1","25AN"=>"A84V1,A62V1","26"=>"A73V1,A73V1","26AN"=>"A73V1,A73V1","27"=>"A73V1,A84V1","27AN"=>"A84V1,A73V1","28"=>"A84V1,A84V1","28AN"=>"A84V1,A84V1"]; 

      $valorFinal = ["1"=>"A11V1,A11V1","1AN"=>"A11V1,A11V1","2"=>"A11V1,A22V1","2AN"=>"A22V1,A11V1","3"=>"A11V1,A33V1","3AN"=>"A33V1,A11V1","4"=>"A11V1,A51V1","4AN"=>"A51V1,A11V1","5"=>"A11V1,A62V1","5AN"=>"A11V1,A62V1","6"=>"A11V1,A73V1","6AN"=>"A73V1,A11V1","7"=>"A11V1,A84V1","7AN"=>"A84V1,A11V1","8"=>"A22V1,A22V1","8AN"=>"A22V1,A22V1","9"=>"A22V1,A33V1","9AN"=>"A33V1,A22V1","10"=>"A22V1,A51V1","10AN"=>"A51V1,A22V1","11"=>"A22V1,A62V1","11AN"=>"A62V1,A22V1","12"=>"A22V1,A73V1","12AN"=>"A73V1,A22V1","13"=>"A22V1,A84V1","13AN"=>"A84V1,A22V1","14"=>"A33V1,A33V1","14AN"=>"A33V1,A33V1","15"=>"A33V1,A51V1","15AN"=>"A51V1,A33V1","16"=>"A33V1,A62V1","16AN"=>"A62V1,A33V1","17"=>"A33V1,A73V1","17AN"=>"A73V1,A33V1","18"=>"A33V1,A84V1","18AN"=>"A84V1,A33V1","19"=>"A51V1,A51V1","19AN"=>"A51V1,A51V1","20"=>"A51V1,A62V1","20AN"=>"A62V1,A51V1","21"=>"A51V1,A73V1","21AN"=>"A73V1,A51V1","22"=>"A51V1,A84V1","22AN"=>"A84V1,A51V1","23"=>"A62V1,A62V1","24"=>"A62V1,A73V1","24AN"=>"A62V1,A73V1","25"=>"A62V1,A84V1","25AN"=>"A84V1,A62V1","26"=>"A73V1,A73V1","26AN"=>"A73V1,A73V1","27"=>"A73V1,A84V1","27AN"=>"A84V1,A73V1","28"=>"A84V1,A84V1","28AN"=>"A84V1,A84V1"]; 


      $tnGerado = [];
      $arrTemp = [];
      
      foreach ($tn2 as $key => $value){        
        $chave = strtoupper($value[0].",".$value[1]);             
        if(in_array($chave, $verificadores)){
          $chaveTn = array_search($chave, $verificadores);          
          $tnEncontrado = $verificadores[$chaveTn];          
          $valorTn = array_search($tnEncontrado, $valoresTn);          
          $tnFinal = $valorFinal[$valorTn].'<br>';                    
          $tnGerado[$tnFinal] = $tnFinal;          
        }
      }

      return $tnGerado;      
  }



  function tnRefiltragem($tn){
    $controle = [];
    $verificadores = ["19","15","28","24","9","5","18","14","27","23","8","4","17","13","26","22","7","3","16","12","25","21","6","2","15","11","24","20","5","1","14","10"];
        
    $chaves = explode("-", strip_tags(trim($tn)));

    foreach ($chaves as $chave) {
      array_push($controle, strip_tags(trim($chave)));
    }
    
    $duplaFinal = [];
    $duplaTemp = [];
    
    $duplas = ["19"=>"19,15", "15"=>"19,15", "28"=>"28,24", "24"=>"28,24", "9"=>"9,5", "5"=>"9,5", "18"=>"18,14","14"=>"18,14","27"=>"27,23","23"=>"27,23",
    "8"=>"8,4","4"=>"8,4","17"=>"17,13","13"=>"17,13","26"=>"26,22","22"=>"26,22","7"=>"7,3","3"=>"7,3","16"=>"16,12","12"=>"16,12","25"=>"25,21","21"=>"25,21",
    "6"=>"6,2","2"=>"6,2","15"=>"15,11","11"=>"15,11","24"=>"24,20","20"=>"24,20","5"=>"5,1","1"=>"5,1","14"=>"14,10","10"=>"14,10"];

    foreach ($controle as $chave) {      
      if(array_key_exists(strip_tags(trim($chave)), $duplas)){
        array_push($duplaTemp, $duplas[strip_tags(trim($chave))]);        
      }  
    }
    
    $qtdDuplaTemp = array_count_values($duplaTemp);

    foreach ($qtdDuplaTemp as $keyRefiltragem => $valueRefiltragem){      
      if($valueRefiltragem == 2){
        array_push($duplaFinal, verificaChave(strip_tags(trim($keyRefiltragem))));        
      }
      else{
        $buscaChave = explode(",", $keyRefiltragem);
        if(in_array(strip_tags(trim($buscaChave[0])), $controle)){
          array_push($duplaFinal, verificaChave(strip_tags(trim($buscaChave[0]))));          
        }
        if(in_array(strip_tags(trim($buscaChave[1])), $controle)){
          array_push($duplaFinal, verificaChave(strip_tags(trim($buscaChave[1]))));                     
        }        
      }      
    }

    return $duplaFinal;
  }

  function verificaChave($chave){
    // Criar verificador para o caso do 5, que pode vir com o 9 ou com o 1
    // Uma alternativa seria passar além da chave inutária, um ARRAY contendo todas que seja verificado se nesse Array as multiplas correspondencias existem.    
    $chaveMontada = [];
    
    if($chave == "19,15"){
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn19-Hn15] = 1A12v1 = 1A(2,2)V1");
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn15] = 2A22v1 = 2A(2,0)V1");      
      // return "19,15 (1,1) 1A12V1";
    }
    if($chave == "19"){
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn19] = 1A12v1 = 1A(2,0)V1");
      // return "19 (1,0) 1A12V1";
    }
    if($chave == "15"){
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn15] = 1A12v1 = 1A(0,2)V1");
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn15] = 2A22v1 = 2A(2,0)V1");            
      // return "15 (0,1) 1A12V1";
    }

    if($chave == "28,24"){      
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn28-Hn24] = 1A23v1 = 1A(2,2)V1");
      array_push($chaveMontada, "(Hn6-Hn2) =  [Hn24] = 1A33v1 = 1A(3,0)V1");      
      // return "28,24 (2,2) 1A23V1";
    }
    if($chave == "28"){
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn28] = 1A23v1 = 1A(2,0)V1");
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn19] = 1A12v1 = 1A(1,0)V1");
      // return "28 (2,0) 1A23V1";
    }
    if($chave == "24"){
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn24] = 1A23v1 = 1A(0,2)V1");
      array_push($chaveMontada, "(Hn6-Hn2) =  [Hn24] = 1A33v1 = 1A(3,0)V1");      
      // return "24 (0,2) 1A23V1";
    }

    if($chave == "9,5"){
      array_push($chaveMontada, "(Hn9-Hn5) = [Hn9-Hn5] = 5A11v1 = 5A(1,1)V1");      
      array_push($chaveMontada, "(Hn5-Hn1) = [Hn5]     = 1A11v1 = 1A(1,0)V1"); 
      // return "9,5 (1,1) 5A11V1";
    }
    if($chave == "9"){
      array_push($chaveMontada, "(Hn9-Hn5) = [Hn9] = 5A11v1 = 5A(1,0)V1");      
      // return "9 (1,0) 5A11V1";
    }
    if($chave == "5"){
      array_push($chaveMontada, "(Hn9-Hn5) = [Hn5] = 5A11v1 = 5A(0,1)V1");      
      array_push($chaveMontada, "(Hn5-Hn1) = [Hn5] = 1A11v1 = 1A(1,0)V1"); 
      // return "5 (0,1) 5A11V1";
    }

    if($chave == "18,14"){
      array_push($chaveMontada, "(Hn9-Hn5) = [Hn18-Hn14] = 5A22V1 = 5A(2,2)V1");      
      array_push($chaveMontada, "(Hn5-Hn1) = [Hn14]      = 1A22v1 = 1A(2,0)V1");            
      // return "18,14 (2,2) 5A22V1";
    }
    if($chave == "18"){
      array_push($chaveMontada, "(Hn9-Hn5) = [Hn18] = 5A22V1 = 5A(2,0)V1");      
      // return "18 (2,0) 5A22V1";
    }
    if($chave == "14"){      
      array_push($chaveMontada, "(Hn9-Hn5) = [Hn14] = 5A22V1 = 5A(0,2)V1");      
      array_push($chaveMontada, "(Hn5-Hn1) = [Hn14] = 1A22v1 = 1A(2,0)V1");                  
      // return "18 (0,2) 5A22V1";
    }
          
    if($chave == "27,23"){
      array_push($chaveMontada, "(Hn9-Hn5) = [Hn27,Hn23] = 4A33v1 = 4A(3,3)V1");      
      // return "27,23 (3,3) 4A33V1";
    }
    if($chave == "27"){
      array_push($chaveMontada, "(Hn9-Hn5) = [Hn27] = 4A33v1 = 4A(3,0)V1");
      // return "27 (3,0) 4A33V1";
    }
    if($chave == "23"){
      array_push($chaveMontada, "(Hn9-Hn5) = [Hn23] = 4A33v1 = 4A(0,3)V1");
      // return "(Hn9-Hn5) = [Hn23] = 4A33v1 = 4A(3,0)V1";
    }

    if($chave == "8,4"){
      array_push($chaveMontada, "(Hn8-Hn4) = [Hn8,Hn4] = 4A11v1 = 4A(1,1)V1");
      // return "8,4 (1,1) 4A11V1";
    }
    if($chave == "8"){
      array_push($chaveMontada, "(Hn8-Hn4) = [Hn8] = 4A11v1 = 4A(1,0)V1");
      // return "8 (1,0) 4A11V1";
    }
    if($chave == "4"){
      array_push($chaveMontada, "(Hn8-Hn4) = [Hn4] = 4A11v1 = 4A(0,1)V1");
      // return "4 (0,1) 4A11V1";
    }

    if($chave == "17,13"){
      array_push($chaveMontada, "(Hn8-Hn4) = [Hn17,Hn13] = 4A22v1 = 4A(2,2)V1");
      // return "17,13 (2,2) 4A22V1";
    }
    if($chave == "17"){
      array_push($chaveMontada, "(Hn8-Hn4) = [Hn17] = 4A22v1 = 4A(2,0)V1");
      // return "17 (2,0) 4A22V1";
    }
    if($chave == "13"){
      array_push($chaveMontada, "(Hn8-Hn4) = [Hn13] = 4A22v1 = 4A(0,2)V1");
      // return "13 (0,2) 4A22V1";
    } 

    if($chave == "26,22"){
      array_push($chaveMontada, "(Hn8-Hn4) = [Hn26,Hn22] = 3A33v1 = 4A(3,3)V1");
      // return "26,22 (3,3) 3A33V1";
    }
    if($chave == "26"){
      array_push($chaveMontada, "(Hn8-Hn4) = [Hn26] = 3A33v1 = 4A(3,0)V1");
      // return "26 (3,0) 3A33V1";
    }
    if($chave == "22"){
      array_push($chaveMontada, "(Hn8-Hn4) = [Hn22] = 3A33v1 = 4A(0,3)V1");
      // return "22 (0,3) 3A33V1";
    } 

    if($chave == "7,3"){
      array_push($chaveMontada, "(Hn7-Hn3) = [Hn7,Hn3] = 3A11V1 = 3A(1,1)V1");
      // return "7,3 (1,1) 3A11V1";
    }
    if($chave == "7"){
      array_push($chaveMontada, "(Hn7-Hn3) = [Hn7] = 3A11V1 = 4A(1,0)V1");
      // return "7 (1,0) 3A11V1";
    }
    if($chave == "3"){
      array_push($chaveMontada, "(Hn7-Hn3) = [Hn3] = 3A11V1 = 3A(0,1)V1");
      // return "3 (0,1) 3A11V1";
    }    

    if($chave == "16,12"){
      array_push($chaveMontada, "(Hn7-Hn3) = [Hn12,Hn16] = 3A22V1 = 3A(2,2)V1");
      // return "16,12 (2,2) 3A22V1";
    }
    if($chave == "16"){
      array_push($chaveMontada, "(Hn7-Hn3) = [Hn16] = 3A22V1 = 3A(2,0)V1");
      // return "16 (2,0) 3A22V1";
    }
    if($chave == "12"){
      array_push($chaveMontada, "(Hn7-Hn3) = [Hn12] = 3A22V1 = 3A(0,2)V1");
      // return "12 (0,2) 3A22V1";
    }

    if($chave == "25,21"){
      array_push($chaveMontada, "(Hn7-Hn3) = [Hn25,Hn21] = 2A33V1 = 2A(3,3)V1");
      // return "25,21 (3,3) 2A33V1";
    }
    if($chave == "25"){
      array_push($chaveMontada, "(Hn7-Hn3) = [Hn25] = 2A33V1 = 2A(3,0)V1");
      // return "25 (3,0) 2A33V1";
    }
    if($chave == "21"){
      array_push($chaveMontada, "(Hn7-Hn3) = [Hn21] = 2A33V1 = 2A(0,3)V1");
      // return "21 (0,3) 2A33V1";
    }                                         

    if($chave == "6,2"){
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn6,Hn2] = 2A22V1 = 2A(2,2)V1");
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn15] = 1A12v1 = 1A(0,1)V1");
      // return "6,2 (1,1) 2A22V1";
    }
    if($chave == "6"){
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn6] = 2A22V1 = 2A(2,0)V1");
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn15] = 1A12v1 = 1A(0,1)V1");
      // return "6 (1,0) 2A22V1";
    }
    if($chave == "2"){
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn2] = 2A22V1 = 2A(0,2)V1");      
      // return "2 (0,2) 2A22V1";
    }

    if($chave == "24,20"){      
      array_push($chaveMontada, "(Hn6-Hn2)  = [Hn24,Hn20] = 1A33v1 = 1A(3,3)V1");
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn24]      = 1A23v1 = 1A(0,2)V1");
      // return "24,20 (3,3) 1A33V1";
    }
    if($chave == "24"){
      array_push($chaveMontada, "(Hn6-Hn2)  = [Hn24] = 1A33v1 = 1A(3,0)V1");
      array_push($chaveMontada, "(Hn10-Hn6) = [Hn24] = 1A23v1 = 1A(0,2)V1");
      // return "24 (3,0) 1A33V1";
    }
    if($chave == "20"){
      array_push($chaveMontada, "(Hn6-Hn2) = [Hn20] = 1A33v1 = 1A(0,3)V1");
      // return "20 (0,3) 1A33V1";
    }   

    if($chave == "5,1"){
      array_push($chaveMontada, "(Hn5-Hn1) = [Hn5,Hn1] = 1A11V1 = 1A(1,1)V1");
      array_push($chaveMontada, "(Hn9-Hn5) = [Hn5] = 5A11v1 = 5A(0,1)V1");      
      // return "(Hn5-Hn1) = [Hn5,Hn1] = 1A11V1 = 1A(1,1)V1";
    }
    if($chave == "5"){
      array_push($chaveMontada, "(Hn5-Hn1) = [Hn5] = 1A11V1 = 1A(1,0)V1");
      array_push($chaveMontada, "(Hn9-Hn5) = [Hn5] = 5A11v1 = 5A(0,1)V1");      
      // return "5 (1,0) 1A11V1";
    }
    if($chave == "1"){
      array_push($chaveMontada, "(Hn5-Hn1) = [Hn1] = 1A11V1 = 1A(0,1)V1");
      // return "1 (0,1) 1A11V1";
    }

    if($chave == "14,10"){      
      array_push($chaveMontada, "(Hn14-Hn10) = [Hn14,Hn10] = 1A22V1 = 2A(2,2)V1");
      array_push($chaveMontada, "(Hn9-Hn5)   = [Hn14]      = 5A22V1 = 5A(0,2)V1");      
      // return "14,10 (2,2) 1A22V1";
    }
    if($chave == "14"){
      array_push($chaveMontada, "(Hn14-Hn10) = [Hn14] = 1A22V1 = 2A(2,0)V1");
      array_push($chaveMontada, "(Hn9-Hn5)   = [Hn14] = 5A22V1 = 5A(0,2)V1");      
      // return "14 (2,0) 1A22V1";
    }
    if($chave == "10"){      
      array_push($chaveMontada, "(Hn14-Hn10) = [Hn10] = 1A22V1 = 2A(0,2)V1");
      // return "10 (0,2) 1A22V1";
    }
    // else{
    //   array_push($chaveMontada, "Não Encontrado");
    // }

    return $chaveMontada;                                                        
    
  }

    function tnRefiltragemOx($tnOx){
    $controleOx = [];
    $verificadores = ["9","6","8","5","7","4","6","3","15","12","5","2","14","11","4","1","13","10"];
        
    $chavesOx = explode("-", strip_tags(trim($tnOx)));

    foreach ($chavesOx as $chaveOx) {
      array_push($controleOx, strip_tags(trim($chaveOx)));
    }
    
    $duplaFinalOx = [];
    $duplaTempOx = [];
    
    $duplasOx = ["9"=>"9,6", "6"=>"9,6", "8"=>"8,5", "5"=>"8,5", "7"=>"7,4", "4"=>"7,4", "6"=>"6,3","3"=>"6,3","15"=>"15,12","12"=>"15,12",
    "5"=>"5,2","2"=>"5,2","14"=>"14,11","4"=>"4,1","1"=>"4,1","13"=>"13,10","10"=>"13,10"];

    foreach ($controleOx as $chaveOx) {      
      if(array_key_exists(strip_tags(trim($chaveOx)), $duplasOx)){
        array_push($duplaTempOx, $duplasOx[strip_tags(trim($chaveOx))]);        
      }  
    }
    
    $qtdDuplaTempOx = array_count_values($duplaTempOx);

    foreach ($qtdDuplaTempOx as $keyRefiltragemOx => $valueRefiltragemOx){      
      if($valueRefiltragemOx == 2){
        array_push($duplaFinalOx, verificaChaveOx(strip_tags(trim($keyRefiltragemOx))));        
      }
      else{
        $buscaChaveOx = explode(",", $keyRefiltragemOx);
        if(in_array(strip_tags(trim($buscaChaveOx[0])), $controleOx)){
          array_push($duplaFinalOx, verificaChaveOx(strip_tags(trim($buscaChaveOx[0]))));          
        }
        if(in_array(strip_tags(trim($buscaChaveOx[1])), $controleOx)){
          array_push($duplaFinalOx, verificaChaveOx(strip_tags(trim($buscaChaveOx[1]))));                     
        }        
      }      
    }

    return $duplaFinalOx;
  }

  function verificaChaveOx($chave){
    echo $chave.'<br>';
    $oxMontado = [];
    if($chave == "9,6"){
      array_push($oxMontado, "(Oxn9-Oxn6) = [Oxn9-Oxn6] = 6Ox A11V1");
      // return ;      
    }
    if($chave == "6"){
      array_push($oxMontado, "(Oxn9-Oxn6) = [Oxn6] = 6Ox A11V1");
      // return "";            
    }
    if($chave == "9"){
      array_push($oxMontado, "(Oxn9-Oxn6) = [Oxn9] = 6Ox A11V1");
      // return "(Oxn9-Oxn6) = [Oxn9] = 6Ox A11V1";            
    }

    if($chave == "8,5"){
      array_push($oxMontado, "(Oxn8-Oxn5) = [Oxn8-Oxn5] = 5Ox A11V1");
      // return "(Oxn8-Oxn5) = [Oxn8-Oxn5] = 5Ox A11V1";            
    }
    if($chave == "8"){
      array_push($oxMontado, "(Oxn8-Oxn5) = [Oxn8] = 5Ox A11V1");
      // return "(Oxn8-Oxn5) = [Oxn8] = 5Ox A11V1";            
    }
    if($chave == "5"){
      array_push($oxMontado, "(Oxn8-Oxn5) = [Oxn5] = 5Ox A11V1");
      // return "(Oxn8-Oxn5) = [Oxn5] = 5Ox A11V1";            
    }

    if($chave == "7,4"){
      array_push($oxMontado, "(Oxn7-Oxn4) = [Oxn7-Oxn4] = 4Ox A11V1");
      // return "(Oxn7-Oxn4) = [Oxn7-Oxn4] = 4Ox A11V1";                  
    }
    if($chave == "7"){
      array_push($oxMontado, "(Oxn7-Oxn4) = [Oxn7] = 4Ox A11V1");
      // return "(Oxn7-Oxn4) = [Oxn7] = 4Ox A11V1";
      // return "(7,-) = 4Ox A11V1";
    }
    if($chave == "4"){
      array_push($oxMontado, "(Oxn7-Oxn4) = [Oxn4] = 4Ox A11V1");
      // return "(Oxn7-Oxn4) = [Oxn4] = 4Ox A11V1";
    }

    if($chave == "6,3"){
      array_push($oxMontado, "(Oxn6-Oxn3) = [Oxn6-Oxn3] = 3Ox A11V1");
      // return "(Oxn6-Oxn3) = [Oxn6-Oxn3] = 3Ox A11V1";
      // return "(6,3) = 3Ox A11V1";
    }
    if($chave == "6"){
      array_push($oxMontado, "(Oxn6-Oxn3) = [Oxn6] = 3Ox A11V1");
      // return "(Oxn6-Oxn3) = [Oxn6] = 3Ox A11V1";
    }
    if($chave == "3"){
      array_push($oxMontado, "(Oxn6-Oxn3) = [Oxn3] = 3Ox A11V1");
      // return "(Oxn6-Oxn3) = [Oxn3] = 3Ox A11V1";
    }

    if($chave == "15,12"){
      array_push($oxMontado, "(Oxn15-Oxn12) = [Oxn15-Oxn12] = 3Ox A22V1");
      // return "(Oxn15-Oxn12) = [Oxn15-Oxn12] = 3Ox A22V1";
    }
    if($chave == "15"){
      array_push($oxMontado, "(Oxn15-Oxn12) = [Oxn15] = 3Ox A22V1");
      // return "(Oxn15-Oxn12) = [Oxn15] = 3Ox A22V1";
    }
    if($chave == "12"){
      array_push($oxMontado, "(Oxn15-Oxn12) = [Oxn12] = 3Ox A22V1");
      // return "(Oxn15-Oxn12) = [Oxn12] = 3Ox A22V1";
    }

    if($chave == "5,2"){      
      array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn5-Oxn2] = 2Ox A11V1");
      // return "(Oxn5-Oxn2) = [Oxn5-Oxn2] = 2Ox A11V1";
    }
    if($chave == "5"){
      array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn5] = 2Ox A11V1");
      // return "(Oxn5-Oxn2) = [Oxn5] = 2Ox A11V1";
    }
    if($chave == "2"){
      array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn2] = 2Ox A11V1");
      // return "(Oxn5-Oxn2) = [Oxn2] = 2Ox A11V1";
      // return "(-,2) = 2Ox A11V1";
    }

    if($chave == "14,11"){
      array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn14-Oxn11] = 2Ox A22V1");
      // return "(Oxn5-Oxn2) = [Oxn14-Oxn11] = 2Ox A22V1";
    }
    if($chave == "14"){
      array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn14] = 2Ox A22V1");
      // return "(Oxn5-Oxn2) = [Oxn14] = 2Ox A22V1";
    }
    if($chave == "11"){
      array_push($oxMontado, "(Oxn5-Oxn2) = [Oxn11] = 2Ox A22V1");
      // return "(Oxn5-Oxn2) = [Oxn11] = 2Ox A22V1";
    }

    if($chave == "4,1"){
      array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn4-Oxn1] = 1Ox A11V1");
      // return "(Oxn4-Oxn1) = [Oxn4-Oxn1] = 1Ox A11V1";      
    }
    if($chave == "4"){
      array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn4] = 1Ox A11V1");
      // return "(Oxn4-Oxn1) = [Oxn4] = 1Ox A11V1";      
    }
    if($chave == "1"){
      array_push($oxMontado, "(Oxn4-Oxn1) = [Oxn1] = 1Ox A11V1");
      // return "(Oxn4-Oxn1) = [Oxn1] = 1Ox A11V1";            
    }

    if($chave == "13,10"){
      array_push($oxMontado, "(Oxn13-Oxn10) = [Oxn13,Oxn10] = 1Ox A22V1");
      // return "(Oxn13-Oxn10) = [Oxn13,Oxn10] = 1Ox A22V1";            
    }
    if($chave == "13"){
      array_push($oxMontado, "(Oxn13-Oxn10) = [Oxn13] = 1Ox A22V1");
      // return "(Oxn13-Oxn10) = [Oxn13] = 1Ox A22V1";            
    }
    if($chave == "10"){
      array_push($oxMontado, "(Oxn13-Oxn10) = [Oxn10] = 1Ox A22V1");
      // return "(Oxn13-Oxn10) = [Oxn10] = 1Ox A22V1";            
    }
    return $oxMontado;
    // else{
    //   return "Não Encontrado";
    // }
    
  }


 ?>