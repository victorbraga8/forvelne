<?php 
// error_reporting(0);
// ini_set('display_errors', 0 );
require_once('includes/head-dashboard.php');
require_once('functions/combi-dao.php');
if($_GET['elementos']){
	if($_GET['gerar'] || $_GET['gerar'] == 0){
		// $elementosGrupo = ['a','e','h','j','o'];      		
		$elementosGrupo = ['a','e','h','j','o','t','x','b','f','i','k','p','v','y','c','g','l','q','v','z','d','m','r','w','n','s'];      	
		shuffle($elementosGrupo);	
		$resultado = combinacoesDe($_GET['elementos'], $elementosGrupo);  		
		array_unique($resultado);		
		$resultado = montaBase($_GET['elementos'], $resultado);		
		asort($resultado);	
		$resultadoBase = count($resultado);

		file_put_contents('elementos.txt',  '<?php return ' . var_export($elementosGrupo, true) . ';');		
		file_put_contents('array.txt',  '<?php return ' . var_export($resultado, true) . ';');

	}else{
		$resultado = file_get_contents("array.txt");
		// $resultadoBase = file_get_contents("contador.txt");
		$resultado = include('array.txt');	
		$resultadoBase = count($resultado);
	}		

	$resultado = montagemFinal($resultado, $_GET['elementos']);

	if($_GET['gerar']){
		$page = 0;
	}

	if($_GET['page']){
		$page = $_GET['page'];	
	}

	if($page == 0){
		$paginaControle = 1;

	}else{
		$paginaControle = $page +1;
	}

	$count = count($resultado);
	$perPage = 1000;
	$numberOfPages = ceil($count / $perPage);		
	$offset = $page * $perPage;		
	$resultado = array_slice($resultado, $offset, $perPage);	
	
	if($_GET['page']){
		$page = $_GET['page']+1;								
	}
	if($_GET['gerar'] || !$_GET['page']){
		$page = 1;
		$ocultaBtn = "display:none;";
	}

	if($_GET['page'] - 1 < 1){
		$pageAnterior = 0;
	}else{
		$pageAnterior = $_GET['page']-1;	
	}						

	if($paginaControle == $numberOfPages){
		$ocultaBtnAvn = "display:none;";
	}
}
?>
<div class="container-fluid">
	<div class="row">
<?php 
		require_once('includes/sidemenu.php');
?>
	</div>
	<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
			<h1 class="h2">Condição de Somatório A</h1>        
		</div>
		<form method="GET">
			<div class="row">
				<div class="col-sm-3">										
					<label for="elementos" style="font-size:17px;">Numerador de Elementos por Grupo</label>
					<input type="number" name="elementos" class="form-control" min="1" max="5" id="elementos" required="Informe a quantidade de elementos por grupo" value="<?=$_GET['elementos']?>">
				</div>
				<div class="col-sm-3">
					<label for="condicao" style="font-size:17px;">Condição de Somatório</label>
					<input type="number" name="condicao" class="form-control" min="1" max="22" id="condicao"  value="<?=$_GET['condicao']?>">
				</div>
				<div class="col-sm-1" style="max-width:77px;">					
					<button type="submit" name="gerar" value="0" id="btnGerar" class="btn btn-success" style="position: absolute;top: 16.9%;">Gerar</button>
				</div>
<?php 
				if($_GET){
?>	
				<div class="col-sm-3">
					<a href="dashboard.php">
						<button type="button" class="btn btn-primary" style="position: absolute;top: 16.9%;">Limpar Resultados</button>            
					</a>
				</div>	

				<div class="row" style="padding-top:13px;">
					<div class="col-sm-3">
						<button type="submit"  class="btn btn-primary" name="page"  style="top: 48.9%; <?php echo $ocultaBtn;?>" id="btnAnterior" value="<?php echo $pageAnterior;?>">ANTERIOR</button>
					</div>	
					<div class="col-sm-3" style="padding-left:18px;">
						<button type="submit"  class="btn btn-primary" name="page" id="btnProximo"  style="top: 48.9%; <?php echo $ocultaBtnAvn;?>" value="<?php echo $page;?>">PROXIMO</button>	
					</div>									
				</div>
				<br>
				<div class="row"  style="padding-top: 25px;">
					<div class="col-sm-4">
						<h5>Selecionar Página</h5>		
						<div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <button class="btn btn-success btnpageSlc" type="submit" id="button-addon1">Confirmar</button>
						  </div>
						  <input type="text" id="pageSlc" class="form-control" name="page" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1">
						</div>						
					</div>
				</div>						
<?php 				
				}
?>
			</div>
			<hr>
		</form>
		<script type="text/javascript">

			paginaSlc = document.querySelector('#pageSlc');
			paginaSlc.addEventListener('change',numerodePaginas);			
			btnPgSlc = document.querySelector('.btnpageSlc');			
			btnPgSlc.addEventListener('click',mudaName);

			btnGerar = document.querySelector('#btnGerar');
			btnProximo = document.querySelector('#btnProximo'); 
			btnAnterior = document.querySelector('#btnAnterior');

			btnGerar.addEventListener('mouseover', tokenPaginacao);
			btnGerar.addEventListener('click', tokenPaginacao);

			btnProximo.addEventListener('mouseover', mudaNamePageSlc);
			btnAnterior.addEventListener('mouseover', mudaNamePageSlc);
			
			function mudaNamePageSlc(){				
				btnProximo.setAttribute('name','page');
				btnAnterior.setAttribute('name','page');
				paginaSlc.removeAttribute('name');
			}

			function mudaName(){				
				btnProximo.removeAttribute('name');				
				btnAnterior.removeAttribute('name');				
				paginaSlc.setAttribute('name','page');
				paginaSlc.setAttribute('style','color:transparent;');
				paginaSlc.value = Number(paginaSlc.value) - 1;				
			}			

			function tokenPaginacao(){
				btnGerar.setAttribute('value',1);							
			}

			function numerodePaginas(){
				numerodePaginas = document.querySelector('#numerodePaginas');
				if(Number(paginaSlc.value) > Number(numerodePaginas.innerText)){
					alert(`Número máximo de páginas permitido ${numerodePaginas.innerText}`);
					paginaSlc.value = Number(numerodePaginas.innerText);
				}
			}
		</script>
<?php 
		if($resultado){			
 ?>			
 		<div class="table-responsive" style="overflow-x: unset!important;">      			 			
 			<div class="row">
				<div class="col-sm-3">
 					<p>Quantidade Total de Combinações: <b><?=$resultadoBase?></b></p>		
 				</div> 	
 				<div class="col-sm-3">
 					<p>Número de Páginas: <b><?=$paginaControle?> de <span id="numerodePaginas"><?=$numberOfPages?><span></b></p>		
 				</div> 				
 			</div> 			
 			<h2>Resultados</h2>
 			<div class="row" style="max-height: 350px; overflow-x: auto; padding:15px;">              
<?php 			
			if($_GET['page'] > 0){
				$i = $_GET['page'] * 1000;
				$i = $i + 1;
			}else{
				$i = 1;  	
			}			
			asort($resultado);                  
			foreach ($resultado as $resultados) {
					echo "<div class='col-sm-4'><p><b>".$i.":</b> Grupo: <b>".strtoupper($resultados[2])."</b> (".$resultados[1].") -- <b>".$resultados[0]."</b></p></div>";
				$i++;
			}
?>
			</div> 	
			<div class="col-sm-3" style="margin-top: 15px;">
				<div class="alert alert-primary" role="alert">                              
<?php 
					$qtd = $i-1;
					echo "Total de Resultados na Página: <b>".$qtd."</b>";
?>
				</div>
			</div>			
 		</div>
 		<div class="table-responsive" style="overflow-x: unset!important;">     
 			<h2>Condição de Somatório: <?=$_GET['condicao']?></h2> 			
 			<div class="row" style="max-height: 350px; overflow-x: auto; padding:15px;">              
<?php 
			if($_GET['condicao'] >= $_GET['elementos']){
				$i = 0;
				foreach ($resultado as $resultados) {
				if($resultados[0] != $_GET['condicao']){
					$ocultaExibeDiv = "style=display:none;";	
				}else{
					$ocultaExibeDiv = "style=display:block;";	
					$i ++;
				}					
				echo "<div class='col-sm-4' ".$ocultaExibeDiv."><p><b>".$i.":</b> Grupo: <b>".strtoupper($resultados[2])."</b> (".$resultados[1].") - Soma: <b>".$resultados[0]."</b></p></div>";								
				}
?>
		</div>		 			
			<div class="col-sm-3" style="margin-top: 15px;"> 				
				<div class="alert alert-primary" role="alert">                              
	<?php 
				$qtd = $i;
				echo "Total de Combinações na Página: <b>".$qtd."</b>";
	?>
				</div>
			</div>
 		</div>
<?php								
			}elseif($_GET['condicao'] < $_GET['elementos']){
				$grupo = verificaGrupoBase($_GET['condicao']);							
				$resultado = combinacoesDe($_GET['elementos'], $grupo);								
				$resultado = montagemFinal($resultado, $_GET['elementos']);

?>
			<div class="table-responsive" style="overflow-x: unset!important;">     				
				<div class="row" style="max-height: 350px; overflow-x: auto; padding:15px;">   
<?php 
					$i = 1;
					foreach ($resultado as $resultados) {
					echo "<div class='col-sm-4' ".$ocultaExibeDiv."><p><b>".$i.":</b> Grupo: <b>".strtoupper($resultados[2])."</b> (".$resultados[1].") - Soma: <b>".$resultados[0]."</b></p></div>";
					$i++;	
					}
					$qtd = $i-1;					
 ?>					
				</div>
				<div class="col-sm-3" style="margin-top: 15px; margin-left:-15px;">
					<div class="alert alert-success" role="alert">    
<?php 
						echo "Total de Resultados: <b>".$qtd."</b>";
?>
					</div>
				</div>
			</div>
<?php				
			}		
		}
?>
	</main>
</div>
<script type="text/javascript">
  condicao = document.querySelector('#condicao');
  condicao.addEventListener('change',validaInsert);
  elementos = document.querySelector('#elementos');
  elementos.addEventListener('change',validaInsert);
  function validaInsert(){                
    if(elementos.value > 5){
      alert('O valor máximo de elementos por grupo habilitado é 5.');
      elementos.value = 5          
    }
    if(condicao.value > 22){
      alert('O valor máximo para condição de somatório é 22.');
      condicao.value = 22;
    }
  }
</script>