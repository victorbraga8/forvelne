<?php 
error_reporting(0);
ini_set('display_errors', 0 );
require_once('functions/Combinations.php');
require_once("verificaLogin.php");
require_once('includes/head-dashboard.php');
require_once('functions/combi-dao-a.php');
// require_once('functions/combi-dao-b.php');
    $dadosD = strip_tags(rtrim(ltrim(trim($_POST['dados']))));
    $dadosD = preg_replace('/\s+/', ' ', $dadosD);    
?>
<style type="text/css">

  @media screen and (max-width: 1880px){
  #btnGerar, #limparResultado{  
    top: 22.9%!important;
  }
  .variaveisTitulo{
    height: 100px!important;
  } 
}
  .gruposVariaveis{
  border-bottom: 5px dashed #6c757d; 
  padding: 15px 0px 0px 0px;
  }

  .gruposVariaveis2{  
  padding: 15px 0px 0px 0px;
  }

  .produtorio{
    font-size: 25px!important;
    font-weight: bold!important;
  }

  .variaveisTitulo{
    font-size: 19px!important;
    height: 75px !important;
    margin-top: -15px; 
  }

  .variaveis{
    font-size: 18px!important;
    font-weight: bold!important;
  }

</style>

<div class="container-fluid">
  <div class="row">
<?php 
    require_once('includes/sidemenu.php');
?>
  </div>
  <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Transposição Numérica</h1>        
    </div>
    <form method="POST">
      <div class="row">
        <label for="elementos" style="font-size:17px;">Massa de Dados</label>
        <div class="col-sm-8">
                    
          <input type="text" name="dados" class="form-control" id="dados" required="Informe a massa de dados" value="<?=$dadosD?>">
        </div>        
        <div class="col-sm-1" style="max-width:77px;">          
          <button type="submit" name="gerar" class="btn btn-success">Gerar</button>
        </div>
<?php 
        if($_POST){
?>  
        <div class="col-sm-3">
          <a href="dashboard.php">
            <button type="button" class="btn btn-primary" >Limpar Resultados</button>            
          </a>
        </div>
<?php         
        }
?>
      </div>
      <hr>
    </form>
<?php    
    if($_POST['dados']){
      $dados = explode(" ", $dadosD);
    }
    $grupos = array();   
    $dados = array_chunk($dados, 5);

    for($i=0; $i<count($dados); $i++){    
      $grupo = verificaGrupo($dados[$i]);        
      if($grupo){
        array_push($grupos, $grupo);  
      }    
    }    
    
    $ordenador = [];

  for($i=0; $i<count($grupos); $i++){
      $subGrupo = montaGrupo($grupos[$i]);        
?>        
    <div class="row" id='rowBase'>
      <div class="row">
        <div class="col-sm-12 produtorio">          
<?php
        echo $subGrupo[0];
?>                    
        </div>
<?php 
          $tn2SubA = combinacoesDe(2, $subGrupo[4]);    
          $tn2GeradoSubA = tn2A($tn2SubA); 
          // echo "<pre>";
          // print_r($tn2GeradoSubA);
          // echo "</pre>";
          foreach($tn2GeradoSubA as $keySubA => $valueSubA){          
            $explodeSubA = explode("-", $valueSubA);
            $ordenadorSubA = $explodeSubA[0];
            $duplaBaseSubA = $explodeSubA[1];

            if(mb_strpos($valueSubA, "AN") !== false){
              $explodeOrdenadorSubA = explode("AN", $valueSubA);
              $ordenadorSubA = $explodeOrdenadorSubA[0];
              $explodeChaveSubA = explode(",", $duplaBaseSubA);                        
              $chaveFinalSubA = strip_tags(trim($explodeChaveSubA[1]).trim(',').trim($explodeChaveSubA[0]));
              $chaveRemocaoSubA = strip_tags(trim($explodeChaveSubA[0]).trim(',').trim($explodeChaveSubA[1]));            
                     
              if(array_key_exists($chaveFinalSubA, $tn2GeradoSubA)){
                unset($tn2GeradoSubA[$chaveRemocaoSubA]);              
              }else{
                $tn2GeradoSubA[$chaveRemocaoSubA] = strip_tags($ordenadorSubA." - ".trim($chaveFinalSubA));
              }            
            }          
          } 
            // echo "<pre>";
            // print_r($tn2GeradoSubA);
            // echo "</pre>";                        
          foreach($tn2GeradoSubA as $keySubA => $valueSubA){          
            $explodeSubA = explode("-", $valueSubA);
            $ordenadorSubA = $explodeSubA[0];
            $duplaBaseSubA = $explodeSubA[1];
            
            $tnTempSubA .= strip_tags($ordenadorSubA." - ");          
            $tnSubA .= strip_tags($valueSubA." / ");          
          }

            // echo "<pre>";
            // print_r($tn2GeradoSubA);
            // echo "</pre>";               

          $tnSubA = substr($tnSubA,0,-2);
          $tnTempSubA = substr($tnTempSubA,0,-2);                    
          $ordenadorFinalSubA = tnRefiltragem($tnTempSubA);  

            // echo "<pre>";
            // print_r($tnTempSubA);
            // echo "</pre>";            

          $arrOrdTempSubA = [];      
          rsort($ordenadorFinalSubA);   
?>        
        <div class="col-sm-12 produtorio" style="margin:2px 0px 5px;">
<?php        
          if($ordenadorFinalSubA){
            foreach ($ordenadorFinalSubA as $ordenadorSubA) {                    
              $ordenadoresSubA .= '<span>'.$ordenadorSubA."</span><br>";       
            }  
          }else{
            $ordenadoresSubA = "<span>Refiltragem SubGrupo A = Sem Resultados</span>";
          }

          echo $ordenadoresSubA;

          $tn2SubA = "";
          $tn2GeradoSubA = "";
          $tnSubA = "";

          $tnTempSubA = "";
          $ordenadorFinalSubA = "";
          $ordenadoresSubA = "";         
?>
        </div>
        <div class="col-sm-12 produtorio">
<?php 
        $tn2SubAOx = combinacoesDe(2, $subGrupo[6]);     
        $oxTn2Gerado = tn2AOx($tn2SubAOx); 
        
        foreach($oxTn2Gerado as $key => $value){          
          $explodeOx = explode("-", $value);
          $ordenadorOx = $explodeOx[0];
          $duplaBaseOx = $explodeOx[1];

          if(mb_strpos($value, "AN") !== false){
            $explodeOrdenadorOx = explode("AN", $value);
            $ordenadorOx = $explodeOrdenadorOx[0];
            $explodeChaveOx = explode(",", $duplaBaseOx);

            $chaveFinalOx = strip_tags(trim($explodeChaveOx[1]).trim(',').trim($explodeChaveOx[0]));
            $chaveRemocaoOx = strip_tags(trim($explodeChaveOx[0]).trim(',').trim($explodeChaveOx[1]));            
            
            if(array_key_exists($chaveFinalOx, $oxTn2Gerado)){
              unset($oxTn2Gerado[$chaveRemocaoOx]);              
            }else{
              $oxTn2Gerado[$chaveRemocaoOx] = strip_tags($ordenadorOx." - ".trim($chaveFinalOx));
            }            
          }          
        }

        foreach($oxTn2Gerado as $key => $value){          
          $explodeOx = explode("-", $value);
          $ordenadorOx = $explodeOx[0];
          $duplaBaseOx = $explodeOx[1];
          
          $tnTempOx .= strip_tags($ordenadorOx." - ");          
          $tnOx .= strip_tags($value." / ");          
        }

        $tnOx = substr($tnOx,0,-2);
        $tnTempOx = substr($tnTempOx,0,-2);                 
        $ordenadorFinalOx = tnRefiltragemOxA($tnTempOx);  
                
        if($ordenadorFinalOx){
        // ForEach está errado, não precisa girar 2 niveis          
          rsort($ordenadorFinalOx);
          foreach ($ordenadorFinalOx as $ordenadorOx) {
            $ordernadoresOx .= '<span>'.$ordenadorOx."</span><br>";
          }          
        }else{
          $ordernadoresOx = "<span>Listagem Ox SubGrupo A = Sem Resultados</span>";
        }
        
        echo $ordernadoresOx;        

        $ordernadoresOx = "";   
        $ordenadorFinalOx = "";  
        $tnTempOx = "";
        $tnTemp = "";
 ?>          
        </div>
      </div>
      <!-- Final do Row de Listagem do Sub A -->

      <!-- Final do Row de Listagem do Sub B -->
<?php 
        $tn2SubB = combinacoesDe(2, $subGrupo[5]);
        $tn2BGerado = tn2B($tn2SubB);
        
        echo "<pre>";
        print_r($tn2SubB);
        echo "</pre>";        

        echo "<pre>";
        print_r($tn2BGerado);
        echo "</pre>";        
       
        foreach($tn2BGerado as $key => $value){              
          $explode = explode("-", $value);       
          $ordenador = $explode[0];
          $duplaBase = $explode[1];

          if(mb_strpos($value, "AN") !== false){
            $explodeOrdenador = explode("AN", $value);
            $ordenador = $explodeOrdenador[0];
            $explodeChave = explode(",", $duplaBase);                        
            $chaveFinal = strip_tags(trim($explodeChave[1]).trim(',').trim($explodeChave[0]));
            $chaveRemocao = strip_tags(trim($explodeChave[0]).trim(',').trim($explodeChave[1]));        
            if(array_key_exists($chaveFinal, $tn2BGerado)){
              unset($tn2BGerado[$chaveRemocao]);              
            }else{
              $tn2BGerado[$chaveRemocao] = strip_tags($ordenador." - ".trim($chaveFinal));
            }            
          }          
        } 

        foreach($tn2BGerado as $key => $value){          
          $explode = explode("-", $value);
          $ordenador = $explode[0];
          $duplaBase = $explode[1];
          
          $tnTemp .= strip_tags($ordenador." - ");          
          $tn .= strip_tags($value." / ");          
        }

        $tn = substr($tn,0,-2);
        $tnTemp2 = substr($tnTemp,0,-2);  
        $ordenadorFinalB = tnRefiltragemB($tnTemp2);            

        $arrOrdTempB = []; 

        if($ordenadorFinalB){
          foreach ($ordenadorFinalB as $ordenadorB) {                    
            $ordernadoresB .= '<span>'.$ordenadorB."</span><br>";       
          }  
        }else{
          $ordernadoresB = "<span>Refiltragem SubGrupo B = Sem Resultados</span>";
        }               
 ?>      
      <div class="row">
        <div class="col-sm-12 produtorio" style="margin-top:25px;">
<?php 
        echo $subGrupo[2];
?>          
        </div>
        <div class="col-sm-12 produtorio" style="margin:2px 0px 5px;">
<?php 
          echo $ordernadoresB;
          $tn2SubB = "";

          $tn2BGerado = "";
          $ordenadorFinalB = "";

          $arrOrdTempB = "";
          $ordenadorFinalB = "";
          $ordernadoresB = "";

          $oxTn2BGerado = "";
          $tn2BSubOx = "";

          $tn2BSubOx = combinacoesDe(2, $subGrupo[7]);             
          $oxTn2BGerado = tn2BOx($tn2BSubOx); 

          if($oxTn2BGerado){
            foreach ($oxTn2BGerado as $oxBMontado) {
              $ordernadoresOxB .= '<span>'.$oxBMontado."</span><br>";          
            }  
          }else{
            $ordernadoresOxB = "<span>Listagem Ox SubGrupo B = Sem Resultados</span>";
          }                          
?>          
        </div>
        <div class="col-sm-12 produtorio">
<?php 
          echo $ordernadoresOxB;
          $ordernadoresOxB = "";        
          $oxTn2BGerado = "";
          $tn2BSubOx = ""; 
?>          
        </div>
      </div>      
      <!-- Final do Row de Listagem do Sub B -->
    </div>
    <hr>
<?php
  }    
?>            
  </main>
</div>
<script type="text/javascript" src="includes/script.js"></script>