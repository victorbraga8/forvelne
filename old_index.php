<?php    
    require_once('includes/head.php');
    require_once('db/conecta.php');   
    require_once('functions/usuario-dao.php'); 
    // if($_POST['email'] == "teste" || $_POST['senha'] == "teste"){

    // }    
    if($_POST['email'] || $_POST['senha']){
        $usuario = Login($conecta, $_POST['email'], $_POST['senha']);
        
        if($usuario){
          usuarioAprovado($usuario['nome']);          
        }else{
?>
      <script type="text/javascript">
        alert("Usuário não encontrado ou senha incorreta");
      </script>
<?php          
        }
    }
?>
    
<main class="form-signin">
  <form method="POST" action="">
    <img class="mb-4" src="assets/brand/bootstrap-logo.svg" alt="" width="72" height="57">
    <h1 class="h3 mb-3 fw-normal">Sistema - Forvelne</h1>
    <label for="inputEmail" class="visually-hidden">Email address</label>
    <input type="text" style="margin-bottom: 16px;" id="inputEmail" name="email" class="form-control" placeholder="Informe o seu usuário" required autofocus>
    <label for="inputPassword" class="visually-hidden">Password</label>
    <input type="password" id="inputPassword" name="senha" class="form-control" placeholder="Informe a sua senha" required>
    <div class="checkbox mb-3">
      <!-- <label>
        <input type="checkbox" value="remember-me"> Remember me
      </label> -->
    </div>
    <button class="w-100 btn btn-lg btn-primary" type="submit" id="loginBtn" name="login">Login</button>
    <br>
    <a href="cadastra.php">
    <button class="w-100 btn btn-underline btn-link" type="button" style="margin-top:22px;">Cadastrar Usuário</button></a>
    <p class="mt-5 mb-3 text-muted">&copy; <?=date("Y")?></p>
  </form>
</main>


    
  </body>
</html>
