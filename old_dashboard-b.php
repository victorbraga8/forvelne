<?php 
error_reporting(0);
ini_set('display_errors', 0 );
require_once('includes/head-dashboard.php');
require_once('functions/combi-dao-b.php');
if($_POST['elementos']){
  $elementosGrupo = ['a','b','d','h','k','m','n','c','e','i','l','f','j','g'];      
  $resultado = combinacoesDe($_POST['elementos'], $elementosGrupo);         
}
?>
<div class="container-fluid">
  <div class="row">
<?php 
    require_once('includes/sidemenu.php');
?>
  </div>
  <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 class="h2">Condição de Somatório B</h1>        
    </div>
    <form method="POST">
      <div class="row">
        <div class="col-sm-3">
          <label for="elementos" style="font-size:17px;">Numerador de Elementos por Grupo</label>
          <input type="number" name="elementos" class="form-control" min="1" max="5" id="elementos" required="Informe a quantidade de elementos por grupo" value="<?=$_POST['elementos']?>">
        </div>
        <div class="col-sm-3">
          <label for="condicao" style="font-size:17px;">Condição de Somatório</label>
          <input type="number" name="condicao" class="form-control" min="1" max="14" id="condicao" required="Informe a condição de somatório" value="<?=$_POST['condicao']?>">
        </div>
        <div class="col-sm-1" style="max-width:77px;">          
          <button type="submit" name="gerar" id="btnGerar" class="btn btn-success" style="position: absolute;top: 16.9%;">Gerar</button>
        </div>
<?php 
        if($_POST){
?>  
        <div class="col-sm-3">
          <a href="dashboard-b.php">
            <button type="button" class="btn btn-primary"style="position: absolute;top: 16.9%;">Limpar Resultados</button>            
          </a>
        </div>
<?php         
        }
?>
      </div>
      <hr>
    </form>
<?php 
    if($resultado){
      $resultado = montagemFinal($resultado,$_POST['elementos']);
      array_unique($resultado);
      asort($resultado);              
 ?>   
    <div class="table-responsive" style="overflow-x: unset!important;">     
      <h2>Resultados</h2>
      <div class="row" style="max-height: 350px; overflow-x: auto; padding:15px;">              
<?php       
      $i = 1;                    
      foreach ($resultado as $resultados) {
          echo "<div class='col-sm-4'><p><b>".$i.":</b> Grupo: <b>".strtoupper($resultados[2])."</b> (".$resultados[1].") - Soma: <b>".$resultados[0]."</b></p></div>";
        $i++;
      }
?>
      </div>  
      <div class="col-sm-3" style="margin-top: 15px;">
        <div class="alert alert-primary" role="alert">                              
<?php 
          $qtd = $i-1;
          echo "Total de Combinações: <b>".$qtd."</b>";
?>
        </div>
      </div>      
    </div>
    <div class="table-responsive" style="overflow-x: unset!important;">     
      <h2>Condição de Somatório: <?=$_POST['condicao']?></h2>       
      <div class="row" style="max-height: 350px; overflow-x: auto; padding:15px;">              
<?php 
      if($_POST['condicao'] >= $_POST['elementos']){
        $i = 1;
        foreach ($resultado as $resultados) {
        if($resultados[1] != $_POST['condicao']){
          $ocultaExibeDiv = "style=display:none;";  
        }else{
          $ocultaExibeDiv = "style=display:block;"; 
          $i ++;
        }         
          echo "<div class='col-sm-4' ".$ocultaExibeDiv."><p><b>".$i.":</b> Grupo: <b>".strtoupper($resultados[2])."</b> (".$resultados[1].") - Soma: <b>".$resultados[0]."</b></p></div>";                         
        }
?>
    </div>          
      <div class="col-sm-3" style="margin-top: 15px;">        
        <div class="alert alert-primary" role="alert">                              
  <?php 
        $qtd = $i-1;
        echo "Total de Combinações: <b>".$qtd."</b>";
  ?>
        </div>
      </div>
    </div>
<?php               
      }elseif($_POST['condicao'] < $_POST['elementos']){
        $grupo = verificaGrupoBase($_POST['condicao']);             
        $resultado = combinacoesDe($_POST['elementos'], $grupo);        
        $resultado = montagemFinal($resultado, $_POST['elementos']);
        array_unique($resultado);
        asort($resultado);        
?>
      <div class="table-responsive" style="overflow-x: unset!important;">     
        <!-- <h2>Condição de Somatório informada: <b><?=$_POST['condicao']?></b></h2> -->
        <div class="row" style="max-height: 350px; overflow-x: auto; padding:15px;">   
<?php 
          $i = 1;
          foreach ($resultado as $resultados) {
          echo "<div class='col-sm-4'><p><b>".$i.":</b> Grupo: <b>".strtoupper($resultados[2])."</b> (".$resultados[1].") - Soma: <b>".$resultados[0]."</b></p></div>";      
          $i++;  
          }
          $contador = $i - 1;
 ?>         
        </div>
        <div class="col-sm-3" style="margin-top: 15px; margin-left:-15px;">
          <div class="alert alert-success" role="alert">    
<?php 
            echo "Total com Condição não atendida: <b>".$contador."</b>";
?>
          </div>
        </div>
      </div>
<?php       
      }   
    }
?>
  </main>
</div>
<script type="text/javascript">
  condicao = document.querySelector('#condicao');
  condicao.addEventListener('change',validaInsert);
  elementos = document.querySelector('#elementos');
  elementos.addEventListener('change',validaInsert);
  function validaInsert(){                
    if(elementos.value > 5){
      alert('O valor máximo de elementos por grupo habilitado é 5.');
      elementos.value = 5          
    }
    if(condicao.value > 22){
      alert('O valor máximo para condição de somatório é 22.');
      condicao.value = 22;
    }
  }
</script>