<?php 
error_reporting(0);
ini_set('display_errors', 0 );
require_once('includes/head-dashboard.php');
require_once('functions/combi-dao-a.php');
require_once('includes/sidemenu.php');

if($_POST['dados']){
	$resultado = primeFactors($_POST['dados']);
    $fatoracao = verificaProdutorio($resultado);
    if($_POST['tipoGrupo']){
        $tipoGrupo = true;
        $checked = "checked=checked";
    }
}

?>
<div class="container-fluid">
	<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
			<h1 class="h2">Condição de Produtório</h1>        
		</div>
    <form method="POST">
       <div class="row">
           <div class="col-sm-3">
               <label style="font-size:17px;" id="labelTitulo">Informe um número até 1600</label>
               <input type="number" name="dados" class="form-control" id="dadosProd" required="Informe a massa de dados" min="1" max="1600" value="<?=$_POST['dados']?>">
           </div>
           <div class="col-sm-3">
               <label style="font-size:17px;">Utilizar segundo subgrupo</label><br>
               <input type="checkbox" name="tipoGrupo" id="tipoGrupo" value="segundo" <?=$checked?> style="width:38px; height: 38px;">
           </div>
        </div>
        <div class="row">    
           <div class="col-sm-1">
               <label style="font-size:17px;"></label><br>
               <button type="submit" name="gerar" id="gerarProd" class="btn btn-success">Gerar</button>
           </div>
<?php 
            if($_POST){
 ?>
           <div class="col-sm-2">
                <label style="font-size:17px;"></label><br>
            <a href="dashboard-b.php">
               <button type="button" class="btn btn-primary" >Limpar</button>
            </a>   
           </div>
<?php 
            }
 ?>
       </div>
        <hr> 
    </form>
<?php
      if($fatoracao){
        $repeticoesBase = array_count_values($fatoracao);
        // echo "<pre>";
        // print_r($repeticoesBase);
        // echo "</pre>";
        
        $repeticoes = escreveRepeticoes($repeticoesBase);
        
        $fatoracao = escreveFatoracao($fatoracao);

        $variaveis = gerenciaRepeticoes($repeticoesBase, $tipoGrupo);
        // echo "<pre>";
        // print_r($variaveis);
        // echo "</pre>";
        // die();
        $quintetos = montaQuintetoFinal($variaveis, $repeticoesBase);

        // echo "<pre>";
        // print_r($quintetos);
        // echo "</pre>";
        // die();
        echo "(<strong>Fatoração: </strong>".$fatoracao.') - (<strong>Repetições:</strong> '.$repeticoes.') - (<strong>Qtd Quintetos: </strong> <span id="qtdLabel">'.count($quintetos).'</span>)<br><br>';
?>
        <div class="container">
          <div class="row">   
<?php
        $i = 0;
        foreach ($quintetos as $key => $value){
            $resultado = escreveQuintetoFinal($quintetos[$key], $_POST['dados'], $repeticoesBase);
            if($resultado){
?>
          <div class="col-sm-4">
<?php
            echo escreveResultado($resultado);
?>
          </div>
<?php       
            $i++;    
            }   
        }
      }elseif($_POST){
        $fatoracao = escreveFatoracao($fatoracao);
        echo "(<strong>Fatoração: </strong>".$resultado.')<br><br>';
?>
        <div class="alert alert-danger" role="alert">
          Condição de Produtório Inválida
        </div>
<?php
      }
?>
        <input type="hidden" id="controleQuantidade" value="<?=$i?>">
        </div>
      </div>
	</main>
    <script type="text/javascript">
        labelTitulo = document.querySelector('#labelTitulo');
        tipoGrupo = document.querySelector('#tipoGrupo');
        hiddenQtd = document.querySelector('#controleQuantidade');
        if(hiddenQtd.value){
            labelQtd = document.querySelector('#qtdLabel');
            labelQtd.innerText = hiddenQtd.value;
        }
        tipoGrupo.addEventListener('click',handleTextoTitulo);
        handleTextoTitulo();
        function handleTextoTitulo(){
            if(this.checked || tipoGrupo.checked){
                labelTitulo.innerText = "Informe um número até 144";
            }else{
                labelTitulo.innerText = "Informe um número até 1600";
            }
        }
    </script>
    <!-- <script type="text/javascript" src="includes/script.js"></script> -->
</div>
